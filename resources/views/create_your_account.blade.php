@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
<div class="userloginregister">
    <div class="userloginregisterinner">
        <div class="formbox createaccount">
            <h2>Create Account with <br> Wet Tropic Times</h2>
            {!! Form::open(array('route' => 'user.subscribe','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'add-signup-data-form')) !!}

                @if ($message = Session::get('success'))
                    <div class="text-success">
                        {{ $message }}
                    </div>
                @elseif (isset($data['error']) && $data['error'] != '')
                    <div class="text-danger">
                        {{ $data['error'] }}
                    </div>
                @endif

                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="text-danger">{{ $error }}</div>
                    @endforeach
                @endif

                <div class="field">
                    <label>Display Name</label>
                    @if (isset($data['displayname']) && $data['displayname'] != '')
                        <input type="text" name="displayname" id="displayname" placeholder="Type here" required value="{{$data['displayname']}}">
                    @else
                        <input type="text" name="displayname" id="displayname" placeholder="Type here" required>
                    @endif
                    <span>The name you see when you login with Wet Tropic Times </span>
                </div>
                <div class="field">
                    <label>Email Address</label>
                    @if (isset($data['email']) && $data['email'] != '')
                        <input type="email" name="email" id="email" placeholder="Type your email" required value="{{$data['email']}}">
                    @else
                        <input type="email" name="email" id="email" placeholder="Type your email" required>
                    @endif
                </div>
                <div class="field">
                    <label>Password</label>
                    <input type="password" name="password" id="password" placeholder="Type your password" required>
                </div>
                <div class="button">
                    <button type="submit">Continue with Email</button>
                </div>
                <div class="agreeterms">
                    <p>By signing in or creating an account, you agree to our <br>
                       <a href="{{Config::get('constants.app.wp-url').'/terms-conditions'}}" target="_blank">Terms & Conditions</a> and <a href="{{Config::get('constants.app.wp-url').'/privacy-policy'}}" target="_blank">Privacy Policy</a></p>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@section('pagescript')
<!-- Jquery validate -->
<script type="text/javascript">

$(document).ready(function () {
    
    $('#add-signup-data-form').validate({ 
    rules: {
        "displayname": {
            required: true
        },
        "email": {
            required: true,
            email: true
        },
        "password": {
            required: true
        },
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
@stop

@endsection