@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 
<div class="userloginregister">
    <div class="userloginregisterinner">
        <div class="formbox checkinbox">
            <h2>Check <br> Your Inbox</h2>
            <p>
                We’ve just emailed instructions and a reset password link
                to {{$data['Email']}}. It might take a few minutes
                to arrive.
            </p>
            <form>
                <div class="button">
                    <a href="{{route('user.login')}}">Back to Sign In</a>
                </div>
                <div class="agreeterms">
                    <p>By signing in or creating an account, you agree to our <br>
                       <a href="{{Config::get('constants.app.wp-url').'/terms-conditions'}}" target="_blank">Terms & Conditions</a> and <a href="{{Config::get('constants.app.wp-url').'/privacy-policy'}}" target="_blank">Privacy Policy</a></p>
                </div>
            </form>
        </div>
    </div>
</div>

@section('pagescript')

@stop

@endsection