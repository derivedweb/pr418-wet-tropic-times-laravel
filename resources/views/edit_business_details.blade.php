@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
 <style type="text/css">
#image1-error{
    padding-top: 76px;
}
 </style>

@stop
 
<div class="businessdetails">
    <div class="businessdetailsinner">
        <div class="businessdetailsbox">
            <h2>Business Profile <img src="{{ asset('images/check.png')}}"></h2>
            <div class="profileform">

      {!! Form::open(array('route' => 'business.update_business_details','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'update-business-details', 'enctype' => 'multipart/form-data')) !!}  

        @if ($message = Session::get('success'))
        <div class="text-success">
            {{ $message }}
        </div>
        @elseif ($message = Session::get('error'))
        <div class="text-danger">
            {{ $message }}
        </div>
        @endif

        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="text-danger">{{ $error }}</div>
            @endforeach
        @endif
                
            <div class="formfield">
                <div class="fields">
                    <div class="field">
                        <label>Business Name*</label>
                        <input type="text" placeholder="Type your business name" name="business_name" id="business_name" placeholder="Type your business name" required value="{{$business->business_name}}">
                        <div class="radio">
                            <input type="radio" name="use_legal_name_as_business_name" value="1" {{($business->use_legal_name_as_business_name == 1) ? 'checked' : '' }}> Use legal name as business name
                        </div>
                    </div>

                    <div class="field">
                        <label>Category*</label>
                        <div class="select">
                            <select name="category_id" id="category_id" required>
                                <option value=""></option>
                               @foreach ($categories as $value) 
                                    @if($business->category_id == $value->category_id)
                                        <option value="{{$value->category_id}}" selected="">{{$value->category_name}}</option>
                                    @else
                                        <option value="{{$value->category_id}}">{{$value->category_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                        </div>
                    </div>
                    <div class="field">
                        <label>Region*</label>
                        <div class="select">
                            <select name="region_id" id="region_id" required>
                                <option value=""></option>
                                @foreach ($regions as $value) 
                                    @if($business->region_id == $value->region_id)
                                        <option value="{{$value->region_id}}" selected="">{{$value->region_name}}</option>
                                    @else
                                        <option value="{{$value->region_id}}">{{$value->region_name}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label>Business Summary*</label>
                        <div class="textarea">
                             <textarea name="business_summary" id="business_summary" placeholder="Type here" required>{{$business->business_summary}}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <div class="formfield">
                <div class="fields">
                    <div class="field">
                        <label>Address*</label>
                        <input type="text" name="address" id="address" placeholder="Type here" required value="{{$business->address}}">
                        <input type="hidden" name="location" id="location" value="{{$business->location}}">
                        <input type="hidden" name="latitude" id="latitude" value="{{$business->latitude}}">
                        <input type="hidden" name="longitude" id="longitude" value="{{$business->longitude}}">
                        <input type="hidden" name="city" id="city" value="{{$business->city}}">
                        <input type="hidden" name="state" id="state" value="{{$business->state}}">
                        <input type="hidden" name="country" id="country" value="{{$business->country}}">
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label>Phone Number*</label>
                        <input type="text" name="phone_no" id="phone_no" onkeypress="return KeycheckOnlyNumeric(event);" placeholder="Type your phone number" required value="{{$business->phone_no}}">
                    </div>
                </div>
            </div>

            <div class="formfield">
                <div class="fields">
                    <div class="field">
                        <label>Email*  (for us to contact you)</label>
                        <input type="text" name="email_for_contact_you" id="email_for_contact_you" placeholder="Type your email address" required value="{{$business->email_for_contact_you}}">
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label>Email*  (to be listed on the business directory)</label>
                        <input type="text" name="email_for_business_directory" id="email_for_business_directory" placeholder="Type your email address" required value="{{$business->email_for_business_directory}}">
                    </div>
                </div>
            </div>   

            <div class="formfield">
            <div class="formfield">
            <div class="fields">
                <div class="field">
                    <label>Opening Hours</label>
                    <div class="selectdays office-opening-hours">
                        @foreach($business_times as $key => $business_time)
                        <div class="row">
                            <div class="col-md-4 pt-5 text-left">
                                <label>{{$business_time->day}}</label>
                                <input type="hidden" name="day[]" id="day_{{$key}}" value="{{$business_time->day}}">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="open_time[]" id="open_time_{{$key}}" class="from-timepicker" readonly="" value="{{date('h:i A', strtotime($business_time->open_time))}}">
                            </div>
                            <div class="col-md-4">
                                <input type="text" name="close_time[]" id="close_time_{{$key}}" class="to-timepicker" readonly="" value="{{date('h:i A', strtotime($business_time->close_time))}}">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="fields">
                <div class="field">
                    <label>Links</label>
                    <ul class="links">
                        <li>
                            <input type="text" name="facebook_link" id="facebook_link" value="{{$business->facebook_link}}">
                            <img src="{{ asset('images/ic_business_listing_facebook.png')}}">
                        </li>
                        <li>
                            <input type="text" name="twitter_link" id="twitter_link" value="{{$business->twitter_link}}">
                            <img src="{{ asset('images/ic_business_listing_twitter.png')}}">
                        </li>
                        <li>
                            <input type="text" name="linkedin_link" id="linkedin_link" value="{{$business->linkedin_link}}">
                            <img src="{{ asset('images/ic_business_listing_linkedin.png')}}">
                        </li>
                        <li>
                            <input type="text" name="youtube_link" id="youtube_link" value="{{$business->youtube_link}}">
                            <img src="{{ asset('images/ic_business_listing_youtube.png')}}">
                        </li>
                        <li>
                            <input type="text" name="website_link" id="website_link" value="{{$business->website_link}}">
                            <img src="{{ asset('images/ic_business_listing_website.png')}}">
                        </li>
                    </ul>
                </div>
            </div>     
            </div> 
                
            <div class="formfield">
                <div class="fields">
                    <div class="field">
                        <label>Business Services*</label>
                        <div class="textarea">
                            <textarea name="services" id="services" maxlength="100" onkeyup="countChar(this)" placeholder="Type here" required>{{$business->services}}</textarea>
                            <span><b id="charNum">0</b>/100</span>
                        </div>
                    </div>
                </div>
                <div class="fields">
                    <div class="field">
                        <label>Upload your crest, emblem or logo here</label>
                        <div class="uploadlogo">
                            <div class="upload">
                                <input name="bus_logo" id="business_logo" type="file">
                                <img src="{{ asset('images/ic_profile_logo_upload.png')}}">
                                <span class="filename">{{$business->business_logo}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  

            <div class="formfield">
                <label>Photos (minimum 1 image per profile. maximum 5 images)*</label>
                 <div class="photos">
                    <div class="item addphoto">
                        <div class="iteminner text-center">
                            <img id="photo1" class="" src="{{ asset('business_images/'.$business->image)}}"> 
                            
                            <input type="file" name="business_image" id="image1" accept="image/*" onchange="loadFile1(event)" style="opacity: 0">
                            <!-- <img id="remove1" class="remove" src="{{ asset('images/ic_profile_images_remove.png')}}" data-business-id="{{$business->business_id}}"> -->
                        </div>
                    </div>
                    @php
                        $photo_cnt = 1;
                    @endphp
                    @foreach($business_images as $key => $business_image)
                    @php
                        $photo_cnt++;
                    @endphp
                    <div class="item addphoto">
                        <div class="iteminner text-center">
                            <img id="photo{{($key+2)}}" class="" src="{{ asset('business_images/'.$business_image->image_name)}}">
                            <!-- <img  id="photo{{($key+2)}}" class="upload" src="{{ asset('images/ic_profile_images_upload.png')}}"> -->
                            <input type="file" name="business_images[]" id="image{{($key+2)}}" accept="image/*" onchange="loadFile{{($key+2)}}(event)" style="opacity:0;">
                            <img id="remove{{($key+2)}}" class="remove" src="{{ asset('images/ic_profile_images_remove.png')}}" data-business-image-id="{{$business_image->business_image_id}}">
                        </div>
                    </div>
                    @endforeach
                    @for($x=$business_images->count()+2; $x<=5; $x++)
                    <div class="item addphoto">
                        <div class="iteminner text-center">
                            <!-- <img src="{{ asset('images/businesses2.png')}}"> -->
                            <img  id="photo{{$x}}" class="upload" src="{{ asset('images/ic_profile_images_upload.png')}}">
                            <input type="file" name="business_images[]" id="image{{$x}}" accept="image/*" onchange="loadFile{{$x}}(event)" style="display: none;">
                            <img id="remove{{$x}}" class="remove" src="{{ asset('images/ic_profile_images_remove.png')}}" style="display: none;">
                        </div>
                    </div>
                    @endfor
                    <!-- <div class="item addphoto">
                        <div class="iteminner text-center">
                            <img  id="photo3" class="upload" src="{{ asset('images/ic_profile_images_upload.png')}}">
                            <input type="file" name="business_images[]" id="image3" accept="image/*" onchange="loadFile3(event)" style="display: none;">
                            <img id="remove3" class="remove" src="{{ asset('images/ic_profile_images_remove.png')}}" style="display: none;">
                        </div>
                    </div>
                    <div class="item addphoto">
                        <div class="iteminner text-center">
                            <img  id="photo4" class="upload" src="{{ asset('images/ic_profile_images_upload.png')}}">
                            <input type="file" name="business_images[]" id="image4" accept="image/*" onchange="loadFile4(event)" style="display: none;">
                            <img id="remove4" class="remove" src="{{ asset('images/ic_profile_images_remove.png')}}" style="display: none;">
                        </div>
                    </div>
                    <div class="item addphoto">
                        <div class="iteminner text-center">
                            <img  id="photo5" class="upload" src="{{ asset('images/ic_profile_images_upload.png')}}">
                            <input type="file" name="business_images[]" id="image5" accept="image/*" onchange="loadFile5(event)" style="display: none;">
                            <img id="remove5" class="remove" src="{{ asset('images/ic_profile_images_remove.png')}}" style="display: none;">
                        </div>
                    </div>
                </div> -->
            </div>
            @if(Request::get('flag') && Request::get('flag') == 'edit')
                <input type="hidden" name="flag" value="edit">
                <input type="hidden" name="product_id" value="{{Request::get('product_id')}}">
                <div class="buttons">
                    <button type="button">Preview</button>
                    <button type="submit">Next</button>
                </div>
            @else
                <div class="buttons updateprof">
                    <a class="unlist" href="{{route('business.make_unlist_profile')}}">Unlist Profile</a>
                    <button class="update" type="submit">Update Profile</button>
                </div>
            @endif

            </div>
        </div>
    </div>
</div>

@section('pagescript')
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH2uiqesOZEbWOdwb0YbnrO9pErqV28nk&libraries=places"></script>
<script type="text/javascript"> 

    $('.from-timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
     /*   minTime: '7:00am',
        maxTime: '11:00am',
        defaultTime: '10:00am',*/
       /* startTime: '10:00',*/
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $('.to-timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
      /*  minTime: '4:00pm',
        maxTime: '10:00pm',
        defaultTime: '7:00pm',*/
       /* startTime: '10:00',*/
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
</script>

<script>
  function countChar(val) {
    var len = val.value.length;
    $('#charNum').text(len);
  };
</script>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                jQuery('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#business_logo").change(function(){
        readURL(this);
        //jQuery('.previewimg').show();
        $('.filename').show();
        var file = $('#business_logo')[0].files[0].name;
        $('.filename').text(file);
    });
</script>

<script>
    function KeycheckOnlyNumeric(e)
    {

        var _dom = 0;
        _dom = document.all ? 3 : (document.getElementById ? 1 : (document.layers ? 2 : 0));
        if (document.all)
            e = window.event; // for IE
        var ch = '';
        var KeyID = '';
        if (_dom == 2) {                     // for NN4
            if (e.which > 0)
                ch = '(' + String.fromCharCode(e.which) + ')';
            KeyID = e.which;
        }
        else
        {
            if (_dom == 3) {                   // for IE
                KeyID = (window.event) ? event.keyCode : e.which;
            }
            else {                       // for Mozilla
                if (e.charCode > 0)
                    ch = '(' + String.fromCharCode(e.charCode) + ')';
                KeyID = e.charCode;
            }
        }
        if ((KeyID >= 65 && KeyID <= 90) || (KeyID >= 97 && KeyID <= 122) || (KeyID >= 33 && KeyID <= 47) || (KeyID >= 58 && KeyID <= 64) || (KeyID >= 91 && KeyID <= 96) || (KeyID >= 123 && KeyID <= 126) || (KeyID == 32))//changed by jshah for stopping spaces
        {
            return false;
        }
        return true;
    }
</script>

<script type="text/javascript">
    var upload_plusicon = "{{ asset('images/ic_profile_images_upload.png')}}";

//image 1 upload start
    $(document).on('click','#photo1',function(){
        $( "#image1" ).trigger( "click" );
    });
    var loadFile1 = function(event) {
        var output = document.getElementById('photo1');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo1').removeClass('upload');
            $('#remove1').css('display','block');
        }
    };
    $(document).on('click', '#remove1', function(){
        document.getElementById("image1").value=null; 
        var output = document.getElementById('photo1');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo1').addClass('upload');
            $('#remove1').css('display','none');
        }

        if($(this).data('business-id') != "undefined"){
            $.ajax({
                url : "{{route('business.remove_business_image')}}",
                data : { business_id: $(this).data('business-id')},
                dataType: 'json',
            }).done(function (data) { 
                
            });
        }
        
    });
//image 1 upload end

//image 2 upload start
    $(document).on('click','#photo2',function(){
        $( "#image2" ).trigger( "click" );
    });
    var loadFile2 = function(event) {
        var output = document.getElementById('photo2');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo2').removeClass('upload');
            $('#remove2').css('display','block');
        }
    };
    $(document).on('click', '#remove2', function(){
        document.getElementById("image2").value=null; 
        var output = document.getElementById('photo2');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo2').addClass('upload');
            $('#remove2').css('display','none');
        }

        if($(this).data('business-image-id') != "undefined"){
            $.ajax({
                url : "{{route('business.remove_business_image')}}",
                data : { business_image_id:$(this).data('business-image-id') },
                dataType: 'json',
            }).done(function (data) { 
                
            });
        }
    });
//image 2 upload end

//image 3 upload start
    $(document).on('click','#photo3',function(){
        $( "#image3" ).trigger( "click" );
    });
    var loadFile3 = function(event) {
        var output = document.getElementById('photo3');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo3').removeClass('upload');
            $('#remove3').css('display','block');
        }
    };
    $(document).on('click', '#remove3', function(){
        document.getElementById("image3").value=null; 
        var output = document.getElementById('photo3');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo3').addClass('upload');
            $('#remove3').css('display','none');
        }

        if($(this).data('business-image-id') != "undefined"){
            $.ajax({
                url : "{{route('business.remove_business_image')}}",
                data : { business_image_id:$(this).data('business-image-id') },
                dataType: 'json',
            }).done(function (data) { 
                
            });
        }
    });
//image 3 upload end

//image 4 upload start
    $(document).on('click','#photo4',function(){
        $( "#image4" ).trigger( "click" );
    });
    var loadFile4 = function(event) {
        var output = document.getElementById('photo4');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo4').removeClass('upload');
            $('#remove4').css('display','block');
        }
    };
    $(document).on('click', '#remove4', function(){
        document.getElementById("image4").value=null; 
        var output = document.getElementById('photo4');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo4').addClass('upload');
            $('#remove4').css('display','none');
        }

        if($(this).data('business-image-id') != "undefined"){
            $.ajax({
                url : "{{route('business.remove_business_image')}}",
                data : { business_image_id:$(this).data('business-image-id') },
                dataType: 'json',
            }).done(function (data) { 
                
            });
        }
    });
//image 4 upload end

//image 5 upload start
    $(document).on('click','#photo5',function(){
        $( "#image5" ).trigger( "click" );
    });
    var loadFile5 = function(event) {
        var output = document.getElementById('photo5');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo5').removeClass('upload');
            $('#remove5').css('display','block');
        }
    };
    $(document).on('click', '#remove5', function(){
        document.getElementById("image5").value=null; 
        var output = document.getElementById('photo5');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo5').addClass('upload');
            $('#remove5').css('display','none');
        }

        if($(this).data('business-image-id') != "undefined"){
            $.ajax({
                url : "{{route('business.remove_business_image')}}",
                data : { business_image_id:$(this).data('business-image-id') },
                dataType: 'json',
            }).done(function (data) { 
                
            });
        }
    });
//image 5 upload end
</script>

<!-- Jquery validate -->
<script type="text/javascript">
$(document).ready(function () {
    $('#update-business-details').validate({ 
        rules: {
            "business_name": {
                required: true
            },
            "category_id": {
                required: true
            },
            "business_summary": {
                required: true,
                wordCount: 5
            },
            "address": {
                required: true
            },
            "phone_no": {
                required: true,
                number: true
            },
            "email_for_contact_you": {
                required: true,
                email: true
            },
            "email_for_business_directory": {
                required: true,
                email: true
            },
            "services": {
                required: true,
                maxlength: 100
            },
        },
       /* errorPlacement: function(error, element){
            if (element.attr("name") == "Email" ){
                $(".form-error-msg").html( error ); 
            } else {
                $(".form-error-msg").html( 'Please fill in all the information.' );
            }
        },*/
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
    });

    $.validator.addMethod('wordCount', function(value, element, params) {
          var typedWords = jQuery.trim(value).split(' ').length; 
          return value == '' || (typedWords >= params);
    }, 'Minimum 50 words.');

});

set_location_in_map(23.026454661469973,72.5569747308441);

function set_location_in_map(latitude, longitude){
   
      var map;
      var marker;
      var myLatlng = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));
      var geocoder = new google.maps.Geocoder();
      var infowindow = new google.maps.InfoWindow();
      
      var mapOptions = {
        zoom: 5,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      
      //map = new google.maps.Map(document.getElementById("popupmap"), mapOptions);
      
      var input = document.getElementById('address');
      var searchBox = new google.maps.places.SearchBox(input);
      //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      
      // Bias the SearchBox results towards current map's viewport.
      /*map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });
      
      var markers = [];
      
      marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: true
      });
      map.setCenter(marker.getPosition());
      geocoder.geocode({'latLng': myLatlng }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            
            $('#address').val(results[0].formatted_address);
            $('#latitude').val(marker.getPosition().lat());
            $('#longitude').val(marker.getPosition().lng());
          }
        }
      });*/
      
      searchBox.addListener('places_changed', function(results, status) {
        var places = searchBox.getPlaces();
        
        if (places.length == 0) {
          return;
        }
        
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          
          //marker.setPosition(place.geometry.location);
          
          geocoder.geocode({'latLng': place.geometry.location}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                //alert(results[0]);
                $("#location").val('');
                $("#address").val('');
                $("#city").val('');
                $("#postcode").val('');
                $("#state").val('');
                $("#country").val('');
                lat = marker.getPosition().lat();
                lng = marker.getPosition().lng();
                $("#latitude").val(lat);
                $("#longitude").val(lng);
                
                arrAddress = results[0].address_components;
                for (ac = 0; ac < arrAddress.length; ac++) {
                  if (arrAddress[ac].types[0] == "street_number") { document.getElementById("location").value = arrAddress[ac].long_name }
                  if (arrAddress[ac].types[0] == "route") { document.getElementById("address").value = arrAddress[ac].long_name }
                  if (arrAddress[ac].types[0] == "locality") { document.getElementById("city").value = arrAddress[ac].long_name }
                  if (arrAddress[ac].types[0] == "postal_code") { document.getElementById("postcode").value = arrAddress[ac].long_name }
                  if (arrAddress[ac].types[0] == "administrative_area_level_1") { document.getElementById("state").value = arrAddress[ac].long_name; }
                  if (arrAddress[ac].types[0] == "country") { document.getElementById("country").value = arrAddress[ac].long_name; }
                }
              }
            }
          });
          
          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
       /* map.fitBounds(bounds);
        map.setZoom(17);
        map.setCenter(marker.getPosition());*/
        
      });
      
      /*google.maps.event.addListener(marker, 'dragend', function() {
        
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $("#location").val('');
              $("#address").val('');
              $("#city").val('');
              $("#postcode").val('');
              $("#state").val('');
              $("#country").val('');
              lat = marker.getPosition().lat();
              lng = marker.getPosition().lng();
              $("#latitude").val(lat);
              $("#longitude").val(lng);
              
              arrAddress = results[0].address_components;
              for (ac = 0; ac < arrAddress.length; ac++) {
                if (arrAddress[ac].types[0] == "street_number") { document.getElementById("location").value = arrAddress[ac].long_name }
                if (arrAddress[ac].types[0] == "route") { document.getElementById("address").value = arrAddress[ac].long_name }
                if (arrAddress[ac].types[0] == "locality") { document.getElementById("city").value = arrAddress[ac].long_name }
                if (arrAddress[ac].types[0] == "postal_code") { document.getElementById("postcode").value = arrAddress[ac].long_name }
                if (arrAddress[ac].types[0] == "administrative_area_level_1") { document.getElementById("state").value = arrAddress[ac].long_name; }
                if (arrAddress[ac].types[0] == "country") { document.getElementById("country").value = arrAddress[ac].long_name; }
              }
            }
          }
        });
      });*/
 }
</script>
@stop

@endsection