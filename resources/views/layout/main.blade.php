<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, user-scalable=0">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>WET Tropic Times</title>
        <!--Main Stylesheet-->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css')}}">

        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800" />
        <script src="{{ asset('js/jquery.min.js')}}"></script>
        <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
        <style type="text/css">
            .error{
                color: maroon !important;
            }
        </style>

        @yield('pagestylesheet')

    </head>
    <body>
        
        @if(Auth::check())
            <div class="header loginheader">
                <div class="sitecontainer">
                    <div class="logo">
                        <a href="{{Config::get('constants.app.wp-url')}}"><img src="{{ asset('images/logo_header.png')}}"></a>
                    </div>
                    <div class="myaccount">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                                <img src="{{ asset('images/ic_header_login.png')}}">
                                {{Auth::user()->displayname}}
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{route('user.my_account')}}">My Account</a></li>
                              @if(!empty(Auth::user()->business_id))
                                <li><a href="{{route('business.edit_business_details')}}">
                              Business Details</a></li>
                              @endif
                              @if(!empty($subscription) && $subscription->status == 'cancelled')
                                <li><a href="{{route('user.subscriptions')}}?flag=renew">Subscriptions</a></li>
                              @else
                                <li><a href="{{route('user.subscriptions')}}">Subscriptions</a></li>
                              @endif
                              
                              <li class="logout"><a href="{{route('logout')}}">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                @if(Auth::check())
                    <div class="myaccount">
                        <div class="dropdown">
                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                                <img src="{{ asset('images/ic_header_login.png')}}">
                                {{Auth::user()->displayname}}
                            </button>
                            <ul class="dropdown-menu">
                              <li><a href="{{route('user.my_account')}}">My Account</a></li>
                              @if(!empty(Auth::user()->business_id))
                                <li><a href="{{route('business.edit_business_details')}}">Business Details</a></li>
                              @endif
                              <li><a href="{{route('user.subscriptions')}}">Subscriptions</a></li>
                              <li class="logout"><a href="{{route('logout')}}">Logout</a></li>
                            </ul>
                        </div>
                    </div>
                @else
                    <div class="login">
                        <a href="{{route('user.login')}}"><img src="{{ asset('images/ic_header_login.png')}}"> Login</a>
                    </div>
                @endif

                <ul class="navmenu">
                    <li class="active"><a href="#">News</a></li>
                    <li><a href="{{Config::get('constants.app.wp-url').'/our-local-areas/'}}">Local Areas</a></li>
                    <li><a href="{{Config::get('constants.app.wp-url').'/business-listing/'}}">Business Directory</a></li>
                    <li><a href="{{Config::get('constants.app.wp-url').'/our-story/'}}">About Us</a></li>
                </ul>

                <div class="downloadapp">
                    <label>Download app</label> 
                    <a href="#"><img src="{{ asset('images/ic_header_apple.png')}}"></a>
                    <a href="#"><img src="{{ asset('images/ic_header_play_store.png')}}"></a>
                </div>  
            </div>

            <div class="header">
                <div class="headertop">
                    <div class="leftside">
                        <div class="contact">
                            <a href="{{Config::get('constants.app.wp-url').'/contact-us/'}}"><img src="{{ asset('images/ic_header_contactus.png')}}"> Contact Us</a>
                        </div>
                           
                        <div class="social">
                            <ul>
                                <li><a href="https://www.facebook.com/wettropictimes/" target="_blank"><img src="{{ asset('images/ic_header_facebook.png')}}"></a></li>
                                <li><a href="https://www.instagram.com/wettropictimes/" target="_blank"><img src="{{ asset('images/ic_header_instagram.png')}}"></a></li>
                                <li><a href="https://twitter.com/wettropictimes" target="_blank"><img src="{{ asset('images/ic_header_twitter.png')}}"></a></li>
                                <li><a href="https://au.linkedin.com/company/wet-tropic-times" target="_blank"><img src="{{ asset('images/ic_header_linkedin.png')}}"></a></li>
                            </ul>
                        </div>  
                        <div class="downloadapp">
                            <label>Download app</label> 
                            <a href="#"><img src="{{ asset('images/ic_header_apple.png')}}"></a>
                            <a href="#"><img src="{{ asset('images/ic_header_play_store.png')}}"></a>
                        </div>  
                    </div>
                    <div class="rightside">
                        <div class="address">
                            <img src="{{ asset('images/ic_header_location.png')}}">
                            <label>Clayton <br> <a href="#">Change</a></label>
                        </div>
                        <div class="weather">
                            <img src="{{ asset('images/ic_header_weather.png')}}">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">26C Max <br> Min of 15
                                <i class="fa fa-chevron-down"></i></button>
                                <ul class="dropdown-menu">
                                  <li><a href="#">15</a></li>
                                  <li><a href="#">20</a></li>
                                  <li><a href="#">25</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="login">
                            @if(Auth::check())
                            <a href="javascript:void(0);" class="dropdown-toggle" type="button" data-toggle="dropdown"><img src="{{ asset('images/ic_header_login.png')}}"> {{Auth::user()->displayname}}</a>
                            <ul class="dropdown-menu home-dd-menu">
                              <li><a href="{{route('user.my_account')}}">My Account</a></li>
                              @if(!empty(Auth::user()->business_id))
                                <li><a href="{{route('business.edit_business_details')}}">Business Details</a></li>
                              @endif
                              <li><a href="{{route('user.subscriptions')}}">Subscriptions</a></li>
                              <li class="logout"><a href="{{route('logout')}}">Logout</a></li>
                            </ul>
                            @else
                            <a href="{{route('user.login')}}"><img src="{{ asset('images/ic_header_login.png')}}"> Login</a>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="headermiddle">
                    <span class="togglesidemenu" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></span>
                    <div class="sitecontainer">
                        <div class="logo">
                            <a href="{{Config::get('constants.app.wp-url')}}"><img src="{{ asset('images/logo_header.png')}}"></a>
                        </div>
                        <div class="menus">
                            <ul class="text-center">
                                <li class="{{(Request::route()->getName() == 'user.community') || (Request::route()->getName() == 'user.view_video') ? 'active' : ''}}"><a href="#">News</a></li>
                                <li class="{{(Request::route()->getName() == 'user.local_area' || Request::route()->getName() == 'user.view_areas') ? 'active' : ''}}"><a href="{{Config::get('constants.app.wp-url').'/our-local-areas/'}}">Local Areas</a></li>
                                <li class="{{(Request::route()->getName() == 'user.business_listing') || (Request::route()->getName() == 'business.find_business') || (Request::route()->getName() == 'business.search_results') || (Request::route()->getName() == 'business.view_business') || (Request::route()->getName() == 'business.view_business_partial')  ? 'active' : ''}}"><a href="{{Config::get('constants.app.wp-url').'/business-listing/'}}">Business Directory</a></li>
                                <li><a href="{{Config::get('constants.app.wp-url').'/our-story/'}}">About Us</a></li>
                            </ul>
                        </div>
                        <div class="search">
                            <img src="{{ asset('images/ic_header_search.png')}}">
                        </div>
                    </div>
                </div>
                @if(Request::route()->getName() != 'business.business_listing' && Request::route()->getName() != 'user.change_password' && Request::route()->getName() != 'check_your_inbox' && Request::route()->getName() != 'create_your_account' && Request::route()->getName() != 'business.find_business' && Request::route()->getName() != 'forget_password'  &&  Request::route()->getName() != 'user.login' && Request::route()->getName() != 'user.my_account' && Request::route()->getName() != 'business.search_results' && Request::route()->getName() != 'user.subscribe' && Request::route()->getName() != 'business.view_business_partial' && Request::route()->getName() != 'business.view_business')
                <div class="headerbottom">
                    <div class="sitecontainer">
                        <div class="categorymenus">
                            <ul class="desktopcategorymenus">
                                <li class="{{(Request::route()->getName() == 'user.community') ? 'active' : ''}}"><a href="{{Config::get('constants.app.wp-url').'/back_end/category/community/'}}">Community</a></li>
                                <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/schools/'}}">Schools</a></li>
                                <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/sports/'}}">Sports</a></li>
                                <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/classifieds/'}}">Classifieds</a></li>
                                <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/local-events-activities/'}}">Local Events & Activities</a></li>
                                <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/rural/'}}">Rural</a></li>
                                <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/state/'}}">State</a></li>
                                <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/news-in-brief/'}}">News in Brief</a></li>
                                <li class="{{(Request::route()->getName() == 'user.videos') ? 'active' : ''}}"><a href="{{Config::get('constants.app.wp-url').'/back_end/category/videos/'}}">Videos</a></li>
                               <!--  <li class="{{(Request::route()->getName() == 'user.audio') ? 'active' : ''}}"><a href="{{Config::get('constants.app.wp-url').'/back_end/category/community/'}}">Podcasts</a></li> -->
                            </ul>
                            <div class="mobilecategorymenus dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Categories
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li class="{{(Request::route()->getName() == 'user.community') ? 'active' : ''}}"><a href="{{Config::get('constants.app.wp-url').'/back_end/category/community/'}}">Community</a></li>
                                    <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/schools/'}}">Schools</a></li>
                                    <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/sports/'}}">Sports</a></li>
                                    <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/classifieds/'}}">Classifieds</a></li>
                                    <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/local-events-activities/'}}">Local Events & Activities</a></li>
                                    <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/rural/'}}">Rural</a></li>
                                    <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/state/'}}">State</a></li>
                                    <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/news-in-brief/'}}">News in Brief</a></li>
                                    <li class="{{(Request::route()->getName() == 'user.videos') ? 'active' : ''}}"><a href="{{Config::get('constants.app.wp-url').'/back_end/category/videos/'}}">Videos</a></li>
                                   <!--  <li class="{{(Request::route()->getName() == 'user.audio') ? 'active' : ''}}"><a href="#">Podcasts</a></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @elseif(Request::route()->getName() == 'user.local_distributor' || Request::route()->getName() == 'user.local_distributor_selected_closed' || Request::route()->getName() == 'user.local_distributor_selected_open' || Request::route()->getName() == 'user.our_team')
                 <div class="headerbottom">
                    <div class="sitecontainer">
                        <div class="categorymenus">
                            <ul class="desktopcategorymenus">
                                <li ><a href="#">Our Story</a></li>
                                <li class="{{(Request::route()->getName() == 'user.our_team') ? 'active' : ''}}"><a href="#">Meet The Team</a></li>
                                <li class="{{(Request::route()->getName() == 'user.local_distributor') ? 'active' : ''}}"><a href="#">Distribution List</a></li>
                            </ul>

                            <div class="mobilecategorymenus dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Categories
                                <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Our Story</a></li>
                                    <li class="{{(Request::route()->getName() == 'user.our_team') ? 'active' : ''}}"><a href="#">Meet The Team</a></li>
                                    <li class="{{(Request::route()->getName() == 'user.local_distributor') ? 'active' : ''}}"><a href="#">Distribution List</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        @endif

        @yield('content')

    <div class="footer">
            <div class="sitecontainer">
                <div class="footertop">
                    <div class="widget aboutfooter">
                        <h3>Wet Tropic Times</h3>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                        </p>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum headings been the industry's standard dummy text ever since the 1500s, 
                        </p>
                    </div>
                    <div class="widget wttnews">
                        <h4>WTT News</h4>
                        <ul>
                            <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/schools/'}}">Schools</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/sports/'}}">Sports</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/classifieds/'}}">Classifieds</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/local-events-activities/'}}">Local Events & Activities</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/rural/'}}">Rural</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/state/'}}">State</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/news-in-brief/'}}">News in Brief</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/back_end/category/videos/'}}">Videos</a></li>
                            <!-- <li><a href="#">Podcasts</a></li> -->
                        </ul>
                    </div>
                    <div class="widget localarea">
                        <h4>Local Areas</h4>
                          <ul id="menu-local-area" class="">
                            <li id="menu-item-1418" class="menu-item menu-item-type-post_type menu-item-object-local_area menu-item-1418"><a href="{{Config::get('constants.app.wp-url').'/back_end/local_area/lower-tully-and-feluga-3/'}}">TULLY, LOWER TULLY AND FELUGA</a></li>
                            <li id="menu-item-1419" class="menu-item menu-item-type-post_type menu-item-object-local_area menu-item-1419"><a href="{{Config::get('constants.app.wp-url').'/back_end/local_area/discover-innisfail-mourilyan-south-johnstone-and-mundoo/'}}">INNISFAIL, MOURILYAN, SOUTH JOHNSTONE AND MUNDOO</a></li>
                            <li id="menu-item-1420" class="menu-item menu-item-type-post_type menu-item-object-local_area menu-item-1420"><a href="{{Config::get('constants.app.wp-url').'/back_end/local_area/lower-tully-and-feluga-2/'}}">CARDWELL, KENNEDY AND MURRAY UPPER</a></li>
                            <li id="menu-item-1421" class="menu-item menu-item-type-post_type menu-item-object-local_area menu-item-1421"><a href="{{Config::get('constants.app.wp-url').'/back_end/local_area/mission-beach-el-arish-and-silkwood/'}}">MISSION BEACH, EL ARISH AND SILKWOOD</a></li>
                        </ul>
                        <!-- <p>
                            Tully, Lower Tilly & Feluga <br>
                            Innisfail, Mourilyan, South Johnstone & Mundoo <br>
                            Cardwell, Kennedy & Murray Upper <br>
                            Mission Beach, EL Arish & Silkwood
                        </p> -->
                        <h4>Other</h4>
                        <ul>
                            <li><a href="{{Config::get('constants.app.wp-url').'/local-distributor/'}}">Distribution List</a></li>
                            <li><a href="#">Advertise with WTT</a></li>
                        </ul>
                    </div>
                    <div class="widget downloadapp">
                        <h4>Download app</h4>
                        <a class="applestore" href="#"><img src="{{ asset('images/ic_header_apple.png')}}"></a>
                        <a href="#"><img src="{{ asset('images/ic_header_play_store.png')}}"></a>
                        <ul>
                            <li><a href="{{Config::get('constants.app.wp-url').'/terms-conditions'}}">Terms & Conditions</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/privacy-policy'}}">Privacy Policy</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/our-story'}}">About WTT</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/contact-us'}}">Contact US</a></li>
                            <li><a href="{{Config::get('constants.app.wp-url').'/our-local-areas'}}">Local Areas</a></li>
                        </ul>
                    </div>
                </div>

                <div class="footerbottom">
                    <div class="logo">
                        <a href="{{Config::get('constants.app.wp-url')}}"><img src="{{ asset('images/logo_footer.png')}}"></a>
                    </div>
                    <div class="copyright">
                        <p>2020 © WTT | All rights Reserved</p>
                    </div>
                    <div class="social">
                        <ul>
                            <li><a href="https://www.facebook.com/wettropictimes/" target="_blank"><img src="{{ asset('images/ic_header_facebook.png')}}"></a></li>
                            <li><a href="https://www.instagram.com/wettropictimes/"target="_blank"><img src="{{ asset('images/ic_header_instagram.png')}}"></a></li>
                            <li><a href="https://twitter.com/wettropictimes" target="_blank"><img src="{{ asset('images/ic_header_twitter.png')}}"></a></li>
                            <li><a href="https://au.linkedin.com/company/wet-tropic-times" target="_blank"><img src="{{ asset('images/ic_header_linkedin.png')}}"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </body>

    <!--jQuery JS-->
    <script src="{{ asset('js/jquery.min.js')}}"></script>
    <script src="{{ asset('js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('js/jquery.validate.min.js')}}"></script>

    @yield('pagescript')

    <script>
        function openNav() {
          document.getElementById("mySidenav").style.width = "100%";
        }
        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
        }
    </script> 
</html>


