@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 
 <div class="subscriptionsplans">

    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="{{route('user.subscriptions')}}">Subscriptions</a> <span>/</span> Cancel your plan 
        </div>
    </div>
    
    <div class="subscriptionsplansinner">
        <form method="post" action="{{route('user.cancel_subscription_from_stripe')}}">
        @csrf

        <input type="hidden" name="subscription_id" value="{{Request::get('subscription_id')}}">
        <div class="subscriptionsbox">
            <h2>Cancel Your Plan</h2>
            <h4>Current Plan</h4>
            <div class="plandetails">
                 <div class="planinfo renewplan">
                    <label>{{Request::get('plan_title')}}</label>
                    <span>{{Request::get('plan_desc')}}</span>
                    <p>Your plan will be cancelled, but is still available until the end of your billing 
                    period on {{date("d M Y", (Request::get('end_period')))}}.</p>

                    <p style="padding-top: 25px;">If you change your mind, you can renew your subscription.</p>
                 </div>
            </div>
            <div class="buttons cancelplan text-center">
                <a class="goback" href="{{route('user.subscriptions')}}">Go Back</a>
                <button type="submit">Cancel Plan</button>
            </div>
        </div>
     </form>
    </div>
</div>
@section('pagescript')

@stop

@endsection