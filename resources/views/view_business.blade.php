@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css')}}">
@stop
 <div class="viewareabreadcrumb">
    <div class="sitecontainer">
        <div class="viewareabreadcrumbinner">
            <h1>Cheap Orange Country</h1>
            <p>We’ll help protect the things that matter</p>
            <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>
        </div>
    </div>
</div>

<div class="viewareagallery">
    <div class="sitecontainer">
        <div class="viewareagalleryitems">
            <div class="itemleft">
                <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image1.png')}}">
                    </div>
                </div>
            </div>
            <div class="itemright">
                <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image2.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image3.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image4.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <div class="layer"></div>
                        <label>+5 Photos</label>
                        <img src="{{ asset('images/img_view_areas_image5.png')}}">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="bussinesscontactinfo">

    <div class="sitecontainer">
        <div class="items">
            <div class="item text-center">
                <img src="{{ asset('images/ic_header_location.png')}}">
                <p>195 Wellington Road, Clayton, VIC, 3168</p>
            </div>
            <div class="item text-center">
                <img src="{{ asset('images/ic_distributor_phone.png')}}">
                <p>039 985 562</p>
            </div>  
            <div class="item text-center">
                <img src="{{ asset('images/email.png')}}">
                <p>businessname@email.com</p>
            </div> 
            <div class="item text-center">
                <p><span>Closed</span> Opens Wednesday 2PM</p>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                        <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                    </button>
                    <ul class="dropdown-menu">
                      <li>Thursday <span>2-10PM</span></li>
                      <li>Friday <span>2-10PM</span></li>
                      <li>Saturday <span>2-10PM</span></li>
                      <li>Sunday <span>2-10PM</span></li>
                      <li>Monday <span>2-10PM</span></li>
                      <li>Tuesday <span>2-10PM</span></li>
                      <li class="closemenu">Wednesday <span>Closed</span></li>
                    </ul>
                </div>
            </div> 
        </div>
    </div>
</div>


<div class="viewareacontent">
    <div class="sitecontainer">
        <h3>Services</h3>
        <p>This a single line description of the business.</p>

        <h3>Business Details</h3>
        <p>
            NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live. 
        </p>

        <div class="socialshare">
            <div class="items">
                <ul>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_facebook.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_twitter.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_linkedin.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_youtube.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_website.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business listing_message.png')}}"></a></li>
                </ul>
            </div>
        </div>

    </div>
</div>


<div class="recomndedbusiness">
    <div class="sitecontainer">
        <div class="busenesslisting">
            <h2>Recommended Businesses</h2>
            <div class="sitecontainer">
                <div class="businessslider">
                    <div class="owl-carousel owl-business owl-theme">
                        <div class="item">
                            <div class="iteminner">
                                <img src="{{ asset('images/businesses1.png')}}">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                                <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="iteminner">
                                <img src="{{ asset('images/businesses2.png')}}">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                                <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="iteminner">
                                <img src="{{ asset('images/businesses3.png')}}">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                                <span><img src="images/ic_distributor_phone.png"> 03 9001 5805</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="iteminner">
                                <img src="{{ asset('images/businesses4.png')}}">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                                <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                            </div>
                        </div>
                        <div class="item">
                            <div class="iteminner">
                                <img src="{{ asset('images/businesses4.png')}}">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                                <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('pagescript')
<script src="{{ asset('js/slick.min.js')}}"></script>
 <script>
    var owl = $('.owl-business');
    owl.owlCarousel({
        loop:true,
        nav:true, 
        margin:20,
        autoplay:true,
        dots: true,
        items:4,
        responsive:{
           0:{
               items:1
           },
           550:{
               items:2
           },
           700:{
               items:2
           },            
           991:{
               items:3
           },
           1200:{
               items:4 // Row in content item set
           }
       }
    });
    $( ".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $( ".owl-next").html('<i class="fa fa-angle-right"></i>');
</script>
@stop

@endsection