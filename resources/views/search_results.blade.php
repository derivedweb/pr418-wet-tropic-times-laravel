@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 <div class="localdistributers">
    <div class="directorymaplisting searchresults">
        <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>
        <div class="sitecontainer">
            <h1>Find a Business</h1>
            <div class="distributerlisting">
                <div class="distributeritems">
                    <div class="distributeritemsinner">
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('images/businesses1.png')}}">
                            </div>
                            <div class="content">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, 
                                Bentleigh East, VIC</h4>
                                <div class="info">
                                    <img src="{{ asset('images/ic_distributor_phone.png')}}">
                                    <label>03 9001 5805</label>
                                    <span>Open &nbsp;<b>Closes 4PM</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('images/businesses2.png')}}">
                            </div>
                            <div class="content">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, 
                                Bentleigh East, VIC</h4>
                                <div class="info">
                                    <img src="{{ asset('images/ic_distributor_phone.png')}}">
                                    <label>03 9001 5805</label>
                                    <span>Open &nbsp;<b>Closes 4PM</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('images/businesses3.png')}}">
                            </div>
                            <div class="content">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, 
                                Bentleigh East, VIC</h4>
                                <div class="info">
                                    <img src="{{ asset('images/ic_distributor_phone.png')}}">
                                    <label>03 9001 5805</label>
                                    <span>Open &nbsp;<b>Closes 4PM</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('images/businesses4.png')}}">
                            </div>
                            <div class="content">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, 
                                Bentleigh East, VIC</h4>
                                <div class="info">
                                    <img src="{{ asset('images/ic_distributor_phone.png')}}">
                                    <label>03 9001 5805</label>
                                    <span>Open &nbsp;<b>Closes 4PM</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('images/businesses5.png')}}">
                            </div>
                            <div class="content">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, 
                                Bentleigh East, VIC</h4>
                                <div class="info">
                                    <img src="{{ asset('images/ic_distributor_phone.png')}}">
                                    <label>03 9001 5805</label>
                                    <span>Open &nbsp;<b>Closes 4PM</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('images/businesses6.png')}}">
                            </div>
                            <div class="content">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, 
                                Bentleigh East, VIC</h4>
                                <div class="info">
                                    <img src="{{ asset('images/ic_distributor_phone.png')}}">
                                    <label>03 9001 5805</label>
                                    <span>Open &nbsp;<b>Closes 4PM</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="image">
                                <img src="{{ asset('images/businesses7.png')}}">
                            </div>
                            <div class="content">
                                <h3>Cheap Orange Country</h3>
                                <p>This a single line description…</p>
                                <h4>Unit 6/20 Duerdin Street, 
                                Bentleigh East, VIC</h4>
                                <div class="info">
                                    <img src="{{ asset('images/ic_distributor_phone.png')}}">
                                    <label>03 9001 5805</label>
                                    <span>Open &nbsp;<b>Closes 4PM</b></span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="searchdirectory">
                    <div class="searchbox">
                        <input type="text" name="" placeholder="Melbourne VIC, Australia">
                        <img src="{{ asset('images/ic_location_search.png')}}">
                    </div>
                    <div class="searchfields">
                        <div class="field">
                            <div class="select">
                                <select>
                                    <option>All Regions</option>
                                    <option>Regions1</option>
                                    <option>Regions2</option>
                                    <option>Regions3</option>
                                </select>
                                <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                            </div>
                        </div>
                        <div class="field">
                            <div class="select">
                                <select>
                                    <option>All Categories</option>
                                    <option>Categories1</option>
                                    <option>Categories2</option>
                                    <option>Categories3</option>
                                </select>
                                <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                            </div>
                        </div>
                        <div class="field">
                            <button type="submit">Update</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="directorymap">
         <div id="dvMap"></div>
    </div>

</div>
@section('pagescript')
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
     <script type="text/javascript">
        var markers = [
        {
            "title": 'News on Avenue',
            "lat": '-37.5400771',
            "lng": '144.9731875',
            "description": '<img src="images/directory1.png"><h3>News on Avenue</h3><h4>Unit 6/20 Duerdin Street, Bentleigh East VIC</h4><div class="phone"><img src="images/ic_distributor_phone.png"><label>03 9001 5805</label></div><span>Open <b>Closes 4PM</b></span>  '
        },
        ];
        window.onload = function () {
            LoadMap();
        }
        function LoadMap() {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
     
            //Create and open InfoWindow.
            var infoWindow = new google.maps.InfoWindow();
            var index=0;
            for (var i = 0; i < markers.length; i++) {
                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title,
                    icon:'images/ic_map_pin_selected.png?i='+(index++),
                });
     
                //Attach click event to the marker.
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                        infoWindow.setContent("<div class='dirbox'>" + data.description + "</div>");
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
    </script>

    <script>
        function openNav() {
          document.getElementById("mySidenav").style.width = "100%";
        }
        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
        }
    </script> 

    <script>
        var owl = $('.owl-herobanner');
        owl.owlCarousel({
            loop:true,
            nav:false, 
            margin:0,
            autoplay:false,
            dots: true,
            items:1,
            touchDrag : false,
            mouseDrag : false
        });
        
        $('.owl-dot').click(function () {
             var owl = $(".owl-herobanner");
             owl.trigger('to.owl.carousel', [$(this).index(), 300]);

             $('.heroslider .sliderdots .item.active').removeClass('active');
             $(this).addClass('active');
        });
    </script>

    <script>
        $(document).ready(function(){
          $(".regiontopstores .storieslisting .item").slice(0, 8).show();
          $("#loadMore").on("click", function(e){
            e.preventDefault();
            $(".regiontopstores .storieslisting .item:hidden").slice(0, 4).slideDown();
            if($(".regiontopstores .storieslisting .item:hidden").length == 0) {
              $("#loadMore").text("").addClass("noContent");
            }
          });
        })
    </script>
@stop

@endsection