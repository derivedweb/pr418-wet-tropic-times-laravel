@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 <div class="subscriptionsplans">

    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="{{route('user.subscriptions')}}">Subscriptions</a> <span>/</span> Update your plan 
        </div>
    </div>
    
    <div class="subscriptionsplansinner">
         <form method="post" action="{{ route('user.renew_plan_from_stripe') }}">
            @csrf
        <div class="subscriptionsbox">
            <h2>Renew Your Plan</h2>
            <h4>Current Plan</h4>
            <div class="plandetails">
                 <div class="planinfo renewplan">
                    <label>{{Request::get('plan_title')}}</label>
                    <span>{{Request::get('plan_desc')}}</span>
                    <p>This plan will no longer be cancelled. It will renew on {{date("d M Y", (Request::get('end_period')))}}.</p>
                 </div>
            </div>
            <div class="buttons text-center">
                <a class="goback" href="{{route('user.subscriptions')}}?flag=renew">Go Back</a>
                <button type="submit">Renew Plan</button>
            </div>
        </div>
        </form>
    </div>
</div>
@section('pagescript')

@stop

@endsection