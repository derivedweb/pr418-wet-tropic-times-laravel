@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 <div class="localdistributers">
    <div class="directorymaplisting searchresults">
        <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>
        <div class="sitecontainer">
            <h1>Find a Business</h1>
            <div class="distributerlisting">
                <div class="distributeritems">
                    <div class="distributeritemsinner">
                        @if (count($businesses) > 0)
                         <div class="promotionlisting job">

                            @include('business.load_business')

                        </div>
                        @else
                            <p style="text-align: center;">No Businesses Found</p>
                        @endif
                    </div>
                </div>
                <div class="searchdirectory">
                    <form action="{{ route('business.search_results') }}" id="search-business-form" method="post">
                        @csrf
                    
                    <div class="searchbox">
                        <input type="text" placeholder="Melbourne VIC, Australia" name="whatyoulookingfor" id="whatyoulookingfor" value="{{isset($data['postData']['whatyoulookingfor']) ? $data['postData']['whatyoulookingfor'] : ''}}">
                        <img src="{{ asset('images/ic_location_search.png')}}">
                    </div>
                    <div class="searchfields">
                        <div class="field">
                            <div class="select">
                                <select name="region_id" id="region_id"> 
                                    <option value="">All Regions</option>
                                    @if(isset($data['regions']) && count($data['regions']) > 0)
                                        @foreach($data['regions'] as $region)
                                            @if(isset($data['postData']['region_id']) && $region->region_id == $data['postData']['region_id'])
                                                <option value="{{$region->region_id}}" selected>{{$region->region_name}}</option>
                                            @else
                                                <option value="{{$region->region_id}}">{{$region->region_name}}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                                <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                            </div>
                        </div>
                        <div class="field">
                            <div class="select">
                                <select name="category_id" id="category_id">
                                    <option value="">All Categories</option>
                                    @if(isset($data['categories']) && count($data['categories']) > 0)
                                        @foreach ($data['categories'] as $category) 
                                            @if(isset($data['postData']['category_id']) && $category->category_id == $data['postData']['category_id'])
                                                    <option value="{{$category->category_id}}" selected>{{$category->category_name}}</option>
                                                @else
                                                    <option value="{{$category->category_id}}">{{$category->category_name}}</option>
                                                @endif
                                        @endforeach
                                    @endif
                                </select>
                                <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                            </div>
                        </div>
                        <div class="field">
                            <button type="submit">Update</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>

    <div class="directorymap">
         <div id="dvMap"></div>
    </div>

</div>
@section('pagescript')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH2uiqesOZEbWOdwb0YbnrO9pErqV28nk&sensor=false"></script>
     <script type="text/javascript">
        var markers = [];
        <?php if (!empty($businesses_map_markers)) {?> 
        markers = [
        <?php foreach ($businesses_map_markers as $key => $marker) { 
            $business_html = "";
            if(!empty($marker->business_time)){
                $time = explode("-", $marker->business_time);
                if(date("H:i:s") > $time[0] && date("H:i:s") < $time[1]){ 
                    $business_html = '<span>Open &nbsp;<b>Closes '.date("hA", strtotime($time[1])).'</b></span>';
                } else { 
                    $business_html = '<span style="color:red;">Closed &nbsp;<b></b></span>';
                } 
             } else { 
                   $business_html = '<span style="color:red;">Closed</span>';
             } 
        ?>
            {
                "business_id": '{{$marker->business_id}}',
                "title": '{{$marker->business_name}}',
                "lat": '{{$marker->latitude}}',
                "lng": '{{$marker->longitude}}',
                "description": '<img src="{{ asset('business_images/'.$marker->image)}}"><h3>{{$marker->business_name}}</h3><h4>{{$marker->address}}</h4><div class="phone"><img src="{{ asset('images/ic_distributor_phone.png')}}"><label>{{$marker->phone_no}}</label></div><span><?=$business_html?></span>  '
            },
        <?php } ?>
        ];
 <?php } ?>
        /*var markers = [
        {
            "title": 'News on Avenue',
            "lat": '-37.5400771',
            "lng": '144.9731875',
            "description": '<img src="images/directory1.png"><h3>News on Avenue</h3><h4>Unit 6/20 Duerdin Street, Bentleigh East VIC</h4><div class="phone"><img src="images/ic_distributor_phone.png"><label>03 9001 5805</label></div><span>Open <b>Closes 4PM</b></span>  '
        },
        ];*/
        window.onload = function () {
            LoadMap();
        }
        function LoadMap() {
            if(markers.length == 0){
                var mapOptions = {
                    center: new google.maps.LatLng('-37.5400771', '144.9731875'),
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                };
            } else {
                var mapOptions = {
                    center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                };
            }
            
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
     
            //Create and open InfoWindow.
            var infoWindow = new google.maps.InfoWindow();
            var index=0;
            for (var i = 0; i < markers.length; i++) {
                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: 'business-id-'+data.business_id,
                    icon:'{{ asset('images/ic_map_pin_selected.png')}}?i='+(index++),
                });
     
                //Attach click event to the marker.
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.

                        infoWindow.setContent("<a href='view-business/"+data.business_id+"' target='_blank'><div class='dirbox'>" + data.description + "</div></a>");
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
            google.maps.event.addListener(map, "click", function(event) {
               infoWindow.close();
            });
        }

        $(document).on("click", ".item", function(){
            $("div[title='business-id-"+$(this).data('b-id')+"']").trigger("click");
        });
    </script>

    <script>
        function openNav() {
          document.getElementById("mySidenav").style.width = "100%";
        }
        function closeNav() {
          document.getElementById("mySidenav").style.width = "0";
        }
    </script> 

    <script>
        var owl = $('.owl-herobanner');
        owl.owlCarousel({
            loop:true,
            nav:false, 
            margin:0,
            autoplay:false,
            dots: true,
            items:1,
            touchDrag : false,
            mouseDrag : false
        });
        
        $('.owl-dot').click(function () {
             var owl = $(".owl-herobanner");
             owl.trigger('to.owl.carousel', [$(this).index(), 300]);

             $('.heroslider .sliderdots .item.active').removeClass('active');
             $(this).addClass('active');
        });
    </script>

    <script>
        $(document).ready(function(){
          $(".regiontopstores .storieslisting .item").slice(0, 8).show();
          $("#loadMore").on("click", function(e){
            e.preventDefault();
            $(".regiontopstores .storieslisting .item:hidden").slice(0, 4).slideDown();
            if($(".regiontopstores .storieslisting .item:hidden").length == 0) {
              $("#loadMore").text("").addClass("noContent");
            }
          });

           /*$('body').on('click', '.pagination a', function(e) {
                e.preventDefault();
                var url = $(this).attr('href');
                getBusinesses(url);
                window.history.pushState("", "", url);
            });*/

            $('.distributeritemsinner').bind('scroll', function(){
                if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight)
                {
                  
                  if(parseInt($(".last_page:last").val()) > parseInt($(".current_page:last").val())){   

                        var url = $(this).attr('href');
                        var region_id = $("#region_id").val();
                        var category_id = $("#category_id").val();
                        var whatyoulookingfor = $("#whatyoulookingfor").val();
                        var page_number = parseInt($(".current_page:last").val());

                        page_number += 1;
                        $.ajax({
                            url : url,
                            data : { region_id: region_id, category_id:category_id, whatyoulookingfor:whatyoulookingfor, page:page_number },
                            dataType: 'json',
                        }).done(function (data) { 

                            $('.promotionlisting.job').append(data.body);
                            
                        });
                    }
                }
            });

           function getBusinesses(url) 
           {
                var region_id = $("#region_id").val();
                var category_id = $("#category_id").val();
                var whatyoulookingfor = $("#whatyoulookingfor").val();
                
                $.ajax({
                    url : url,
                    data : { region_id: region_id, category_id:category_id, whatyoulookingfor:whatyoulookingfor },
                    dataType: 'json',
                }).done(function (data) { 

                    $('.promotionlisting.job').html(data.body);
                    
                });
            }
        })
    </script>
@stop

@endsection