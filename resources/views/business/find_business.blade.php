@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop

<div class="finbusiness">
    <div class="layer"></div>

    <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>

    <div class="sitecontainer">
        <div class="findbusinessform text-center">
            <h1>Find a Business</h1>
            <p>Wet Tropic Times Directory Helps You Find Your Business </p>
            <div class="form">
                {!! Form::open(array('route' => 'business.search_results','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'search_results-form')) !!}
                    <div class="formfield">
                        <div class="field">
                            <input type="text" name="whatyoulookingfor" id="whatyoulookingfor" placeholder="What are you looking for?">
                        </div>
                        <div class="field">
                            <div class="select">
                                <select name="region_id" id="region_id" required="">
                                    <option value="">All Regions</option>
                                    @if(isset($data['regions']) && count($data['regions']) > 0)
                                        @foreach($data['regions'] as $region)
                                            <option value="{{$region->region_id}}">{{$region->region_name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                            </div>
                        </div>
                        <div class="field">
                            <div class="select">
                                <select name="category_id" id="category_id" required="">
                                    <option value="">Select category</option>
                                    @foreach ($data['categories'] as $value) 
                                        <option value="{{$value->category_id}}">{{$value->category_name}}</option>
                                    @endforeach
                                </select>
                                <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                            </div>
                        </div>
                    </div>
                    <div class="button">
                        <button type="submit">Search</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="finbusinessmain">
    <div class="busenessheadding">
        <h2>Featured Businesses</h2>
    </div>
    <div class="busenesslisting">
        <div class="sitecontainer">
            @if(isset($data['businesses']) && count($data['businesses']) > 0)
                @foreach($data['businesses'] as $business)
                    <a href="{{route('business.view_business',$business->business_id)}}">
                        <div class="item">
                            <div class="iteminner">
                                <img src="{{ asset('business_images/thumb')}}/{{$business->business_logo}}">
                                <h3>{{$business->business_name}}</h3>
                                <p>{{$business->services}}</p>
                                <h4>{{$business->address}}</h4>
                                <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> {{$business->phone_no}}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            @else
                <p style="text-align: center;">No Featured Businesses Found</p>
            @endif
        </div>
    </div>

    <div class="busenessheadding">
        <h2>Search by Category</h2>
    </div>

    <div class="searchbybusiness">
        <div class="sitecontainer">
            @if(isset($data['categories']) && count($data['categories']) > 0)
                @foreach($data['categories'] as $category)
                    <a href="{{route('business.search_results', ['category_id' => $category->category_id])}}">
                        <div class="item col-md-2">
                            <div class="iteminner">
                                <div class="layer"></div>
                                <img src="{{ asset('category/thumb')}}/{{$category->category_image}}">
                                <h3>{{$category->category_name}}</h3>
                            </div>
                        </div>
                    </a>
                @endforeach
            @else
                <p style="text-align: center;">No Category Found</p>
            @endif
        </div>
    </div>

    <div class="busenessheadding">
        <h2>Search by Region</h2>
    </div>

    <div class="searchbybusiness">
        <div class="sitecontainer">
            @if(isset($data['regions']) && count($data['regions']) > 0)
                @foreach($data['regions'] as $region)
                    <a href="{{route('business.search_results', ['region_id' => $region->region_id])}}">
                        <div class="item col-md-2">
                            <div class="iteminner">
                                <div class="layer"></div>
                                <img src="{{ asset('regions/thumb')}}/{{$region->region_image}}">
                                <h3>{{$region->region_name}}</h3>
                            </div>
                        </div>
                    </a>
                @endforeach
            @else
                <p style="text-align: center;">No Category Found</p>
            @endif
            <!-- <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea6.png')}}">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea3.png')}}">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea1.png')}}">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea2.png')}}">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea5.png')}}">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea4.png')}}">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div> -->
        </div>
    </div>
</div>
@section('pagescript')

@stop

@endsection