@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css')}}">
 <style type="text/css">
     .owl-carousel .owl-nav.disabled, .owl-carousel .owl-dots.disabled{
        display: block;
     }
 </style>
@stop
 <div class="viewareabreadcrumb">
    <div class="sitecontainer">
        <div class="viewareabreadcrumbinner">
            <h1>{{$data['businessDetail']->business_name}}</h1>
            <!-- <p>{{$data['businessDetail']->business_summary}}</p> -->
            <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>
        </div>
        @if ($message = Session::get('success'))
            <div class="text-success">
                {{ $message }}
            </div>
        @elseif ($message = Session::get('error'))
            <div class="text-danger">
                {{ $message }}
            </div>
        @endif
        
    </div>
</div>

<div class="viewareagallery">
    <div class="sitecontainer">
        <div class="viewareagalleryitems">
            <div class="itemleft">
                <div class="item">
                    <div class="iteminner">
                        @if(isset($data['businessDetail']->image) && $data['businessDetail']->image != '')
                            <img src="{{ asset('business_images')}}/{{$data['businessDetail']->image}}">
                        @else
                            <img src="{{ asset('images/img_view_areas_image1.png')}}">
                        @endif
                    </div>
                </div>
            </div>
            <div class="itemright">
                
                    @if(isset($data['businessImages']) && count($data['businessImages']) > 0)
                        @foreach($data['businessImages'] as $key => $businessImage)
                            <div class="item">
                                <div class="iteminner">
                                    <img src="{{ asset('business_images/thumb')}}/{{$businessImage->image_name}}">
                                </div>
                            </div>
                        @endforeach
                    @endif
                

                <!-- <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image2.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image3.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image4.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <div class="layer"></div>
                        <label>+5 Photos</label>
                        <img src="{{ asset('images/img_view_areas_image5.png')}}">
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>

<div class="bussinesscontactinfo">

    <div class="sitecontainer">
        <div class="items">
            <div class="item text-center">
                <img src="{{ asset('images/ic_header_location.png')}}">
                @if(isset($data['businessDetail']->address) && $data['businessDetail']->address != '')
                    <p>{{$data['businessDetail']->address}}</p>
                @endif
            </div>
            <div class="item text-center">
                <img src="{{ asset('images/ic_distributor_phone.png')}}">
                @if(isset($data['businessDetail']->phone_no) && $data['businessDetail']->phone_no != '')
                    <p>{{$data['businessDetail']->phone_no}}</p>
                @endif
            </div>  
            <div class="item text-center">
                <img src="{{ asset('images/email.png')}}">
                @if(isset($data['businessDetail']->email_for_business_directory) && $data['businessDetail']->email_for_business_directory != '')
                    <p>{{$data['businessDetail']->email_for_business_directory}}</p>
                @endif
            </div> 
            <div class="item text-center">

                <?php
                    $days   = []; $currentOpenOrClose = ''; $currentOpenOrClose2 = '';
                    $period = new DatePeriod(new DateTime(),new DateInterval('P1D'),6);
                    foreach ($period as $day){
                        $days[] = $day->format('l');
                    }
                    //echo "<pre>";print_r($data['businessTimes']);exit; 
                    if(isset($data['businessTimes']) && count($data['businessTimes']) >0){
                        foreach ($data['businessTimes'] as $key => $value) {
                            if(strtolower(date('l')) == strtolower($value->day)){
                                $currentOpenOrClose = '<p><span>Closed</span> Opens '.date('l').' '.date("g:iA", strtotime($value->open_time)).'</p>';
                                $currentOpenOrClose2 = date('l')." <span>Closed</span>";

                                if (date("H:i:s") > $value->open_time && date("H:i:s") < $value->close_time){
                                   $currentOpenOrClose = '<p><span style="color:green;">Open</span> Closed '.date('l').' '.date("g:iA", strtotime($value->close_time)).'</p>';
                                   $currentOpenOrClose2 = date('l')." <span style='color:green;'>Open</span>";
                                }
                            }
                        }
                    }
                    echo $currentOpenOrClose;
                ?>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                        <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                    </button>
                    <ul class="dropdown-menu">
                        <?php
                            $days   = [];
                            $period = new DatePeriod(new DateTime(),new DateInterval('P1D'),6);
                            foreach ($period as $day){
                                $days[] = $day->format('l');
                            }
                            unset($days[0]);
                            $weeklySchedule = array();
                            foreach($data['businessTimes'] as $key => $value){
                                if(strtolower($days[1]) == strtolower($value->day)){
                                    //remove : and i to show only hours
                                    $weeklySchedule[1] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[2]) == strtolower($value->day)) {
                                    $weeklySchedule[2] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[3]) == strtolower($value->day)) {
                                    $weeklySchedule[3] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[4]) == strtolower($value->day)) {
                                    $weeklySchedule[4] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[5]) == strtolower($value->day)) {
                                    $weeklySchedule[5] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[6]) == strtolower($value->day)) {
                                    $weeklySchedule[6] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }
                            }
                            ksort($weeklySchedule);
                            foreach ($weeklySchedule as $key => $value) {
                                echo $value;
                            }
                        ?>
                          <li class="closemenu"><?=$currentOpenOrClose2?></li>
                    </ul>
                </div>
            </div> 
        </div>
    </div>
</div>


<div class="viewareacontent">
    <div class="sitecontainer">
        <h3>Services</h3>
        @if(isset($data['businessDetail']->services) && $data['businessDetail']->services != '')
            <p>{{$data['businessDetail']->services}}</p>
        @endif

        <h3>Business Details</h3>
        @if(isset($data['businessDetail']->business_summary) && $data['businessDetail']->business_summary != '')
            <p>{{$data['businessDetail']->business_summary}}</p>
        @endif

        <div class="socialshare">
            <div class="items">
                <ul>
                    @if(isset($data['businessDetail']->facebook_link) && $data['businessDetail']->facebook_link != '')
                        <li><a href="{{$data['businessDetail']->facebook_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_facebook.png')}}"></a></li>
                    @endif
                    @if(isset($data['businessDetail']->twitter_link) && $data['businessDetail']->twitter_link != '')
                        <li><a href="{{$data['businessDetail']->twitter_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_twitter.png')}}"></a></li>
                    @endif
                    @if(isset($data['businessDetail']->linkedin_link) && $data['businessDetail']->linkedin_link != '')
                        <li><a href="{{$data['businessDetail']->linkedin_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_linkedin.png')}}"></a></li>
                    @endif
                    @if(isset($data['businessDetail']->youtube_link) && $data['businessDetail']->youtube_link != '')
                        <li><a href="{{$data['businessDetail']->youtube_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_youtube.png')}}"></a></li>
                    @endif
                    @if(isset($data['businessDetail']->website_link) && $data['businessDetail']->website_link != '')
                        <li><a href="{{$data['businessDetail']->website_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_website.png')}}"></a></li>
                    @endif
                    <li><a data-toggle="modal" data-target="#smsshare" href="javascript:void(0);"><img src="{{ asset('images/ic_business listing_message.png')}}"></a></li>
                </ul>
            </div>
        </div>

    </div>
</div>


<div class="recomndedbusiness">
    <div class="sitecontainer">
        <div class="busenesslisting">
            <h2>Recommended Businesses</h2>
            @if(isset($data['recommendedBusinesses']) && count($data['recommendedBusinesses']) > 0)
                <div class="sitecontainer">
                    <div class="businessslider">
                        <div class="owl-carousel owl-business owl-theme">
                            @foreach($data['recommendedBusinesses'] as $business)
                                <a href="{{route('business.view_business',$business->business_id)}}">
                                    <div class="item">
                                        <div class="iteminner">
                                            <img src="{{ asset('business_images/thumb')}}/{{$business->business_logo}}">
                                            <h3>{{$business->business_name}}</h3>
                                            <p>{{$business->services}}</p>
                                            <h4>{{$business->address}}</h4>
                                            <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> {{$business->phone_no}}</span>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @else
                <p style="text-align: center;">No Recommended Businesses Found</p>
            @endif

        </div>
    </div>
</div>

 <div id="smsshare" class="modal fade smsshare" role="dialog">
  <div class="modal-dialog" style="width: 450px;">
    <div class="modal-content">
      <a class="closepop" href="javascript:void(0);" data-dismiss="modal">
          <img src="{{ asset('images/ic_profile_images_remove.png')}}">
      </a>
      <form action="{{route('business.send_business_details_sms')}}" method="post">
        @csrf
        <input type="hidden" name="business_id" value="{{$data['businessDetail']->business_id}}">
        <div class="popupcontent">
        
         <h3><img src="{{ asset('images/ic_business_listing_message.png')}}"> Share via SMS</h3>
         <div class="sharedetail">
            <h4>{{$data['businessDetail']->business_name}}</h4>
            <h5><img src="{{ asset('images/ic_header_location.png')}}"> {{$data['businessDetail']->address}}</h5>
            <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> {{$data['businessDetail']->phone_no}}</span>
         </div>

         <p>Send these details to your mobile phone for free</p>

         <div class="phone">
                <label>Phone</label>
                <input type="text" onkeypress="return KeycheckOnlyNumeric(event);" placeholder="Enter a number" name="phone_no" required="">
                <button type="submit">Send</button>
         </div>
      </div>
    </form>
    </div>
  </div>
</div>
@section('pagescript')
<script src="{{ asset('js/slick.min.js')}}"></script>
 <script>
    var owl = $('.owl-business');
    owl.owlCarousel({
        loop:true,
        nav:true, 
        margin:20,
        autoplay:true,
        dots: true,
        items:4,
        responsive:{
           0:{
               items:1
           },
           550:{
               items:2
           },
           700:{
               items:2
           },            
           991:{
               items:3
           },
           1200:{
               items:4 // Row in content item set
           }
       }
    });
    $( ".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $( ".owl-next").html('<i class="fa fa-angle-right"></i>');
</script>
<script>
        function KeycheckOnlyNumeric(e)
        {

            var _dom = 0;
            _dom = document.all ? 3 : (document.getElementById ? 1 : (document.layers ? 2 : 0));
            if (document.all)
                e = window.event; // for IE
            var ch = '';
            var KeyID = '';
            if (_dom == 2) {                     // for NN4
                if (e.which > 0)
                    ch = '(' + String.fromCharCode(e.which) + ')';
                KeyID = e.which;
            }
            else
            {
                if (_dom == 3) {                   // for IE
                    KeyID = (window.event) ? event.keyCode : e.which;
                }
                else {                       // for Mozilla
                    if (e.charCode > 0)
                        ch = '(' + String.fromCharCode(e.charCode) + ')';
                    KeyID = e.charCode;
                }
            }
            if ((KeyID >= 65 && KeyID <= 90) || (KeyID >= 97 && KeyID <= 122) || (KeyID >= 33 && KeyID <= 47) || (KeyID >= 58 && KeyID <= 64) || (KeyID >= 91 && KeyID <= 96) || (KeyID >= 123 && KeyID <= 126) || (KeyID == 32))//changed by jshah for stopping spaces
            {
                return false;
            }
            return true;
        }
    </script>
@stop

@endsection