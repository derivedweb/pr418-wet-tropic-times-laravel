@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css')}}">
@stop



 <div class="viewareabreadcrumb">
    <div class="sitecontainer">

        <div class="viewareabreadcrumbinner">
            <h1><a href="{{route('business.edit_business_details')}}?flag=edit&product_id={{$data['product_id']}}"><img src="{{ asset('images/ic_map_back.png')}}"></a> {{$business->business_name}}</h1>
            <!-- <p>{{$business->business_summary}}</p> -->
           <!--  <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a> -->


        </div>
    </div>
</div>

<div class="viewareagallery">
    <div class="sitecontainer">
        <div class="viewareagalleryitems">
            <div class="itemleft">
                <div class="item">
                    <div class="iteminner">
                        <!-- <img src="{{ asset('images/img_view_areas_image1.png')}}"> -->
                        <img src="{{ asset('business_images/'.$business->image)}}">
                    </div>
                </div>
            </div>
            <div class="itemright">
                @if(isset($data['businessImages']) && count($data['businessImages']) > 0)
                        @foreach($data['businessImages'] as $key => $businessImage)
                            
                                <div class="item">
                                    <div class="iteminner">
                                        <img src="{{ asset('business_images/'.$businessImage->image_name)}}">
                                    </div>
                                </div>
                            
                        @endforeach
                    @endif
                <!-- <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image2.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image3.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <img src="{{ asset('images/img_view_areas_image4.png')}}">
                    </div>
                </div>
                <div class="item">
                    <div class="iteminner">
                        <div class="layer"></div>
                        <label>+5 Photos</label>
                        <img src="{{ asset('images/img_view_areas_image5.png')}}">
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>

<div class="bussinesscontactinfo">

    <div class="sitecontainer">
        <div class="items">
            <div class="item text-center">
                <img src="{{ asset('images/ic_header_location.png')}}">
                <p>{{$business->address}}</p>
            </div>
            <div class="item text-center">
                <img src="{{ asset('images/ic_distributor_phone.png')}}">
                <p>{{$business->phone_no}}</p>
            </div>  
            <div class="item text-center">
                <img src="{{ asset('images/email.png')}}">
                <p>{{$business->email_for_business_directory}}</p>
            </div> 
            <div class="item text-center">

                <?php
                    $days   = []; $currentOpenOrClose = '';
                    $period = new DatePeriod(new DateTime(),new DateInterval('P1D'),6);
                    foreach ($period as $day){
                        $days[] = $day->format('l');
                    }
                    //echo "<pre>";print_r($data['businessTimes']);exit; 
                    if(isset($data['businessTimes']) && count($data['businessTimes']) >0){
                        foreach ($data['businessTimes'] as $key => $value) {
                            if(strtolower(date('l')) == strtolower($value->day)){
                                $currentOpenOrClose = '<p><span>Closed</span> Opens '.date('l').' '.date("g:iA", strtotime($value->open_time)).'</p>';
                                $currentOpenOrClose2 = date('l')." <span>Closed</span>";
                                if (date("H:i:s") > $value->open_time && date("H:i:s") < $value->close_time){
                                   $currentOpenOrClose = '<p><span style="color:green;">Open</span> Closed '.date('l').' '.date("g:iA", strtotime($value->close_time)).'</p>';
                                   $currentOpenOrClose2 = date('l')." <span style='color:green;'>Open</span>";
                                }
                            }
                        }
                    }
                    echo $currentOpenOrClose;
                ?>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                        <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                    </button>
                    <ul class="dropdown-menu">
                        <?php
                            $days   = [];
                            $period = new DatePeriod(new DateTime(),new DateInterval('P1D'),6);
                            foreach ($period as $day){
                                $days[] = $day->format('l');
                            }
                            unset($days[0]);
                            $weeklySchedule = array();
                            foreach($data['businessTimes'] as $key => $value){
                                if(strtolower($days[1]) == strtolower($value->day)){
                                    //remove : and i to show only hours
                                    $weeklySchedule[1] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[2]) == strtolower($value->day)) {
                                    $weeklySchedule[2] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[3]) == strtolower($value->day)) {
                                    $weeklySchedule[3] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[4]) == strtolower($value->day)) {
                                    $weeklySchedule[4] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[5]) == strtolower($value->day)) {
                                    $weeklySchedule[5] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[6]) == strtolower($value->day)) {
                                    $weeklySchedule[6] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }
                            }
                            ksort($weeklySchedule);
                            foreach ($weeklySchedule as $key => $value) {
                                echo $value;
                            }
                        ?>
                          <li class="closemenu"><?=$currentOpenOrClose2?></li>
                    </ul>
                </div>
            </div> 
        </div>
    </div>
</div>


<div class="viewareacontent">
    <div class="sitecontainer">
        <h3>Services</h3>
        <p>{{$business->services}}</p>

        <h3>Business Details</h3>
        <p>
            {{$business->business_summary}}
        </p>

        <div class="socialshare">
            <div class="items">
                <ul>
                    <li><a href="http://{{$business->facebook_link}}"><img src="{{ asset('images/ic_business_listing_facebook.png')}}"></a></li>
                    <li><a href="http://{{$business->twitter_link}}"><img src="{{ asset('images/ic_business_listing_twitter.png')}}"></a></li>
                    <li><a href="http://{{$business->linkedin_link}}"><img src="{{ asset('images/ic_business_listing_linkedin.png')}}"></a></li>
                    <li><a href="http://{{$business->youtube_link}}"><img src="{{ asset('images/ic_business_listing_youtube.png')}}"></a></li>
                    <li><a href="http://{{$business->website_link}}"><img src="{{ asset('images/ic_business_listing_website.png')}}"></a></li>
                    <!-- <li><a href="{{$business->business_summary}}"><img src="{{ asset('images/ic_business listing_message.png')}}"></a></li> -->
                </ul>
            </div>
        </div>

    </div>
</div>


@section('pagescript')
<script src="{{ asset('js/slick.min.js')}}"></script>
 <script>
    var owl = $('.owl-business');
    owl.owlCarousel({
        loop:true,
        nav:true, 
        margin:20,
        autoplay:true,
        dots: true,
        items:4,
        responsive:{
           0:{
               items:1
           },
           550:{
               items:2
           },
           700:{
               items:2
           },            
           991:{
               items:3
           },
           1200:{
               items:4 // Row in content item set
           }
       }
    });
    $( ".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $( ".owl-next").html('<i class="fa fa-angle-right"></i>');
</script>
@stop

@endsection