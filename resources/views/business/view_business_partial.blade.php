@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css')}}">
@stop
<div class="viewareabreadcrumb">
    <div class="sitecontainer">
        <div class="viewareabreadcrumbinner">
            <h1>{{$data['businessDetail']->business_name}}</h1>
            <!-- <p>We’ll help protect the things that matter</p> -->
            <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>
        </div>
    </div>
</div>

<div class="busiineessmap">
    <div id="dvMap"></div>
</div>

<div class="bussinesscontactinfo">
    <div class="sitecontainer">
        <div class="items">
            <div class="item text-center">
                <img src="{{ asset('images/ic_header_location.png')}}">
                 @if(isset($data['businessDetail']->address) && $data['businessDetail']->address != '')
                    <p>{{$data['businessDetail']->address}}</p>
                @endif
            </div>
            <div class="item text-center">
                <img src="{{ asset('images/ic_distributor_phone.png')}}">
                @if(isset($data['businessDetail']->phone_no) && $data['businessDetail']->phone_no != '')
                    <p>{{$data['businessDetail']->phone_no}}</p>
                @endif
            </div>  
            <div class="item text-center">
                <img src="{{ asset('images/email.png')}}">
                @if(isset($data['businessDetail']->email_for_business_directory) && $data['businessDetail']->email_for_business_directory != '')
                    <p>{{$data['businessDetail']->email_for_business_directory}}</p>
                @endif
            </div> 
            <div class="item text-center">

                <?php
                    $days   = []; $currentOpenOrClose = ''; $currentOpenOrClose2 = '';
                    $period = new DatePeriod(new DateTime(),new DateInterval('P1D'),6);
                    foreach ($period as $day){
                        $days[] = $day->format('l');
                    }
                    //echo "<pre>";print_r($data['businessTimes']);exit; 
                    if(isset($data['businessTimes']) && count($data['businessTimes']) >0){
                        foreach ($data['businessTimes'] as $key => $value) {
                            if(strtolower(date('l')) == strtolower($value->day)){
                                $currentOpenOrClose = '<p><span>Closed</span> Opens '.date('l').' '.date("g:iA", strtotime($value->open_time)).'</p>';
                                $currentOpenOrClose2 = date('l')." <span>Closed</span>";

                                if (date("H:i:s") > $value->open_time && date("H:i:s") < $value->close_time){
                                   $currentOpenOrClose = '<p><span style="color:green;">Open</span> Closed '.date('l').' '.date("g:iA", strtotime($value->close_time)).'</p>';
                                   $currentOpenOrClose2 = date('l')." <span style='color:green;'>Open</span>";
                                }
                            }
                        }
                    }
                    echo $currentOpenOrClose;
                ?>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                        <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                    </button>
                    <ul class="dropdown-menu">
                        <?php
                            $days   = [];
                            $period = new DatePeriod(new DateTime(),new DateInterval('P1D'),6);
                            foreach ($period as $day){
                                $days[] = $day->format('l');
                            }
                            unset($days[0]);
                            $weeklySchedule = array();
                            foreach($data['businessTimes'] as $key => $value){
                                if(strtolower($days[1]) == strtolower($value->day)){
                                    //remove : and i to show only hours
                                    $weeklySchedule[1] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[2]) == strtolower($value->day)) {
                                    $weeklySchedule[2] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[3]) == strtolower($value->day)) {
                                    $weeklySchedule[3] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[4]) == strtolower($value->day)) {
                                    $weeklySchedule[4] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[5]) == strtolower($value->day)) {
                                    $weeklySchedule[5] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }elseif (strtolower($days[6]) == strtolower($value->day)) {
                                    $weeklySchedule[6] = '<li>'.$value->day.' <span>'.date("g:iA", strtotime($value->open_time)).'-'.date("g:iA", strtotime($value->close_time)).'</span></li>';
                                }
                            }
                            ksort($weeklySchedule);
                            foreach ($weeklySchedule as $key => $value) {
                                echo $value;
                            }
                        ?>
                          <li class="closemenu"><?=$currentOpenOrClose2?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="viewareacontent">
    <div class="sitecontainer">
        <h3>Services</h3>
        @if(isset($data['businessDetail']->services) && $data['businessDetail']->services != '')
            <p>{{$data['businessDetail']->services}}</p>
        @endif

        <div class="socialshare">
            <div class="items">
                <ul>
                    @if(isset($data['businessDetail']->facebook_link) && $data['businessDetail']->facebook_link != '')
                        <li><a href="{{$data['businessDetail']->facebook_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_facebook.png')}}"></a></li>
                    @endif
                    @if(isset($data['businessDetail']->twitter_link) && $data['businessDetail']->twitter_link != '')
                        <li><a href="{{$data['businessDetail']->twitter_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_twitter.png')}}"></a></li>
                    @endif
                    @if(isset($data['businessDetail']->linkedin_link) && $data['businessDetail']->linkedin_link != '')
                        <li><a href="{{$data['businessDetail']->linkedin_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_linkedin.png')}}"></a></li>
                    @endif
                    @if(isset($data['businessDetail']->youtube_link) && $data['businessDetail']->youtube_link != '')
                        <li><a href="{{$data['businessDetail']->youtube_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_youtube.png')}}"></a></li>
                    @endif
                    @if(isset($data['businessDetail']->website_link) && $data['businessDetail']->website_link != '')
                        <li><a href="{{$data['businessDetail']->website_link}}" target="_blank"><img src="{{ asset('images/ic_business_listing_website.png')}}"></a></li>
                    @endif
                    <li><a data-toggle="modal" data-target="#smsshare" href="javascript:void(0);"><img src="{{ asset('images/ic_business listing_message.png')}}"></a></li>
                </ul>
            </div>
        </div>

    </div>
</div>

 <div id="smsshare" class="modal fade smsshare" role="dialog">
  <div class="modal-dialog" style="width: 450px;">
    <div class="modal-content">
      <a class="closepop" href="javascript:void(0);" data-dismiss="modal">
          <img src="{{ asset('images/ic_profile_images_remove.png')}}">
      </a>
      <form action="{{route('business.send_business_details_sms')}}" method="post">
        @csrf
        <input type="hidden" name="business_id" value="{{$data['businessDetail']->business_id}}">
        <div class="popupcontent">
        
         <h3><img src="{{ asset('images/ic_business_listing_message.png')}}"> Share via SMS</h3>
         <div class="sharedetail">
            <h4>{{$data['businessDetail']->business_name}}</h4>
            <h5><img src="{{ asset('images/ic_header_location.png')}}"> {{$data['businessDetail']->address}}</h5>
            <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> {{$data['businessDetail']->phone_no}}</span>
         </div>

         <p>Send these details to your mobile phone for free</p>

         <div class="phone">
                <label>Phone</label>
                <input type="text" onkeypress="return KeycheckOnlyNumeric(event);" placeholder="Enter a number" name="phone_no" required="">
                <button type="submit">Send</button>
         </div>
      </div>
    </form>
    </div>
  </div>
</div>

@section('pagescript')
<script src="{{ asset('js/slick.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH2uiqesOZEbWOdwb0YbnrO9pErqV28nk&libraries=places"></script>
 <script type="text/javascript">
    var markers = [
    {
        "title": "{{$data['businessDetail']->business_name}}",
        "lat": "{{$data['businessDetail']->latitude}}",
        "lng": "{{$data['businessDetail']->longitude}}",
        "description": "{{$data['businessDetail']->services}}"
    },
    ];
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
        //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow();
        var index=0;
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title,
                icon:"{{ asset('images/ic_map_pin_selected.png')}}?i="+(index++),
            });
 
            //Attach click event to the marker.
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                    infoWindow.setContent("<div class='dirbox'>" + data.title + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
    }
</script>

<script>
        function KeycheckOnlyNumeric(e)
        {

            var _dom = 0;
            _dom = document.all ? 3 : (document.getElementById ? 1 : (document.layers ? 2 : 0));
            if (document.all)
                e = window.event; // for IE
            var ch = '';
            var KeyID = '';
            if (_dom == 2) {                     // for NN4
                if (e.which > 0)
                    ch = '(' + String.fromCharCode(e.which) + ')';
                KeyID = e.which;
            }
            else
            {
                if (_dom == 3) {                   // for IE
                    KeyID = (window.event) ? event.keyCode : e.which;
                }
                else {                       // for Mozilla
                    if (e.charCode > 0)
                        ch = '(' + String.fromCharCode(e.charCode) + ')';
                    KeyID = e.charCode;
                }
            }
            if ((KeyID >= 65 && KeyID <= 90) || (KeyID >= 97 && KeyID <= 122) || (KeyID >= 33 && KeyID <= 47) || (KeyID >= 58 && KeyID <= 64) || (KeyID >= 91 && KeyID <= 96) || (KeyID >= 123 && KeyID <= 126) || (KeyID == 32))//changed by jshah for stopping spaces
            {
                return false;
            }
            return true;
        }
    </script>
@stop

@endsection