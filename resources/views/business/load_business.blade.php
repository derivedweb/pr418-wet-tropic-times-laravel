@if(!$businesses->isEmpty())
@foreach($businesses as $business)
    <a href="javascript:void(0);"> 
        <div class="item" data-b-id="{{$business->business_id}}">
            <div class="image">
                <img src="{{ asset('business_images/thumb')}}/{{$business->business_logo}}">
            </div>
            <div class="content">
                <h3>{{$business->business_name}}</h3>
                <p>{{$business->services}}</p>
                <h4>{{$business->address}}</h4>
                <div class="info">
                    <img src="{{ asset('images/ic_distributor_phone.png')}}">
                    <label>{{$business->phone_no}}</label>
                    @if(!empty($business->business_time))
                   
                        @php
                            $time = explode("-", $business->business_time);
                        @endphp
                        @if(date("H:i:s") > $time[0] && date("H:i:s") < $time[1])
                            <span>Open &nbsp;<b>Closes {{date("hA", strtotime($time[1]))}}</b></span>
                        @else
                            <span style="color:red;">Closed &nbsp;<b><!-- Open {{date("hA", strtotime($time[0]))}} --></b></span>
                        @endif
                    
                    @else
                        <span style="color:red;">Closed</span>
                    @endif
                </div>
            </div>
        </div>
     </a> 
@endforeach
<input type="hidden" class="last_page" value="{{$businesses->lastPage()}}">
<input type="hidden" class="current_page" value="{{$businesses->currentPage()}}">
@else
  <div class="text-center">
    <!-- <img class="no-messages" src="{{ asset('images/no-messages.png')}}" width="100"> -->
    <p>No Business.</p>
  </div>
@endif
@if ($businesses->lastPage() > 1)
   <!--  <div class="pagination">
        <ul>
            <li><a href="{{ $businesses->url(1) }}"><i class="fa fa-angle-left"></i></a></li>
            @for ($i = 1; $i <= $businesses->lastPage(); $i++)
                <li class="page-item {{ ($businesses->currentPage() == $i) ? ' active' : '' }}">
                    <a class="page-link" href="{{ $businesses->url($i) }}">{{ $i }}</a>
                </li>
            @endfor
            <li><a href="{{ ($businesses->currentPage() == $businesses->lastPage()) ? '#' : $businesses->url($businesses->currentPage()+1) }}" ><i class="fa fa-angle-right"></i></a></li>
        </ul>
    </div> -->
@endif