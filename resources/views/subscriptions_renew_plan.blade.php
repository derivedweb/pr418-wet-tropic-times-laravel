@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 <div class="subscriptionsplans">
    <div class="subscriptionsplansinner">
        <div class="subscriptionsbox">
            <h2>Subscriptions</h2>
            <h4>Current Plan</h4>
            <div class="plandetails">
                 <div class="planinfo">
                    <label>Monthly</label>
                    <span>$9.99 per month</span>
                    <p>Your plan renews on 16 March 2021</p>
                 </div>
                 <div class="planbuttons">
                    <a class="renew" href="#">Renew Plan</a>
                 </div>
            </div>
            <h4>Payment Methods</h4>
            <div class="paymentmethods">
                <div class="item">
                    <img src="{{ asset('images/ic_payment_visa.png')}}">
                    <span>****  4242</span>
                    <span>Exp 04/24</span>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                            <img src="{{ asset('images/ic_payment_edit.png')}}">
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="#">Make Default</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                </div>
                <div class="item">
                    <img src="{{ asset('images/ic_payment_master.png')}}">
                    <span>****  4242</span>
                    <span>Exp 04/24</span>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                            <img src="{{ asset('images/ic_payment_edit.png')}}">
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="#">Make Default</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="addpaymentmethod">
                <a href="#"><img src="{{ asset('images/ic_payment_add.png')}}"> Add payment method</a>
            </div>

        </div>
    </div>
</div>
@section('pagescript')

@stop

@endsection