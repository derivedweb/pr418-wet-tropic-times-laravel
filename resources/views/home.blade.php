@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 
<div class="heroslider">
    <div class="sitecontainer">
        <div class="slideritems">
            <div class="owl-carousel owl-herobanner owl-theme">
                <div class="item">
                    <img src="images/slide1.png">
                    <div class="content">
                        <div class="possted">
                            <a href="#">Video</a> 
                            <p>Posted 16/Dec/2020 12:30 PM</p>
                        </div>
                        <h2>Paula Badosa <br> Tests Positive </h2>
                        <h4>Rising tennis star Paula Badosa tests positive for <br> COVID before Australian Open</h4>
                        <h5>The 23-year-old has been isolating in Melbourne in <br>mandatory 14-day hotel quarantine…</h5>
                        <a class="viewstory" href="#">View Story</a>
                    </div>
                </div>
                <div class="item">
                    <img src="images/slide1.png">
                    <div class="content">
                        <div class="possted">
                            <a href="#">Video</a> 
                            <p>Posted 16/Dec/2020 12:30 PM</p>
                        </div>
                        <h2>NSW Premier Gladys <br> Berejiklian’s staff…</h2>
                        <h4>Rising tennis star Paula Badosa tests positive for <br> COVID before Australian Open</h4>
                        <h5>The 23-year-old has been isolating in Melbourne in <br>mandatory 14-day hotel quarantine…</h5>
                        <a class="viewstory" href="#">View Story</a>
                    </div>
                </div>
                <div class="item">
                    <img src="images/slide1.png">
                    <div class="content">
                        <div class="possted">
                            <a href="#">Video</a> 
                            <p>Posted 16/Dec/2020 12:30 PM</p>
                        </div>
                        <h2>'Innocent mistake': <br> Ash Barty apologises …</h2>
                        <h4>Rising tennis star Paula Badosa tests positive for <br> COVID before Australian Open</h4>
                        <h5>The 23-year-old has been isolating in Melbourne in <br>mandatory 14-day hotel quarantine…</h5>
                        <a class="viewstory" href="#">View Story</a>
                    </div>
                </div>
                <div class="item">
                    <img src="images/slide1.png">
                    <div class="content">
                        <div class="possted">
                            <a href="#">Video</a> 
                            <p>Posted 16/Dec/2020 12:30 PM</p>
                        </div>
                        <h2>'Innocent mistake': <br> Ash Barty apologises…</h2>
                        <h4>Rising tennis star Paula Badosa tests positive for <br> COVID before Australian Open</h4>
                        <h5>The 23-year-old has been isolating in Melbourne in <br>mandatory 14-day hotel quarantine…</h5>
                        <a class="viewstory" href="#">View Story</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="sliderdots dots">
            <div id='carousel-custom-dots' class='owl-dots'>
                <div class="item owl-dot active">
                    <a class="slide1" href="javascript:void(0);">
                        <div class="img">
                            <img src="images/slideitem1.png">
                        </div>
                        <div class="info">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h4>Paula Badosa Tests Positive </h4>
                        </div>
                    </a>
                </div>
                <div class="item owl-dot">
                    <a class="slide2" href="javascript:void(0);">
                        <div class="img">
                            <img src="images/slideitem2.png">
                        </div>
                        <div class="info">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h4>NSW Premier Gladys Berejiklian’s staff…</h4>
                        </div>
                    </a>
                </div>
                <div class="item owl-dot">
                    <a class="slide3" href="javascript:void(0);">
                        <div class="img">
                            <img src="images/slideitem3.png">
                        </div>
                        <div class="info">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h4>'Innocent mistake': Ash Barty apologises …</h4>
                        </div>
                    </a>
                </div>
                <div class="item owl-dot">
                    <a class="slide4" href="javascript:void(0);">
                        <div class="img">
                            <img src="images/slideitem4.png">
                        </div>
                        <div class="info">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h4>'Innocent mistake': Ash Barty apologises…</h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>

    </div>
    </div>

    <div class="flightticketbook">
    <div class="sitecontainer">
        <img src="images/img_ad1.png">
    </div>
    </div>

    <div class="regiontopstores">
    <div class="sitecontainer">

        <div class="topstoryhead">
            <div class="selectbox">
                <select>
                    <option>All Regions Top Stories</option>
                    <option>All Regions Top Stories1</option>
                    <option>All Regions Top Stories2</option>
                    <option>All Regions Top Stories3</option>
                </select>
                <i class="fa fa-chevron-down"></i>
            </div>
        </div>

        <div class="storieslisting">
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_1.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_2.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_3.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_4.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_5.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_6.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_7.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_8.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_1.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_2.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_3.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_4.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_5.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_6.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_1.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <img src="images/img_stories_2.png">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air …</p>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="loadmorestory">
            <a id="loadMore" href="javascript:void(0);">
                <img src="images/ic_stories_load more.png"> <br>
                Load More Stories
            </a>
        </div>

    </div>
    </div>

    <div class="categorywisestory videos">
    <div class="sitecontainer">
        <div class="heading">
            <h3>Videos</h3>
            <a href="#">More Videos</a>
        </div>
        <div class="storieslisting">
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="image">
                            <img src="images/img_stories_1.png">
                            <a class="play" href="#"><img src="images/ic_story_video_play.png"> 3:56</a>
                        </div>
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="image">
                            <img src="images/img_stories_2.png">
                            <a class="play" href="#"><img src="images/ic_story_video_play.png"> 3:56</a>
                        </div>
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="image">
                            <img src="images/img_stories_3.png">
                            <a class="play" href="#"><img src="images/ic_story_video_play.png"> 3:56</a>
                        </div>
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="image">
                            <img src="images/img_stories_4.png">
                            <a class="play" href="#"><img src="images/ic_story_video_play.png"> 3:56</a>
                        </div>
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                        </div>
                    </a>
                </div>
            </div>
            
        </div>
    </div>
    </div>

    <div class="categorywisestory">
    <div class="sitecontainer">
        <div class="heading">
            <h3>Community</h3>
            <a href="#">More Community</a>
        </div>
        <div class="storieslisting">
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live.</p>
                        </div>
                    </a>
                </div>
            </div>
            
        </div>
    </div>
    </div>

    <div class="categorywisestory">
    <div class="sitecontainer">
        <div class="heading">
            <h3>Rural</h3>
            <a href="#">More Rural</a>
        </div>
        <div class="storieslisting">
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live.</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <a href="#">
                        <div class="content">
                            <span>Posted 16/Dec/2020 12:30 PM</span>
                            <h3>NSW flags stricter quarantine protocols for international air crew</h3>
                            <p>NSW wants incoming international air crew to face stricter quarantine measures after an airport driver for Sydney Ground Transport tested positive for COVID-19. Follow live.</p>
                        </div>
                    </a>
                </div>
            </div>
            
        </div>
    </div>
    </div>

@section('pagescript')

<script>
    var owl = $('.owl-herobanner');
    owl.owlCarousel({
        loop:true,
        nav:false, 
        margin:0,
        autoplay:false,
        dots: true,
        items:1,
        touchDrag : false,
        mouseDrag : false
    });
    
    $('.owl-dot').click(function () {
         var owl = $(".owl-herobanner");
         owl.trigger('to.owl.carousel', [$(this).index(), 300]);

         $('.heroslider .sliderdots .item.active').removeClass('active');
         $(this).addClass('active');
    });
</script>

<script>
    $(document).ready(function(){
      $(".regiontopstores .storieslisting .item").slice(0, 8).show();
      $("#loadMore").on("click", function(e){
        e.preventDefault();
        $(".regiontopstores .storieslisting .item:hidden").slice(0, 4).slideDown();
        if($(".regiontopstores .storieslisting .item:hidden").length == 0) {
          $("#loadMore").text("").addClass("noContent");
        }
      });
    })
</script>
@stop

@endsection