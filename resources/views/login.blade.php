@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 <div class="userloginregister">
    <div class="userloginregisterinner">
        <div class="formbox">
            <h2>Login</h2>
            {!! Form::open(array('route' => 'login','method'=>'POST', 'class'=>'login-form', 'id'=>'login-form')) !!}

                @if ($message = Session::get('success'))
                    <div class="text-success">
                        {{ $message }}
                    </div>
                @elseif ($message = Session::get('error'))
                    <div class="text-danger">
                        {{ $message }}
                    </div>
                @endif 
                <div class="field">
                    <label>Email</label>
                    <input type="text" placeholder="Type your email address" name="email">
                </div>
                <div class="field">
                    <label>Password</label>
                    <input type="password" placeholder="Type your password" name="password">
                </div>
                <div class="button">
                    <button type="submit">Login</button>
                </div>
                <div class="forgotpwd">
                    <a href="{{ route('forget_password')}}">Forgotten your password?</a>
                </div>
                <div class="otherlogin text-center">
                    <div class="line"></div>
                    <label>Or use one of these options</label>
                    <ul>
                        <li class="apple"><a href="#"><img src="{{ asset('images/ic_login_apple.png')}}"></a></li>
                        <li class="google"><a href="#"><img src="{{ asset('images/ic_login_google.png')}}"></a></li>
                        <li class="facebook"><a href="#"><img src="{{ asset('images/ic_login_facebook.png')}}"></a></li>
                    </ul>
                </div>
                <div class="newuser">
                    <p>New to Wet Tropic Times? <a href="{{ route('create_your_account')}}">Sign Up Now</a></p>
                </div>
             {!! Form::close() !!}
        </div>
    </div>
</div>
@section('pagescript')
<!-- Jquery validate -->
<script type="text/javascript">

$(document).ready(function () {
    
    $('#login-form').validate({ 
    rules: {
        "email": {
            required: true,
            email: true
        },
        "password": {
            required: true
        },
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
@stop

@endsection