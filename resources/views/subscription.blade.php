@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
<div class="businesssubscription">
    <div class="back">
        <div class="sitecontainer">
            <a href="{{route('business.edit_business_details')}}?flag=edit&product_id={{$product->id}}"><img src="{{ asset('images/ic_map_back.png')}}"></a>
        </div>
    </div>
    <div class="subscriptioninner">
        <div class="subscriptionbox">
            <h2>Subscription</h2>

            @if ($message = Session::get('success'))
                <div class="text-success">
                    {{ $message }}
                </div>
            @elseif ($message = Session::get('error'))
                <div class="text-danger">
                    {{ $message }}
                </div>
            @endif

            <div class="plandetails">
                <div class="planinfo">
                    <div class="item text-center">
                        <div class="iteminner">
                            <h3>{{$product->name}}</h3>
                            <span>{{$product->description}}</span>
                            <p>Get your business listed on Wet Tropic Times directory.</p>
                            <ul>
                                <li>Business details</li>
                                <li>Contact details (email, address,phone number, website,social media links)</li>
                                <li>Space for your logo, images, business services</li>
                                <li>Opening hours</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="paymentdetail">
                    <h3>Payment Details</h3>

                    <form action="{{route('business.stripe_payment_request')}}" method="post" id="payment-form">
                    @csrf

                    <input type="hidden" name="price_id" value="{{$product->price->id}}">
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    <input type="hidden" name="business_id" value="{{Request::get('business_id')}}">
                    <div class="paymethod">
                        <ul>
                            <li class="active">
                                <label>
                                        <img src="{{ asset('images/ic_payment_method_card.png')}}">
                                </label>
                            </li>
                            <li>
                                <label>
                                        <img src="{{ asset('images/ic_payment_google pay.png')}}">
                                </label>
                            </li>
                            <li>
                                <label>
                                        <img src="{{ asset('images/ic_payment_apple pay.png')}}">
                                </label>
                            </li>
                            <li>
                                <label>
                                        <img src="{{ asset('images/ic_payment_paypal.png')}}">
                                </label>
                            </li>
                        </ul>

                        <div class="cardsdetails">
                            <div class="cardname">
                                <input type="text" name="" placeholder="Name on card" name="name_on_card">
                            </div>
                            <!-- <div class="cardnum">
                                <input class="num" type="text" name="" placeholder="Card number">
                                <input class="month" type="text" name="" placeholder="MM/YY">
                                <input class="cvv" type="text" name="" placeholder="CVV">
                            </div> -->
                            <div class="cardnum" style="padding: 14px;">
                                <div id="card-element">
                                <!-- A Stripe Element will be inserted here. -->
                                </div>
                                <div id="card-errors" role="alert"></div>
                              
                            </div>
                        </div>

                        <div class="button">
                            <button type="submit">Submit Now</button>
                        </div>

                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="success-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="">
       <!--  <h3 class="modal-title" id="exampleModalLabel">Add New Card</h3> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
          <div class="row">
            <div class="col-md-12">
            
            
            <div class="col-md-12 text-center">
                <h4>Thanks {{Auth::user()->displayname}}</h4>
                <p>You will not be charged until your business profile has been reviewed and approved.</p>
                <p>A member from the Wet Tropic Times will be contact via email to notify you of your listing activation or further details required.</p>
            </div>
          </div>

      </div>
    </div>
  </div>
</div>
</div>

<div id="payment-request-button"></div>
@section('pagescript')

<script type="text/javascript">

<?php 
if ($message = Request::get('success')){?>
    $(function() {
        $("#success-popup").modal("show");
    });
<?php } ?>
</script>

<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
var stripe = Stripe("{{Config::get('constants.stripe.publishable-key')}}");
var elements = stripe.elements();
// Custom styling can be passed to options when creating an Element.
var style = {
  base: {
    // Add your base input styles here. For example:
    fontSize: '18px',
    /*color: '#32325d',*/
  },
};

// Create an instance of the card Element.
var card = elements.create('card', {hidePostalCode: true, style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Create a token or display an error when the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>

<!-- <script type="text/javascript">
$(function() {
  var paymentRequest = stripe.paymentRequest({
  country: 'US',
  currency: 'usd',
  total: {
    label: 'Demo total',
    amount: 1099,
  },
  requestPayerName: true,
  requestPayerEmail: true,
  });

  var elements = stripe.elements();
  var prButton = elements.create('paymentRequestButton', {
    paymentRequest: paymentRequest,
  });

  // Check the availability of the Payment Request API first.
  paymentRequest.canMakePayment().then(function(result) {
    console.log(result);
    if (result) { 
      prButton.mount('#payment-request-button');
    } else {
      document.getElementById('payment-request-button').style.display = 'none';
    }
  });
});
</script> -->
@stop

@endsection