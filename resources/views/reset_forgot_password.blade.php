@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 
<div class="userloginregister myaccount">

    <!-- <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="#">My Account</a> <span>/</span> Change Password 
        </div>
    </div> -->
    <div class="userloginregisterinner">
        <div class="formbox">
            <h2>Reset Password</h2>
            {!! Form::open(array('route' => ['update_reset_forgot_password', $UserID],'method'=>'POST', 'class'=>'form-horizontal', 'id'=>'reset-password-form')) !!}
                <!-- <div class="form-group m-b-20"> -->
                @if ($message = Session::get('success'))
                    <div class="text-success">
                        {{ $message }}
                    </div>
                @elseif ($message = Session::get('error'))
                    <div class="text-danger">
                        {{ $message }}
                    </div>
                @endif 
                <!-- </div> -->
                <div class="field">
                    <label>New Password</label>
                    <input type="password" placeholder="XXXXXXXX" name="NewPassword" id="NewPassword" required>
                </div>
                <div class="field">
                    <label>Confirm Password</label>
                    <input type="password" placeholder="XXXXXXXX" name="ConfirmNewPassword" id="ConfirmNewPassword" required>
                </div>
                <div class="button chaangepwd">
                    <button type="submit">Change Password</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@section('pagescript')
<!-- Jquery validate -->
<script type="text/javascript">

$(document).ready(function () {
    
    $('#reset-password-form').validate({ 
    rules: {
        "NewPassword": {
            required: true
        },
        "ConfirmNewPassword": {
            required: true,
            equalTo : "#NewPassword",
        },
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
@stop

@endsection