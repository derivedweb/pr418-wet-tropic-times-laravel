@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 
<div class="userloginregister">
    <div class="userloginregisterinner">
        <div class="formbox forgetpass">
            <h2>Forgotten <br> Your Password</h2>
            <p>
                No problem! We’ll send you a link  to reset it.
                Please enter the email address you use to sign in to WTT.
            </p>
            {!! Form::open(array('route' => 'check_your_inbox','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'forgot-password-form')) !!}

                <!-- <div class="form-group m-b-20"> -->
                    @if ($message = Session::get('success'))
                        <div class="text-success">
                            {{ $message }}
                        </div>
                    @elseif ($message = Session::get('error'))
                        <div class="text-danger">
                            {{ $message }}
                        </div>
                    @endif 
                <!-- </div> -->

                <div class="field">
                    <label>Your Email Address</label>
                    <input type="email" id="Email" name="Email" placeholder="Type your email" required>
                </div>
                <div class="button">
                    <button type="submit">Send Reset Link</button>
                </div>
                <div class="agreeterms">
                    <p>By signing in or creating an account, you agree to our <br>
                       <a href="{{Config::get('constants.app.wp-url').'/terms-conditions'}}" target="_blank">Terms & Conditions</a> and <a href="{{Config::get('constants.app.wp-url').'/privacy-policy'}}" target="_blank">Privacy Policy</a></p>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
        
@section('pagescript')
<!-- Jquery validate -->

<script type="text/javascript">

$(document).ready(function () {
    
    $('#forgot-password-form').validate({ 
    rules: {
        "Email": {
            required: true,
            email: true
        },
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
@stop

@endsection