@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
<div class="subscriptionsplans">
    <div class="subscriptionsplansinner">

        

        <div class="subscriptionsbox">
            <h2>Subscriptions</h2>

            @if ($message = Session::get('success'))
                <div class="text-success">
                    {{ $message }}
                </div>
            @elseif ($message = Session::get('error'))
                <div class="text-danger">
                    {{ $message }}
                </div>
            @endif

            <h4>Current Plan</h4>
            @foreach($active_subscriptions as $active_subscription)
                @if($active_subscription['subscription']->status == 'active' && (($active_subscription['subscription']->cancel_at_period_end==false) || ($active_subscription['subscription']->cancel_at_period_end==true && Request::get('flag') == 'renew')))
                <div class="plandetails">
                     <div class="planinfo">
                        <label>{{$active_subscription['product']->name}}</label>
                        <span>{{$active_subscription['product']->description}}</span>
                        @if(Request::get('flag') == 'renew')
                          <p>Your plan will be active until {{date("d M Y",($active_subscription['subscription']->current_period_end))}}</p>
                        @else
                          <p>Your plan renews on {{date("d M Y",($active_subscription['subscription']->current_period_end))}}</p>
                        @endif
                        
                     </div>
                     @if(Request::get('flag') == 'renew')
                      <div class="planbuttons">
                        <a class="renew" href="{{route('user.renew_plan_confirmation')}}?plan_title={{$active_subscription['product']->name}}&plan_desc={{$active_subscription['product']->description}}&end_period={{$active_subscription['subscription']->current_period_end}}">Renew Plan</a>
                      </div>
                     @else
                     <div class="planbuttons">
                        <a class="update" href="{{route('user.update_plan')}}"><img src="{{ asset('images/ic_subscription_update.png')}}"> Update Plan</a>
                        <a class="cancel" href="{{route('user.cancel_plan_confirmation')}}?plan_title={{$active_subscription['product']->name}}&plan_desc={{$active_subscription['product']->description}}&end_period={{$active_subscription['subscription']->current_period_end}}&subscription_id={{$active_subscription['subscription']->id}}"><img src="{{ asset('images/ic_subscription_cancel.png')}}"> Cancel</a>
                     </div>
                     @endif
                </div>
                @endif
            @endforeach
            <h4>Payment Methods</h4>
            <div class="paymentmethods">
                @foreach($payment_methods->data as $payment_method)
                <div class="item item-{{$payment_method->id}} {{($customer->default_source ==$payment_method->id) ? 'active' : '' }}">
                    <img src="{{ asset('images/ic_payment_visa.png')}}">
                    <span>****  {{$payment_method->last4}}</span>
                    <span>Exp {{$payment_method->exp_month}}/{{$payment_method->exp_year}}</span>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                            <img src="{{ asset('images/ic_payment_edit.png')}}">
                        </button>
                        <ul class="dropdown-menu">
                          <li class="make-default" data-card-id="{{$payment_method->id}}"><a href="javascript:void(0);">Make Default</a></li>
                          <li class="delete" data-card-id="{{$payment_method->id}}"><a href="javascript:void(0);">Delete</a></li>
                        </ul>
                    </div>
                </div>
                @endforeach
                <!-- <div class="item">
                    <img src="{{ asset('images/ic_payment_master.png')}}">
                    <span>****  4242</span>
                    <span>Exp 04/24</span>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                            <img src="{{ asset('images/ic_payment_edit.png')}}">
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="#">Make Default</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                </div> -->
            </div>

            <div class="addpaymentmethod">
                <a href="#" data-toggle="modal" data-target="#addCardModal"><img src="{{ asset('images/ic_payment_add.png')}}"> Add payment method</a>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addCardModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Add New Card</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="{{route('user.stripe_add_card')}}" method="post" id="payment-form" data-rewardful>
          @csrf
          <div class="row">
            <div class="col-md-12">
            <label for="card-element">
              Credit or debit card
            </label>
            <div id="card-element">
              <!-- A Stripe Element will be inserted here. -->
            </div>

            <!-- Used to display form errors. -->
            <div id="card-errors" role="alert"></div>
            </div>
            <div class="col-md-12 text-center">
              <button class="btn btn-success card-submit">Submit</button>
            </div>
          </div>
        </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
@section('pagescript')
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
var stripe = Stripe("{{Config::get('constants.stripe.publishable-key')}}");
var elements = stripe.elements();
// Custom styling can be passed to options when creating an Element.
var style = {
  base: {
    // Add your base input styles here. For example:
    fontSize: '16px',
    color: '#32325d',
  },
};

// Create an instance of the card Element.
var card = elements.create('card', {hidePostalCode: true, style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Create a token or display an error when the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}


/*$(document).on("click", ".item", function(){
    $(".item").removeClass("active");
    $(this).addClass('active');
});*/

$(document).on("click", ".delete", function(){
    var card_id = $(this).data("card-id");
    if(confirm('Are you sure, you want to delete it?')){
        $.ajax({
            url : "{{route('user.stripe_delete_card')}}",
            data : { card_id:card_id },
            dataType: 'json',
        }).done(function (data) { 
            $(".item-"+card_id).hide();
        });
    }
});

$(document).on("click", ".make-default", function(){
    var card_id = $(this).data("card-id");
    $.ajax({
        url : "{{route('user.stripe_make_card_default')}}",
        data : { card_id:card_id },
        dataType: 'json',
    }).done(function (data) { 
        $(".item").removeClass("active");
        $(".item-"+card_id).addClass('active');
    });
    
});


  </script>
@stop

@endsection