@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
<div class="businesslisting">
    <div class="businessbanner">
        <div class="image">
            <img src="{{ asset('images/img_business_listing_header.png')}}">
        </div>
        <div class="content">
            <h2>Wet Tropic Times <br> <span>Business Listings</span></h2>
            <p>We make it easy for businesses to connect with people searching online.<br>
            See how our digital tools can help customers find you across the top <br>
            search engines, social media, maps and more.</p>
            <div class="buttons">
                <a href="#">List Your Business</a>
                <a class="contact" href="#">Contact Us</a>
            </div>
        </div>
    </div>

    <div class="drivequality">
        <div class="drivequalityinner">
            <div class="content text-center">
                <h2>Drive Quality Leads to <br> Your Website.</h2>
                <p>When customers search on Wet Tropic Times <br>
                they’re actively looking for products and services like yours.</p>
                <div class="buttons">
                    <a href="#">List Your Business</a>
                    <a class="contact" href="#">Contact Us</a>
                </div>
                <a class="viewadd" href="#">View Our Advertising Options</a>
            </div>
        </div>
    </div>

    <div class="budgetplans">
        <div class="sitecontainer">
            <h2>Plans to Suit All Business Budgets</h2>
            <div class="budgetplansbox">
                <div class="item text-center">
                    <div class="iteminner">
                        <h3>Bronze</h3>
                        <span>$10.99 / month</span>
                        <p>Get your business listed on Wet Tropic Times directory.</p>
                        <ul>
                            <li>Increased business awareness</li>
                            <li>Advertise directly to your local community and visitors</li>
                            <li>Between xx - xx viewers per week</li>
                            <li>Be the first to be seen in the search results etc etc.</li>
                        </ul>
                        <div class="button">
                            <a href="#">Let’s get started</a>
                        </div>
                    </div>
                </div>
                <div class="item text-center">
                    <div class="iteminner">
                        <h3>Silver</h3>
                        <span>$10.99 / month</span>
                        <p>Get your business listed on Wet Tropic Times directory.</p>
                        <ul>
                            <li>Increased business awareness</li>
                            <li>Advertise directly to your local community and visitors</li>
                            <li>Between xx - xx viewers per week</li>
                            <li>Be the first to be seen in the search results etc etc.</li>
                        </ul>
                        <div class="button">
                            <a href="#">Let’s get started</a>
                        </div>
                    </div>
                </div>
                <div class="item text-center">
                    <div class="iteminner">
                        <h3>Gold</h3>
                        <span>$10.99 / month</span>
                        <p>Get your business listed on Wet Tropic Times directory.</p>
                        <ul>
                            <li>Increased business awareness</li>
                            <li>Advertise directly to your local community and visitors</li>
                            <li>Between xx - xx viewers per week</li>
                            <li>Be the first to be seen in the search results etc etc.</li>
                        </ul>
                        <div class="button">
                            <a href="#">Let’s get started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="businesscontactus">
        <div class="sitecontainer">
            <div class="heading">
                <h2>Contact Us</h2>
                <p>Our team is happy to answer your sales questiones. Fill out the form and we’ll be in <br> touch as soon as possible.</p>
                <div class="contactform">
                    <div class="formfield">
                        <div class="field">
                            <label>Name</label>
                            <input type="text" name="" placeholder="Type your name">
                        </div>
                        <div class="field">
                            <label>Email Address</label>
                            <input type="text" name="" placeholder="Type your email">
                        </div>
                    </div>
                    <div class="formfield">
                        <label>Message</label>
                        <textarea placeholder="Type here"></textarea>
                    </div>
                    <div class="button">
                        <button type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
@section('pagescript')


@stop

@endsection