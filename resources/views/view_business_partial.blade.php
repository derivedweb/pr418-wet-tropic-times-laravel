@extends('layout.main')
@section('content')

@section('pagestylesheet')
 <link rel="stylesheet" type="text/css" href="{{ asset('css/slick.css')}}">
@stop
<div class="viewareabreadcrumb">
    <div class="sitecontainer">
        <div class="viewareabreadcrumbinner">
            <h1>Cheap Orange Country</h1>
            <p>We’ll help protect the things that matter</p>
            <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>
        </div>
    </div>
</div>

<div class="busiineessmap">
    <div id="dvMap"></div>
</div>

<div class="bussinesscontactinfo">
    <div class="sitecontainer">
        <div class="items">
            <div class="item text-center">
                <img src="{{ asset('images/ic_header_location.png')}}">
                <p>195 Wellington Road, Clayton, VIC, 3168</p>
            </div>
            <div class="item text-center">
                <img src="{{ asset('images/ic_distributor_phone.png')}}">
                <p>039 985 562</p>
            </div>  
            <div class="item text-center">
                <img src="{{ asset('images/email.png')}}">
                <p>businessname@email.com</p>
            </div> 
            <div class="item text-center">
                <p><span>Closed</span> Opens Wednesday 2PM</p>
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                        <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                    </button>
                    <ul class="dropdown-menu">
                      <li>Thursday <span>2-10PM</span></li>
                      <li>Friday <span>2-10PM</span></li>
                      <li>Saturday <span>2-10PM</span></li>
                      <li>Sunday <span>2-10PM</span></li>
                      <li>Monday <span>2-10PM</span></li>
                      <li>Tuesday <span>2-10PM</span></li>
                      <li class="closemenu">Wednesday <span>Closed</span></li>
                    </ul>
                </div>
            </div> 
        </div>
    </div>
</div>


<div class="viewareacontent">
    <div class="sitecontainer">
        <h3>Services</h3>
        <p>This a single line description of the business.</p>

        <div class="socialshare">
            <div class="items">
                <ul>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_facebook.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_twitter.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_linkedin.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_youtube.png')}}"></a></li>
                    <li><a href="#"><img src="{{ asset('images/ic_business_listing_website.png')}}"></a></li>
                    <li><a data-toggle="modal" data-target="#smsshare" href="javascript:void(0);"><img src="{{ asset('images/ic_business listing_message.png')}}"></a></li>
                </ul>
            </div>
        </div>

    </div>
</div>
@section('pagescript')
<script src="{{ asset('js/slick.min.js')}}"></script>
 <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
 <script type="text/javascript">
    var markers = [
    {
        "title": 'News on Avenue',
        "lat": '-37.5400771',
        "lng": '144.9731875',
        "description": ''
    },
    ];
    window.onload = function () {
        LoadMap();
    }
    function LoadMap() {
        var mapOptions = {
            center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        };
        var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
 
        //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow();
        var index=0;
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: data.title,
                icon:'images/ic_map_pin_selected.png?i='+(index++),
            });
 
            //Attach click event to the marker.
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                    infoWindow.setContent("<div class='dirbox'>" + data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }
    }
</script>

<script>
        function KeycheckOnlyNumeric(e)
        {

            var _dom = 0;
            _dom = document.all ? 3 : (document.getElementById ? 1 : (document.layers ? 2 : 0));
            if (document.all)
                e = window.event; // for IE
            var ch = '';
            var KeyID = '';
            if (_dom == 2) {                     // for NN4
                if (e.which > 0)
                    ch = '(' + String.fromCharCode(e.which) + ')';
                KeyID = e.which;
            }
            else
            {
                if (_dom == 3) {                   // for IE
                    KeyID = (window.event) ? event.keyCode : e.which;
                }
                else {                       // for Mozilla
                    if (e.charCode > 0)
                        ch = '(' + String.fromCharCode(e.charCode) + ')';
                    KeyID = e.charCode;
                }
            }
            if ((KeyID >= 65 && KeyID <= 90) || (KeyID >= 97 && KeyID <= 122) || (KeyID >= 33 && KeyID <= 47) || (KeyID >= 58 && KeyID <= 64) || (KeyID >= 91 && KeyID <= 96) || (KeyID >= 123 && KeyID <= 126) || (KeyID == 32))//changed by jshah for stopping spaces
            {
                return false;
            }
            return true;
        }
    </script>
@stop

@endsection