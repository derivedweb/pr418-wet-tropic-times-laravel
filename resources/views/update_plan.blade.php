@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
<div class="subscriptionsplans">

    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="{{route('user.subscriptions')}}">Subscriptions</a> <span>/</span> Update your plan 
        </div>
    </div>

    <div class="subscriptionsplansinner">
        <div class="subscriptionsbox">
            <h2>Update Your Plan</h2>
            <h4>Current Plan</h4>
            @foreach($active_subscriptions as $active_subscription)
                @if($active_subscription['subscription']->status == 'active')
                @php 
                    $active_plan_id = $active_subscription['product']->id;
                @endphp
                <div class="plandetails">
                     <div class="planinfo">
                        <label>{{$active_subscription['product']->name}}</label>
                        <span>{{$active_subscription['product']->description}}</span>
                        <p>Your plan renews on {{date("d M Y",($active_subscription['subscription']->current_period_end))}}</p>
                     </div>
                </div>
                @endif
            @endforeach
            <h4>Plans Available</h4>
            
            <div class="availaableplans">
                @foreach($products->data as $product)
                @if($product->unit_label == '')
                <div class="item {{(isset($active_plan_id) && $active_plan_id==$product->id) ? 'active' : ''}}">
                    <label>{{$product->name}} - {{$product->description}}</label>
                    @if(isset($active_plan_id) && $active_plan_id==$product->id)
                        <span>Current Plan</span>
                    @else
                        <form action="{{route('user.update_plan_confirmation')}}">
                            <input type="hidden" name="new_plan_name" value="{{$product->name}}">
                            <input type="hidden" name="new_plan_description" value="{{$product->description}}">
                            <input type="hidden" name="new_plan_id" value="{{$product->id}}">
                            <input type="hidden" name="new_price_id" value="{{$product->price->id}}">
                            <button type="submit">Continue</button>
                        </form>
                        
                    @endif
                    
                </div>
                @endif
                @endforeach
                <!-- <div class="item">
                    <label>Quarterly - $59.99 / 3 months</label>
                    <button type="button">Continue</button>
                </div>
                <div class="item">
                    <label>Monthly - $14.99 / month</label>
                    <button type="button">Continue</button>
                </div> -->
            </div>
        </div>
    </div>
</div>
@section('pagescript')

@stop

@endsection