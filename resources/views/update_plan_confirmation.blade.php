@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
<div class="subscriptionsplans">
    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="{{route('user.subscriptions')}}">Subscriptions</a> <span>/</span> Update your plan 
        </div>
    </div>
    <div class="subscriptionsplansinner">
        <form method="post" action="{{ route('user.update_current_stripe_plan') }}">
            @csrf

        <div class="subscriptionsbox">
            <h2>Update Your Plan</h2>
            <h4>Changing to</h4>
            <div class="plandetails">
            @foreach($active_subscriptions as $active_subscription)
                @if($active_subscription['subscription']->status == 'active')
                @php 
                    $active_plan_id = $active_subscription['product']->id;
                @endphp
                 <div class="currentplan">
                    <label>{{$active_subscription['product']->name}}</label>
                    <span>{{$active_subscription['product']->description}}</span>
                 </div>
                @endif
            @endforeach
                
                 <div class="updatedplan">
                    <label>{{Request::get('new_plan_name')}} <span>{{Request::get('new_plan_description')}}</span></label>
                   <!--  <p>What you’ll pay monthly starting on <br>
                        16 March 2021</p> -->
                 </div>

                 <input type="hidden" name="new_price_id" value="{{Request::get('new_price_id')}}">
                 <input type="hidden" name="new_plan_id" value="{{Request::get('new_plan_id')}}">

            </div>
            <div class="buttons text-center">
                <a class="goback" href="{{route('user.update_plan')}}">Go Back</a>
                <button type="submit">Confirm</button>
            </div>
        </div>
        </form>
    </div>
</div>
@section('pagescript')

@stop

@endsection