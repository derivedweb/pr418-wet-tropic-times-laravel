@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop
 <div class="userloginregister myaccount">
    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="">Business - Account Details </a>
        </div>
    </div>

    <div class="userloginregisterinner">
        <div class="formbox">
            <h2>My Account</h2>
            {!! Form::open(array('route' => 'update_profile','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'user-update-profile')) !!}

                @if (isset($data['error']) && $data['error'] != '')
                    <div class="text-danger">
                        {{ $data['error'] }}
                    </div>
                @endif

                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="text-danger">{{ $error }}</div>
                    @endforeach
                @endif

                @if ($message = Session::get('success'))
                    <div class="text-success">
                        {{ $message }}
                    </div>
                @elseif ($message = Session::get('error'))
                    <div class="text-danger">
                        {{ $message }}
                    </div>
                @endif 

                <div class="field">
                    <label>Display Name</label>
                    <input type="text" id="displayname" name="displayname" value="{{Auth::user()->displayname}}">
                </div>
                <div class="field">
                    <label>Email</label>
                    <input type="email" id="email" name="email" value="{{Auth::user()->email}}">
                </div>
                <div class="buttons">
                    <a href="{{ route('user.change_password') }}">Change Password</a>
                    <button type="submit">Update Account</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@section('pagescript')
<!-- Jquery validate -->
<script type="text/javascript">

$(document).ready(function () {
    
    $('#user-update-profile').validate({ 
    rules: {
        "displayname": {
            required: true
        },
        "email": {
            required: true,
            email: true
        }
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
@stop

@endsection