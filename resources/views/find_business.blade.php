@extends('layout.main')
@section('content')

@section('pagestylesheet')
 
@stop

<div class="finbusiness">
    <div class="layer"></div>

    <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>

    <div class="sitecontainer">
        <div class="findbusinessform text-center">
            <h1>Find a Business</h1>
            <p>Wet Tropic Times Directory Helps You Find Your Business </p>
            <div class="form">
                <form>
                    <div class="formfield">
                        <div class="field">
                            <input type="text" name="" placeholder="What are you looking for?">
                        </div>
                        <div class="field">
                            <div class="select">
                                <select>
                                    <option>All Regions</option>
                                    <option>Regions1</option>
                                    <option>Regions2</option>
                                    <option>Regions3</option>
                                </select>
                                <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                            </div>
                        </div>
                        <div class="field">
                            <div class="select">
                                <select>
                                    <option>All Categories</option>
                                    <option>Categories1</option>
                                    <option>Categories2</option>
                                    <option>Categories3</option>
                                </select>
                                <img src="{{ asset('images/ic_images_arrow_down.png')}}">
                            </div>
                        </div>
                    </div>
                    <div class="button">
                        <button type="submit">Search</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="finbusinessmain">

    <div class="busenessheadding">
        <h2>Featured Businesses</h2>
    </div>


    <div class="busenesslisting">
        <div class="sitecontainer">
            <div class="item">
                <div class="iteminner">
                    <img src="{{ asset('images/businesses1.png')}}">
                    <h3>Cheap Orange Country</h3>
                    <p>This a single line description…</p>
                    <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                    <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <img src="{{ asset('images/businesses2.png')}}">
                    <h3>Cheap Orange Country</h3>
                    <p>This a single line description…</p>
                    <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                    <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <img src="{{ asset('images/businesses3.png')}}">
                    <h3>Cheap Orange Country</h3>
                    <p>This a single line description…</p>
                    <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                    <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <img src="{{ asset('images/businesses4.png')}}">
                    <h3>Cheap Orange Country</h3>
                    <p>This a single line description…</p>
                    <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                    <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <img src="{{ asset('images/businesses5.png')}}">
                    <h3>Cheap Orange Country</h3>
                    <p>This a single line description…</p>
                    <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                    <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <img src="{{ asset('images/businesses6.png')}}">
                    <h3>Cheap Orange Country</h3>
                    <p>This a single line description…</p>
                    <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                    <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <img src="{{ asset('images/businesses7.png')}}">
                    <h3>Cheap Orange Country</h3>
                    <p>This a single line description…</p>
                    <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                    <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                </div>
            </div>
            <div class="item">
                <div class="iteminner">
                    <img src="{{ asset('images/businesses8.png')}}">
                    <h3>Cheap Orange Country</h3>
                    <p>This a single line description…</p>
                    <h4>Unit 6/20 Duerdin Street, Bentleigh East, VIC</h4>
                    <span><img src="{{ asset('images/ic_distributor_phone.png')}}"> 03 9001 5805</span>
                </div>
            </div>  
        </div>
    </div>

    <div class="busenessheadding">
        <h2>Search by Category</h2>
    </div>

    <div class="searchbybusiness">
        <div class="sitecontainer">
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/food.png')}}">
                    <h3>Food</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/photography.png')}}">
                    <h3>Photography</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/travel.png')}}">
                    <h3>Travel</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/movies.png')}}">
                    <h3>Movies</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/fashion.png')}}">
                    <h3>Fashion</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/technology.png')}}">
                    <h3>Technology</h3>
                </div>
            </div>
        </div>
    </div>

    <div class="busenessheadding">
        <h2>Search by Region</h2>
    </div>

    <div class="searchbybusiness">
        <div class="sitecontainer">
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea6.png')}}">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea3.png')}}">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea1.png')}}">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea2.png')}}">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea5.png')}}">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="{{ asset('images/localarea4.png')}}">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div>
        </div>
    </div>
</div>
@section('pagescript')

@stop

@endsection