<?php
return [
    'app' => [
        'main-title' => 'WET Tropic Times',
        'title' => 'WET Tropic Times',
        'from-email' => 'info@ttm.com',
        'from-email-name' => 'WET Tropic Times',
        'wp-url' => 'https://wettropictimes.com.au',
    ],
    'stripe' => [
        'publishable-key' => 'pk_test_51IOtqbBrmqkUw5mZTxtWOEfY32IwDBSvlfo6M3ALCyM6KIXvngvUchz7gmAgnb1Cvr26vDIlq6dKoHjQgWF1ZlGv00BDjQxwVH',
        'secret-key' => 'sk_test_51IOtqbBrmqkUw5mZnElGEs43KyDyPOvD2bxgjn9YV46RMh67VdR2ucrgZ6uZ8oDlBhh2O2MOHHQIKcxxj4FMSws000DA9F5iIC',
        /* 'publishable-key' => 'pk_live_51IOtqbBrmqkUw5mZ9pjLrII0IchwoUnT7UJcn6QApavWvHmxHbe35nPUVvweGqFAd0hgV8coIos7sKmKvQXbA6KG00t00BXVY3',
        'secret-key' => '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',*/
    ],
];