<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            'layout.main',
            function ($view) {

               $user_id = (Auth::check() ? Auth::user()->user_id : 0);
                
               $view->with('subscription', DB::table('subscriptions')->where('user_id', $user_id)->first());
            }
        );
    }
}
