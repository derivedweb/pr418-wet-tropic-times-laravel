<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    
    protected $table = 'users';

    protected $primaryKey = 'user_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'displayname','email','password','stripe_customer_id','created_at','updated_at'
    ]; 

     protected $hidden = [
        'password','deleted_at','updated_at'
    ];

    public function getAuthEmail()
    {
        return $this->email;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

}
