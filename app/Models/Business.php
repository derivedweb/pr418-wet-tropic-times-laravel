<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Business extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    
    protected $table = 'businesses';

    protected $primaryKey = 'business_id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','stripe_customer_id','business_name','use_legal_name_as_business_name','business_summary','category_id','region_id','image','address','location','city','state','country','postcode','latitude','longitude','phone_no','email_for_business_directory','email_for_contact_you','opening_hours','facebook_link','twitter_link','linkedin_link','youtube_link','website_link','services','business_logo','created_at','updated_at', 'is_preview', 'deleted_at'
    ]; 

     protected $hidden = [
        'password','deleted_at','updated_at'
    ];

    public function getAuthEmail()
    {
        return $this->email;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

}
