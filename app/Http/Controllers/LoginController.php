<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use Hash;
use AuthenticatesUsers;
use Mail;
use DB;
use Config;

class LoginController extends Controller
{
     /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->user = Auth::user();
    
            return $next($request);
        });
    }

    public function login(Request $Request)
    { 
        if (Auth::check()) {
            return redirect(Config::get('constants.app.wp-url'));
            //return redirect()->route('user.home');
        }
        return view('login');
    }

    public function authCheck(Request $request){
        
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            
            if(!auth()->user()->is_email_verified){
                $request->session()->flush();
                Auth::logout();
                return redirect()->route('login')->with('error','Your email id is not verified. please verify your email.');
            } 

            Session::put('login_user_id', auth()->user()->user_id);

        	//return redirect()->intended('my-account');

            return redirect(Config::get('constants.app.wp-url'));
            
        } else {
        	 return redirect()->route('login')->with('error','Invalid email or password.');
        }
    }

    public function logout(Request $request) {

      $request->session()->flush();
      Auth::logout();
      return redirect('login');
    }

    public function forget_password(){
        return view('forget_password');
    }

    public function check_your_inbox(Request $request){

        //$UserID = User::where('email', $request->Email)->first()->user_id;

        $result = User::where('email', $request->Email)->first();

        if ($result === null) {
            return redirect()->route('forget_password')->with('error','Email address not found in our database.');
        }else{
            $data = array('UserID' => $result->user_id, 'Email' => $request->Email);

            // $result = Mail::send([], $data, function ($message) use ($data) {  
            //         $message->from('support@wtt.com', 'NoReply');
            //         $message->to($data['Email']) 
            //         ->subject('Reset Password')
            //         ->setBody('<p>Hi,</p>
            //             <p>Reset Password Link : <a href='.route('reset_forgot_password', MD5($data['UserID'])).'>Please click in this link to reset your password.</a></p>
            //             <p>Thank you.</p>', 'text/html');
            // });

            //return redirect()->route('user.check_your_inbox')->with('Email'=>$request->user_id);
            //echo "<pre>";print_r($data);exit;
            return view('check_your_inbox', compact('data'));
        }
    }
    
    public function reset_forgot_password($UserID){

        return view('reset_forgot_password', compact('UserID'));

    }

    public function update_reset_forgot_password(Request $Request, $UserID){

        User::whereRaw(DB::raw("MD5(user_id) = '".$UserID."'"))->update([
            'Password'=> Hash::make($Request->NewPassword),
        ]);
        return redirect()->route('login')->with('success','Password reset has been successfully.');

    }

    public function create_your_account()
    { 
        return view('create_your_account');
    }

    public function add_signup_data(Request $request)
    {

        $request['password'] = Hash::make($request['password']);
        
        $result = User::where('email', $request->email)->first();
        if ($result === null) {
            $UserID = User::create($request->all())->user_id;
     
            $data = array('UserID' => $UserID, 'Email' => $request->email);
            /*$result = Mail::send([], $data, function ($message) use ($data) {  
                    $message->from('support@fiveteams.com', 'Smart Briefing');
                    $message->to($data['Email']) 
                    ->subject('Welcome to Smart Briefing!')
                    ->setBody('<p>Hi,</p>
                        <p>Your account has been created successfully. <a href='.route('verify_email', MD5($data['UserID'])).'>Please click in this link to verify you account.</a></p>
                        <p>Thank you.</p>', 'text/html');
            });*/

            return redirect()->route('create_your_account')->with('success','A new user account has been successfully created and a confirmation has been emailed to you. Please check your email and confirm your email address to complete the registration process.'); 
        }else{
            $data = array('error'=>'Email address already exists.','displayname' => $request->displayname,'email' => $request->email);
            return view('create_your_account', compact('data'));
            //return redirect()->route('create_your_account')->with(compact('data'));
            //return redirect()->route('create_your_account')->with('error','Email address already exists.')->
                //with('displayname', $request->displayname);
        }
    }
}
