<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use Hash;
use DB;
use Config;

class SubscriptionController extends Controller
{
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function subscribe(Request $request)
    { 
        if($request->method() == 'POST'){
          $validator = request()->validate([
                'email' => 'required|unique:users,email,NULL,user_id,deleted_at,NULL',
                'password' => 'required',
            ]);  
        }

        $products = $this->get_plans();
        //echo "<pre>";print_r($products);exit;
        
        return view('subscribe', compact('products'));
    }

    public function get_plans()
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/products?active=true",
            CURLOPT_POST => 0,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
        ]);
        $result = curl_exec($curl);
        $products = json_decode($result);

        foreach ($products->data as $key => $product) {

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.stripe.com/v1/prices?product=".$product->id,
                CURLOPT_POST => 0,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
                ],
            ]);
            $result = curl_exec($curl);
            $price = json_decode($result);
            $products->data[$key]->price = $price->data[0];
            curl_close($curl);
        }

        return $products;
    }

    public function stripe_payment_request(Request $request)
    {
        $validator = request()->validate([
            'email' => 'required|unique:users,email,NULL,user_id,deleted_at,NULL',
            'password' => 'required',
        ]);

        //print_r($request->all());exit;
        /* Stripe API Start */
        $apiKey = Config::get('constants.stripe.secret-key');

        $cust_data = [
                'name' => $request->name_on_card,
                'description' => 'Stripe Payment Request',
                'email' => $request->email,
                'source' => $_POST['stripeToken']
            ];

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/customers",
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . $apiKey
            ],
            CURLOPT_POSTFIELDS => http_build_query($cust_data)
        ]);
        $res = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($res);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions",
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . $apiKey
            ],
            CURLOPT_POSTFIELDS => http_build_query([
              'customer' => $res->id,
              'items[0][price]' => $request->plan
            ])
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        curl_close($curl);
        /* Stripe API End*/


        if(isset($resp->status) && $resp->status == 'active'){

            $request['stripe_customer_id'] = $res->id;
            $request['password'] = Hash::make($request['password']);
            $user = User::create($request->all());

            $user = User::find($user->user_id); 
            Auth::login($user);

            DB::table('subscriptions')->insert([
                'stripe_subscription_id' => $resp->id,
                'user_id' => $user->user_id,
                'current_period_start' => date("Y-m-d H:i:s", ($resp->current_period_start)),
                'current_period_end' => date("Y-m-d H:i:s", ($resp->current_period_end)),
                'stripe_customer_id' => $resp->customer,
                'stripe_product_id' => $resp->plan->product,
                'stripe_price_id' => $resp->plan->id,
                'start_date' => date("Y-m-d H:i:s", ($resp->start_date)),
                'status' => $resp->status,
                'created_at' => date("Y-m-d H:i:s"),
            ]);
            //$user = User::find($user->user_id); 
            //Auth::login($user);

            return redirect()->route('login')->with('success','Your account has been successfully created.');/*and a confirmation has been emailed to you. Please check your email and confirm your email address to complete the registration process.*/
        } else{
            return redirect()->route('user.subscribe')->with('error','Payment failed.');
        }
    }

    public function get_stripe_active_subscriptions()
    {
        $user = Auth::user();

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions?customer=".$user->stripe_customer_id."&status=active",
            CURLOPT_POST => 0,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
        ]);
        $result = curl_exec($curl);
        $subscriptions = json_decode($result);
        curl_close($curl);
       
        $active_subscriptions = array();

        foreach ($subscriptions->data as $key => $subscription) {

            $resp['subscription'] = $subscription;

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.stripe.com/v1/products/".$resp['subscription']->plan->product,
                CURLOPT_POST => 1,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
                ],
            ]);
            $result = curl_exec($curl);
            $resp['product'] = json_decode($result);
            curl_close($curl);

            $active_subscriptions[] = $resp;
        }

        return $active_subscriptions;
    }

    public function subscriptions(Request $request)
    { 
        $user = Auth::user();
       // $subscriptions = DB::table('subscriptions')->where('user_id', $user->user_id)->where('status', 'active')->get();

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/customers/".$user->stripe_customer_id,
            CURLOPT_POST => 0,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
        ]);
        $result = curl_exec($curl);
        $customer = json_decode($result);
        curl_close($curl);

        $active_subscriptions = $this->get_stripe_active_subscriptions();

        $payment_methods = $this->get_payment_methods();
        
        return view('subscriptions',compact('active_subscriptions', 'payment_methods', 'customer'));
    }

    public function get_payment_methods()
    { 
        $user = Auth::user();
        $result = array();

        if(!is_null($user->stripe_customer_id)){

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.stripe.com/v1/customers/".$user->stripe_customer_id."/sources?object=card",
                CURLOPT_POST => 0,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
                ],
            ]);
            $result = curl_exec($curl);
        }
        curl_close($curl);

        return json_decode($result);
    }

    public function stripe_add_card(){

        $user = Auth::user();

        $result = array();
       
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/customers/".$user->stripe_customer_id."/sources",
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
            CURLOPT_POSTFIELDS => http_build_query([
                'source' => $_POST['stripeToken']
            ])
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        if(isset($resp->error->message)){
            return redirect()->route('user.subscriptions')->with('error', $resp->error->message);
        }
        
        curl_close($curl);

        return redirect()->route('user.subscriptions')->with('success','Card added successfully.');
    }

    public function stripe_delete_card(Request $request){

        $user = Auth::user();

        $result = array();
       
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => "https://api.stripe.com/v1/customers/".$user->stripe_customer_id."/sources/".$request->card_id,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        curl_close($curl);
        if(isset($resp->error->message)){
            echo 0;
        }
        echo 1;
    }

    public function stripe_make_card_default(Request $request){

        $user = Auth::user();

        $result = array();
       
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/customers/".$user->stripe_customer_id,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
            CURLOPT_POSTFIELDS => http_build_query([
                'default_source' => $request->card_id
            ])
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        
        curl_close($curl);
        if(isset($resp->error->message)){
            echo 0;
        }
        echo 1;
    }

    public function update_plan(Request $Request)
    { 
        $user = Auth::user();

        $active_subscriptions = $this->get_stripe_active_subscriptions();

        $products = $this->get_plans();

        return view('update_plan', compact('active_subscriptions', 'products'));
    }

    public function update_plan_confirmation(Request $Request)
    {
        
        $active_subscriptions = $this->get_stripe_active_subscriptions();

        return view('update_plan_confirmation', compact('active_subscriptions'));
    } 

    public function update_current_stripe_plan(Request $Request)
    {
        $user = Auth::user();

        $active_subscriptions = $this->get_stripe_active_subscriptions();

        $subscription_item_id = $active_subscriptions[0]['subscription']->items->data[0]->id;
        $subscription_id = $active_subscriptions[0]['subscription']->id;

        $result = array();
       
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$subscription_id,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
            CURLOPT_POSTFIELDS => http_build_query([
                'cancel_at_period_end' => "false",
                'proration_behavior' => 'create_prorations',
                "items[0][id]" => $subscription_item_id,
                'items[0][price]' => $Request->new_price_id
            ])
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        curl_close($curl);
        if(isset($resp->error->message)){
            return redirect()->route('user.subscriptions')->with('error', $resp->error->message);
        }

        DB::table('subscriptions')->where('stripe_subscription_id', $subscription_id)->delete();
        DB::table('subscriptions')->insert([
            'stripe_subscription_id' => $resp->id,
            'user_id' => $user->user_id,
            'current_period_start' => date("Y-m-d H:i:s", ($resp->current_period_start)),
            'current_period_end' => date("Y-m-d H:i:s", ($resp->current_period_end)),
            'stripe_customer_id' => $resp->customer,
            'stripe_product_id' => $resp->plan->product,
            'stripe_price_id' => $resp->plan->id,
            'start_date' => date("Y-m-d H:i:s", ($resp->start_date)),
            'status' => $resp->status,
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        return redirect()->route('user.subscriptions')->with('success','Your plan has been updated successfully.');
    }

    public function cancel_plan_confirmation(Request $Request)
    {
         return view('cancel_plan_confirmation');
    }

    public function cancel_subscription_from_stripe(Request $Request)
    {
        $result = array();

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$Request->subscription_id,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
            CURLOPT_POSTFIELDS => http_build_query([
              'cancel_at_period_end' => 'true',
            ])
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        curl_close($curl);

        if(isset($resp->error->message)){
            return redirect()->route('user.subscriptions')->with('error',$resp->error->message);
        }
        
        DB::table('subscriptions')->where('stripe_subscription_id', $Request->subscription_id)->update(['status' => 'cancelled']);

        return redirect()->route('user.subscriptions', ['flag' => 'renew'])->with('success','Your plan has been cancelled successfully. Your plan will be active until '.date("d M Y",($resp->current_period_end)));
    }

    public function renew_plan_confirmation(Request $Request)
    {
         return view('renew_plan_confirmation');
    }

    public function renew_plan_from_stripe(Request $Request)
    {
         $active_subscriptions = $this->get_stripe_active_subscriptions();

        $subscription_item_id = $active_subscriptions[0]['subscription']->items->data[0]->id;
        $subscription_id = $active_subscriptions[0]['subscription']->id;

        $result = array();
       
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions/".$subscription_id,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
            CURLOPT_POSTFIELDS => http_build_query([
                'cancel_at_period_end' => "false",
                'proration_behavior' => 'none',
            ])
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        curl_close($curl);

        if(isset($resp->error->message)){
            return redirect()->route('user.subscriptions')->with('error',$resp->error->message);
        }

        DB::table('subscriptions')->where('stripe_subscription_id', $subscription_id)->update(['status' => 'active']);

        return redirect()->route('user.subscriptions')->with('success','Your plan has been renewed successfully.');
    }
    
}
