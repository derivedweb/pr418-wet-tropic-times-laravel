<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use Hash;

class HomeController extends Controller
{
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function index(Request $Request)
    { 
        return view('home');
    }

    // public function change_password(Request $Request)
    // { 
    //     return view('change_password');
    // }
    
    // public function check_your_inbox(Request $Request)
    // { 
    //     return view('check_your_inbox');
    // }

    // public function create_your_account(Request $Request)
    // { 
    //     return view('create_your_account');
    // }

    public function find_business(Request $Request)
    { 
        return view('find_business');
    }
    
    // public function forget_password(Request $Request)
    // { 
    //     return view('forget_password');
    // }

    

    public function search_results(Request $Request)
    { 
        return view('search_results');
    }

    /*public function subscribe(Request $Request)
    { 
        return view('subscribe');
    }

    public function subscription(Request $Request)
    { 
        return view('subscription');
    }

    public function subscriptions_renew_plan(Request $Request)
    { 
        return view('subscriptions_renew_plan');
    }

    public function update_plan(Request $Request)
    { 
        return view('update_plan');
    }*/
}
