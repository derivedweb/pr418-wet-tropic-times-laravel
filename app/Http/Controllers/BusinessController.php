<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use App\Models\Business;
use App\Models\Region;
use App\Models\Category;
use Hash;
use DB;
use Image;

class BusinessController extends Controller
{
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function business_details(Request $Request)
    { 
        $regions = Region::get();
        $categories = Category::get();

        return view('business.business_details', compact('categories','regions'));
    }

    public function add_business_details(Request $request){ 
      
        $data = $request->validate([
            'business_name' => 'required',
            'category_id' => 'required',
            'region_id' => 'required',
            'business_summary' => 'required',
            'address' => 'required',
            'phone_no' => 'required',
            'email_for_contact_you' => 'required',
            'email_for_business_directory' => 'required',
            'address' => 'required'
        ]);
        $request['user_id'] = Auth::user()->user_id;
        $request['use_legal_name_as_business_name'] = isset($request->use_legal_name_as_business_name) ? 1 : 0;

        if($request->hasFile('business_image'))
        { 
            request()->validate([
                'business_image' => 'image|mimes:jpeg,png,jpg,gif',
            ]); 

            $file = $request->file('business_image');

            $imageName = date("dmYHis").substr(uniqid('', true), -3).'.'.$file->getClientOriginalExtension();  
            // Save thumbnail image
            $img = Image::make($file->getRealPath());

            $img->resize(100,100, function ($constraint) {

            $constraint->aspectRatio();

            })->save(public_path('/business_images/thumb/').$imageName);
            
            // Save original size image
            $file->move(public_path('business_images'),$imageName);

            $request['image'] = $imageName;

        }

        if($request->hasFile('bus_logo'))
        { 
            request()->validate([
                'bus_logo' => 'image|mimes:jpeg,png,jpg,gif',
            ]); 

            $file = $request->file('bus_logo');

            $imageName = date("dmYHis").substr(uniqid('', true), -3).'.'.$file->getClientOriginalExtension();  
           
            // Save original size image
            $file->move(public_path('business_images'),$imageName);

            $request['business_logo'] = $imageName;

        }
        if(isset($_REQUEST['preview'])){
            $request['deleted_at'] = date("Y-m-d H:i:s");
            $request['is_preview'] = 1;
        }

        $business = Business::create($request->all());
        User::where('user_id', Auth::user()->user_id)->update(['business_id' => $business->business_id]);

        if($request->hasFile('business_images'))
        {   
            $files = $request->file('business_images');

            foreach($files as $key => $file){

                $imageName = rand().time().'.'.$file->getClientOriginalExtension();

                // Save thumbnail image
                $img = Image::make($file->getRealPath());

                $img->resize(100, 100, function($constraint) {

                $constraint->aspectRatio();

                })->save(public_path('/business_images/thumb/').$imageName);

                //Save original size image
                $file->move(public_path('business_images'), $imageName);

                DB::table('business_images')->insert([
                    'business_id' => $business->business_id,
                    'image_name' => $imageName
                ]);
             }
         }

         foreach($request->day as $key => $day){
            DB::table('business_times')->insert([
                'business_id' => $business->business_id,
                'day' => $day,
                'open_time' => date("H:i:s", strtotime($request->open_time[$key])),
                'close_time' => date("H:i:s", strtotime($request->close_time[$key]))
            ]); 
         }

         if(isset($_REQUEST['preview'])){
            return redirect()->route('business.preview', ['business_id' => $business->business_id, 'product_id' => $request->product_id]);
         }

         return redirect()->route('business.subscription', ['product_id' => $request->product_id, 'business_id' => $business->business_id])->with('success','Business added successfully.');
        
    }
    
    public function edit_business_details(Request $Request)
    { 
        $regions = Region::get();
        $categories = Category::get();

        $business = DB::table('businesses')->where('business_id', Auth::user()->business_id)->first();
        $business_times = DB::table('business_times')->where('business_id', Auth::user()->business_id)->get();
        $business_images = DB::table('business_images')->where('business_id', Auth::user()->business_id)->get();

        return view('edit_business_details', compact('regions', 'categories', 'business', 'business_times','business_images'));
    }
    
    public function business_listing(Request $Request)
    { 
        return view('business_listing');
    }

    public function remove_business_image(Request $request){
        if(isset($request->business_image_id)){
            $business_images = DB::table('business_images')->where('business_id', Auth::user()->business_id)->delete();
        } else {
            $business_images = DB::table('businesses')->where('business_id', $request->business_id)->update(['image' => NULL]);
        }
    }

    public function update_business_details(Request $request){ 
      
        $data = $request->validate([
            'business_name' => 'required',
            'category_id' => 'required',
            'region_id' => 'required',
            'business_summary' => 'required',
            'address' => 'required',
            'phone_no' => 'required',
            'email_for_contact_you' => 'required',
            'email_for_business_directory' => 'required',
            'address' => 'required'
        ]);
      
        $request['use_legal_name_as_business_name'] = isset($request->use_legal_name_as_business_name) ? 1 : 0;

        if($request->hasFile('business_image'))
        { 
            request()->validate([
                'business_image' => 'image|mimes:jpeg,png,jpg,gif',
            ]); 

            $file = $request->file('business_image');

            $imageName = date("dmYHis").substr(uniqid('', true), -3).'.'.$file->getClientOriginalExtension();  
            // Save thumbnail image
            $img = Image::make($file->getRealPath());

            $img->resize(100,100, function ($constraint) {

            $constraint->aspectRatio();

            })->save(public_path('/business_images/thumb/').$imageName);
            
            // Save original size image
            $file->move(public_path('business_images'),$imageName);

            $request['image'] = $imageName;

        }

        if($request->hasFile('bus_logo'))
        { 
            request()->validate([
                'bus_logo' => 'image|mimes:jpeg,png,jpg,gif',
            ]); 

            $file = $request->file('bus_logo');

            $imageName = date("dmYHis").substr(uniqid('', true), -3).'.'.$file->getClientOriginalExtension();  
           
            // Save original size image
            $file->move(public_path('business_images'),$imageName);

            $request['business_logo'] = $imageName;

        }

        DB::table('businesses')->where('business_id', Auth::user()->business_id)->update(['deleted_at' => NULL]);
        $request['is_preview'] = 0;
        $business = Business::find(Auth::user()->business_id);
        $business->update($request->all());

        if($request->hasFile('business_images'))
        {   
            $files = $request->file('business_images');

            foreach($files as $key => $file){

                $imageName = rand().time().'.'.$file->getClientOriginalExtension();

                // Save thumbnail image
                $img = Image::make($file->getRealPath());

                $img->resize(100, 100, function($constraint) {

                $constraint->aspectRatio();

                })->save(public_path('/business_images/thumb/').$imageName);

                //Save original size image
                $file->move(public_path('business_images'), $imageName);

                DB::table('business_images')->insert([
                    'business_id' => $business->business_id,
                    'image_name' => $imageName
                ]);
             }
         }

         DB::table('business_times')->where('business_id', $business->business_id)->delete();
         foreach($request->day as $key => $day){
            DB::table('business_times')->insert([
                'business_id' => $business->business_id,
                'day' => $day,
                'open_time' => date("H:i:s", strtotime($request->open_time[$key])),
                'close_time' => date("H:i:s", strtotime($request->close_time[$key]))
            ]); 
         }

         if(isset($request->flag) && $request->flag == 'edit'){
            return redirect()->route('business.subscription', ['product_id' => $request->product_id, 'business_id' => $business->business_id])->with('success','Business added successfully.');
         } else {
            return redirect()->route('business.edit_business_details')->with('success','Business updated successfully.');
         }
        
        
    }

    public function make_unlist_profile(Request $request){
        Business::where('business_id', Auth::user()->business_id)->update([
            'is_unlist' => 1
        ]);

        return redirect()->route('business.edit_business_details')->with('success','Your profile has been unlisted successfully.');
    }

    public function preview(Request $request){
        $business = DB::table('businesses')->where('business_id', $request->business_id)->first();

        $businessImages = DB::table('business_images')->where('business_id', $business->business_id)->get();
        $data['businessImages'] = $businessImages;
        
        $businessTimes = DB::table('business_times')->where('business_id', $business->business_id)->get();
        $data['businessTimes'] = $businessTimes;

        $data['product_id'] = $request->product_id;

        return view('business.preview', compact('business', 'data'));
        
    }

    public function find_business(Request $Request)
    {   
        $categories = Category::get();
        $data['categories'] = $categories;

        $regions = Region::get();
        $data['regions'] = $regions;

        $businesses = Business::where('status', 'approved')->where('is_unlist', 0)->limit(8)->get();
        $data['businesses'] = $businesses;

        return view('business.find_business', compact('data'));
    }

    public function search_results(Request $request){ 

        $data['postData'] = $request->all();
        
        $categories = Category::get();
        $data['categories'] = $categories;

        $regions = Region::get();
        $data['regions'] = $regions;

        $mainquery = Business::select('*', DB::raw("(SELECT concat(open_time,'-',close_time) FROM business_times WHERE business_id = businesses.business_id AND LOWER(day)=LOWER('".date('l')."')) as business_time"))->where('status', 'approved')->where('is_unlist', 0);
        if(isset($request->category_id) && $request->category_id != ''){
            $mainquery->where('category_id', $request->category_id);
        }
        if(isset($request->region_id) && $request->region_id != ''){
            $mainquery->where('region_id', $request->region_id);
        }
        if(isset($request->whatyoulookingfor) && $request->whatyoulookingfor != ''){
            $mainquery->where(function ($query) use($request){
                $query->where('business_name', 'like', '%' . $request->whatyoulookingfor . '%')
                ->orwhere('business_summary', 'like', '%' . $request->whatyoulookingfor . '%');
            });
        }
        $businesses = $mainquery->where('status', 'approved')->where('is_unlist', 0);

        if ($request->ajax()) {

            return response()->json([
                'businesses' => $businesses->get(),
                'body' => view('business.load_business', ['businesses' => $businesses->paginate(5)])->render()
            ]);
        } else {
            $businesses_map_markers = $mainquery->get();
            $businesses = $mainquery->paginate(5);
            
        }

        return view('business.search_results', compact('data', 'businesses', 'businesses_map_markers'));
    }

    public function view_business($business_id){

        $businessDetail = Business::where('business_id', $business_id)->first();
        $data['businessDetail'] = $businessDetail;

        $categories =  Category::where('category_id', $businessDetail->category_id)->first();
        $data['categories'] = $categories;

        $regions = Region::where('region_id', $businessDetail->region_id)->first();
        $data['regions'] = $regions;

        $businessImages = DB::table('business_images')->where('business_id', $businessDetail->business_id)->get();
        $data['businessImages'] = $businessImages;
        
        $businessTimes = DB::table('business_times')->where('business_id', $businessDetail->business_id)->get();
        $data['businessTimes'] = $businessTimes;

        $recommendedBusinesses = Business::where('status', 'approved')->where('is_unlist', 0)
        ->where('business_id','!=', $businessDetail->business_id)
        ->where(function ($query) use ($businessDetail){
            $query->where('category_id', $businessDetail->category_id)
            ->orWhere('region_id', $businessDetail->region_id);
            })
        ->get();
        $data['recommendedBusinesses'] = $recommendedBusinesses;

        //echo "<pre>";print_r($data);exit;
        return view('business.view_business', compact('data'));
    }

    public function view_business_partial(Request $Request, $business_id)
    { 
        $businessDetail = Business::where('business_id', $business_id)->first();
        $data['businessDetail'] = $businessDetail;

        $businessTimes = DB::table('business_times')->where('business_id', $businessDetail->business_id)->get();
        $data['businessTimes'] = $businessTimes;

        return view('business.view_business_partial', compact('data'));
    }

    public function send_business_details_sms(Request $request){


        $phone = $request->phone_no;
        $business = Business::where('business_id', $request->business_id)->first();
        $msg = '';
        $msg .= $business->business_name;
        $msg .= $business->address;
        $msg .= $business->phone_no;


        /*$ID = '1234567890abcdef1234567890abcdef12';
        $token = '1234567890abcdef1234567890abcdef';
        $service = 'AB1234567890abcdef1234567890abcdef';
        $url = 'https://api.twilio.com/2010-04-01/Accounts/' . $ID . '/Messages.json';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

        curl_setopt($ch, CURLOPT_HTTPAUTH,CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD,$ID . ':' . $token);

        curl_setopt($ch, CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            'To=' . rawurlencode('+' . '01234567890') .
            '&MessagingServiceSid=' . $service .
            //'&From=' . rawurlencode('+18885550000') .
            '&Body=' . rawurlencode('test'));

        $resp = curl_exec($ch);
        curl_close($ch);
        return json_decode($resp,true);


        $id = "SANDBOX_ACC_ID";
        $token = "SANDBOX_TOKEN";
        $url = "https://api.twilio.com/2010-04-01/Accounts/$id/SMS/Messages";
        $from = "+MAGICNUMBER";
        $to = "+XXXXXXXXXX"; // twilio trial verified number
        $body = "using twilio rest api from Fedrick";
        $data = array (
            'From' => $from,
            'To' => $to,
            'Body' => $body,
        );
        $post = http_build_query($data);
        $x = curl_init($url );
        curl_setopt($x, CURLOPT_POST, true);
        curl_setopt($x, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($x, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($x, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($x, CURLOPT_USERPWD, "$id:$token");
        curl_setopt($x, CURLOPT_POSTFIELDS, $post);
        $y = curl_exec($x);
        curl_close($x);
        var_dump($post);
        var_dump($y);*/
        //print_r($request->all());exit;

        if(1){
            return redirect()->route('business.view_business', $business->business_id)->with('success','SMS has been sent successfully.');
        } else {
            return redirect()->route('business.view_business', $business->business_id)->with('error','SMS not sent.');
        }
    }
}
