<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use Hash;
use DB;
class UsersController extends Controller
{
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    
    public function my_account(Request $Request){

        return view('user.my_account');

    }

    public function update_profile(Request $Request){

        $checkEmail = User::where('email', $Request->email)->where('user_id', '!=' ,Auth::user()->user_id)->first();
        if($checkEmail === null){
            $user = DB::table('users')->where('user_id', Auth::user()->user_id)->first();

            DB::table('users')
            ->where('user_id', Auth::user()->user_id)
            ->update([
                'displayname'=> $Request->displayname,
                'email'=> $Request->email
            ]);

            if($user->displayname != $Request->displayname && $user->email != $Request->email){
                return redirect()->route('user.my_account')->with('success','Display name and email changed successfully.');
            } else if($user->displayname != $Request->displayname){
                return redirect()->route('user.my_account')->with('success','Display name changed successfully.');
            } else if($user->email != $Request->email){
                return redirect()->route('user.my_account')->with('success','Email changed successfully.');
            }
            
        }else{
            return redirect()->route('user.my_account')->with('error','Email address already exists, Please use different email.');
        }
    }

    public function change_password(Request $Request){ 

        return view('user.change_password');

    }
    public function update_change_password(Request $Request){

        if (Auth::attempt(['email' => Auth::user()->email, 'password' => $Request->OldPassword])) {
            DB::table('users')
            ->where('user_id', Auth::user()->user_id)
            ->update([
                'password'=> Hash::make($Request->NewPassword)
            ]);
            return redirect()->route('user.change_password')->with('success','Password has been successfully changed.');
        }else{
            return redirect()->route('user.change_password')->with('error','Old password does not match.');
        }
    }
}
