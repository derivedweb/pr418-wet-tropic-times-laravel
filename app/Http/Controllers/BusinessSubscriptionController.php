<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Session;
use App\Models\User;
use App\Models\Business;
use Hash;
use Config;
use DB;

class BusinessSubscriptionController extends Controller
{
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function subscription(Request $request)
    { 
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/products/".$request->product_id,
            CURLOPT_POST => 0,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
            ],
        ]);
        $result = curl_exec($curl);
        $product = json_decode($result);
        curl_close($curl);

        if(isset($product->id) && !empty($product->id)){
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.stripe.com/v1/prices?product=".$product->id,
                CURLOPT_POST => 0,
                CURLOPT_HTTPHEADER => [
                    "Authorization: Bearer " . Config::get('constants.stripe.secret-key')
                ],
            ]);
            $result = curl_exec($curl);
            $price = json_decode($result);
            $product->price = $price->data[0];
            curl_close($curl);
        }

        return view('subscription', compact('product'));
    }

    public function stripe_payment_request(Request $request)
    {
        /* Stripe API Start */
        $apiKey = Config::get('constants.stripe.secret-key');

        $user = Auth::user();
        $business = Business::where('business_id', $request->business_id)->first();

        $cust_data = [
            'name' => $request->name_on_card,
            'description' => 'Stripe Payment Request',
            'email' => $business->email_for_business_directory,
            'source' => $_POST['stripeToken']
        ];
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/customers",
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . $apiKey
            ],
            CURLOPT_POSTFIELDS => http_build_query($cust_data)
        ]);
        $res = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($res);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => "https://api.stripe.com/v1/subscriptions",
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => [
                "Authorization: Bearer " . $apiKey
            ],
            CURLOPT_POSTFIELDS => http_build_query([
              'customer' => $res->id,
              'items[0][price]' => $request->price_id
            ])
        ]);
        $resp = curl_exec($curl);
        $resp = json_decode($resp);
        curl_close($curl);
        /* Stripe API End*/

        if(isset($resp->status) && $resp->status == 'active'){

            Business::where('business_id', $request->business_id)->update(['stripe_customer_id' => $res->id]);

            DB::table('business_subscriptions')->insert([
                'stripe_subscription_id' => $resp->id,
                'user_id' => $user->user_id,
                'business_id' => $request->business_id,
                'current_period_start' => date("Y-m-d H:i:s", ($resp->current_period_start)),
                'current_period_end' => date("Y-m-d H:i:s", ($resp->current_period_end)),
                'stripe_customer_id' => $resp->customer,
                'stripe_product_id' => $resp->plan->product,
                'stripe_price_id' => $resp->plan->id,
                'start_date' => date("Y-m-d H:i:s", ($resp->start_date)),
                'status' => $resp->status,
                'created_at' => date("Y-m-d H:i:s"),
            ]);

            return redirect()->route('business.subscription', ['product_id' => $request->product_id, 'business_id' => $request->business_id, 'success' => 1])->with('success','Your business details added successfully.');
        } else{
            return redirect()->route('business.subscription', ['product_id' => $request->product_id, 'business_id' => $request->business_id])->with('error','Payment failed.');
        }
    }
    
}
