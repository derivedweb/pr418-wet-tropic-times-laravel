<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index')->name('user.home');

Route::get('login', 'LoginController@login')->name('user.login');
Route::post('login', 'LoginController@authCheck')->name('login');

Route::get('home', 'HomeController@index')->name('user.home');

//Route::get('find-business', 'HomeController@find_business')->name('user.find_business');

//Route::get('search-results', 'HomeController@search_results')->name('user.search_results');

//Route::get('business-listing', 'BusinessController@business_listing')->name('user.business_listing');



//Route::get('check-your-inbox', 'HomeController@check_your_inbox')->name('user.check_your_inbox');

Route::get('subscribe', 'SubscriptionController@subscribe')->name('user.subscribe');
	Route::post('subscribe', 'SubscriptionController@subscribe')->name('user.subscribe');
Route::post('stripe-payment-request', 'SubscriptionController@stripe_payment_request')->name('user.stripe_payment_request');


Route::get('find-business', 'BusinessController@find_business')->name('business.find_business');
Route::post('search-results', 'BusinessController@search_results')->name('business.search_results');
Route::get('search-results', 'BusinessController@search_results')->name('business.search_results');
Route::get('view-business-partial/{id}', 'BusinessController@view_business_partial')->name('business.view_business_partial');
Route::get('view-business/{id}', 'BusinessController@view_business')->name('business.view_business');
Route::post('send-business-details-sms', 'BusinessController@send_business_details_sms')->name('business.send_business_details_sms');


Route::group(['middleware' => 'auth',  'prefix' => ''], function(){
	
	Route::get('change-password', 'UsersController@change_password')->name('user.change_password');

	Route::get('my-account', 'UsersController@my_account')->name('user.my_account');
	Route::get('business-myaccount-detail', 'UsersController@my_account')->name('user.business_myaccount_detail');

	Route::post('update-profile', 'UsersController@update_profile')->name('update_profile');

	// User Subscription Routes
	
	
	Route::post('stripe-add-card', 'SubscriptionController@stripe_add_card')->name('user.stripe_add_card');
	Route::get('stripe-delete-card', 'SubscriptionController@stripe_delete_card')->name('user.stripe_delete_card');
	Route::get('stripe-make-card-default', 'SubscriptionController@stripe_make_card_default')->name('user.stripe_make_card_default');
	Route::get('subscriptions', 'SubscriptionController@subscriptions')->name('user.subscriptions');
	Route::get('update-plan', 'SubscriptionController@update_plan')->name('user.update_plan');
	Route::get('update-plan-confirmation', 'SubscriptionController@update_plan_confirmation')->name('user.update_plan_confirmation');
	Route::post('update-current-stripe-plan', 'SubscriptionController@update_current_stripe_plan')->name('user.update_current_stripe_plan');
	Route::get('cancel-plan-confirmation', 'SubscriptionController@cancel_plan_confirmation')->name('user.cancel_plan_confirmation');
	Route::post('cancel-subscription-from-stripe', 'SubscriptionController@cancel_subscription_from_stripe')->name('user.cancel_subscription_from_stripe');
	Route::get('renew-plan-confirmation', 'SubscriptionController@renew_plan_confirmation')->name('user.renew_plan_confirmation');
	Route::post('renew-plan-from-stripe', 'SubscriptionController@renew_plan_from_stripe')->name('user.renew_plan_from_stripe');


	Route::get('business-details', 'BusinessController@business_details')->name('business.business_details');
	Route::POST('add-business-details', 'BusinessController@add_business_details')->name('add_business_details');
	Route::get('edit-business-details', 'BusinessController@edit_business_details')->name('business.edit_business_details');
	Route::post('update-business-details', 'BusinessController@update_business_details')->name('business.update_business_details');
	Route::get('remove-business-image', 'BusinessController@remove_business_image')->name('business.remove_business_image');
	Route::get('make-unlist-profile', 'BusinessController@make_unlist_profile')->name('business.make_unlist_profile');
	Route::get('business-preview', 'BusinessController@preview')->name('business.preview');
	


	// Business Subscription Routes
	Route::get('business-subscription', 'BusinessSubscriptionController@subscription')->name('business.subscription');
	Route::post('business-stripe-payment-request', 'BusinessSubscriptionController@stripe_payment_request')->name('business.stripe_payment_request');
});



Route::get('forget-password', 'LoginController@forget_password')->name('forget_password');
Route::POST('check-your-inbox', 'LoginController@check_your_inbox')->name('check_your_inbox');
Route::get('check-your-inbox', 'LoginController@check_your_inbox')->name('user.check_your_inbox');
Route::get('reset-forgot-password/{id}', 'LoginController@reset_forgot_password')->name('reset_forgot_password');
Route::post('update-reset-forgot-password/{id}', 'LoginController@update_reset_forgot_password')->name('update_reset_forgot_password');

Route::get('create-your-account', 'LoginController@create_your_account')->name('create_your_account');
Route::post('add-signup-data', 'LoginController@add_signup_data')->name('add_signup_data');

Route::post('update-change-password', 'UsersController@update_change_password')->name('update_change_password');


//Route::post('send-forgot-password', 'LoginController@send_forgot_password')->name('send_forgot_password');

Route::get('logout', 'LoginController@logout')->name('logout');

Route::get('clear', function() {

   Artisan::call('cache:clear');
   Artisan::call('config:clear');
   Artisan::call('config:cache');
   Artisan::call('view:clear');

   return "Cleared!";

});

Route::get('{any?}', function ($any = null) {

});

Route::get('{any?}/{any2?}', function ($any = null) {

});
Route::get('{any?}/{any2?}/{any3?}', function ($any = null) {

});