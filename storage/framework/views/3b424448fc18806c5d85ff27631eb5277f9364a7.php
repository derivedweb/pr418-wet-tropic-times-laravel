<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>
<div class="subscriptionsplans">
    <div class="subscriptionsplansinner">

        

        <div class="subscriptionsbox">
            <h2>Subscriptions</h2>

            <?php if($message = Session::get('success')): ?>
                <div class="text-success">
                    <?php echo e($message); ?>

                </div>
            <?php elseif($message = Session::get('error')): ?>
                <div class="text-danger">
                    <?php echo e($message); ?>

                </div>
            <?php endif; ?>

            <h4>Current Plan</h4>
            <?php $__currentLoopData = $active_subscriptions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $active_subscription): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($active_subscription['subscription']->status == 'active' && (($active_subscription['subscription']->cancel_at_period_end==false) || ($active_subscription['subscription']->cancel_at_period_end==true && Request::get('flag') == 'renew'))): ?>
                <div class="plandetails">
                     <div class="planinfo">
                        <label><?php echo e($active_subscription['product']->name); ?></label>
                        <span><?php echo e($active_subscription['product']->description); ?></span>
                        <?php if(Request::get('flag') == 'renew'): ?>
                          <p>Your plan will be active until <?php echo e(date("d M Y",($active_subscription['subscription']->current_period_end))); ?></p>
                        <?php else: ?>
                          <p>Your plan renews on <?php echo e(date("d M Y",($active_subscription['subscription']->current_period_end))); ?></p>
                        <?php endif; ?>
                        
                     </div>
                     <?php if(Request::get('flag') == 'renew'): ?>
                      <div class="planbuttons">
                        <a class="renew" href="<?php echo e(route('user.renew_plan_confirmation')); ?>?plan_title=<?php echo e($active_subscription['product']->name); ?>&plan_desc=<?php echo e($active_subscription['product']->description); ?>&end_period=<?php echo e($active_subscription['subscription']->current_period_end); ?>">Renew Plan</a>
                      </div>
                     <?php else: ?>
                     <div class="planbuttons">
                        <a class="update" href="<?php echo e(route('user.update_plan')); ?>"><img src="<?php echo e(asset('images/ic_subscription_update.png')); ?>"> Update Plan</a>
                        <a class="cancel" href="<?php echo e(route('user.cancel_plan_confirmation')); ?>?plan_title=<?php echo e($active_subscription['product']->name); ?>&plan_desc=<?php echo e($active_subscription['product']->description); ?>&end_period=<?php echo e($active_subscription['subscription']->current_period_end); ?>&subscription_id=<?php echo e($active_subscription['subscription']->id); ?>"><img src="<?php echo e(asset('images/ic_subscription_cancel.png')); ?>"> Cancel</a>
                     </div>
                     <?php endif; ?>
                </div>
                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <h4>Payment Methods</h4>
            <div class="paymentmethods">
                <?php $__currentLoopData = $payment_methods->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment_method): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="item item-<?php echo e($payment_method->id); ?> <?php echo e(($customer->default_source ==$payment_method->id) ? 'active' : ''); ?>">
                    <img src="<?php echo e(asset('images/ic_payment_visa.png')); ?>">
                    <span>****  <?php echo e($payment_method->last4); ?></span>
                    <span>Exp <?php echo e($payment_method->exp_month); ?>/<?php echo e($payment_method->exp_year); ?></span>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                            <img src="<?php echo e(asset('images/ic_payment_edit.png')); ?>">
                        </button>
                        <ul class="dropdown-menu">
                          <li class="make-default" data-card-id="<?php echo e($payment_method->id); ?>"><a href="javascript:void(0);">Make Default</a></li>
                          <li class="delete" data-card-id="<?php echo e($payment_method->id); ?>"><a href="javascript:void(0);">Delete</a></li>
                        </ul>
                    </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!-- <div class="item">
                    <img src="<?php echo e(asset('images/ic_payment_master.png')); ?>">
                    <span>****  4242</span>
                    <span>Exp 04/24</span>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"> 
                            <img src="<?php echo e(asset('images/ic_payment_edit.png')); ?>">
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="#">Make Default</a></li>
                          <li class="delete"><a href="#">Delete</a></li>
                        </ul>
                    </div>
                </div> -->
            </div>

            <div class="addpaymentmethod">
                <a href="#" data-toggle="modal" data-target="#addCardModal"><img src="<?php echo e(asset('images/ic_payment_add.png')); ?>"> Add payment method</a>
            </div>

        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addCardModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Add New Card</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo e(route('user.stripe_add_card')); ?>" method="post" id="payment-form" data-rewardful>
          <?php echo csrf_field(); ?>
          <div class="row">
            <div class="col-md-12">
            <label for="card-element">
              Credit or debit card
            </label>
            <div id="card-element">
              <!-- A Stripe Element will be inserted here. -->
            </div>

            <!-- Used to display form errors. -->
            <div id="card-errors" role="alert"></div>
            </div>
            <div class="col-md-12 text-center">
              <button class="btn btn-success card-submit">Submit</button>
            </div>
          </div>
        </form>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div> -->
    </div>
  </div>
</div>
<?php $__env->startSection('pagescript'); ?>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
var stripe = Stripe("<?php echo e(Config::get('constants.stripe.publishable-key')); ?>");
var elements = stripe.elements();
// Custom styling can be passed to options when creating an Element.
var style = {
  base: {
    // Add your base input styles here. For example:
    fontSize: '16px',
    color: '#32325d',
  },
};

// Create an instance of the card Element.
var card = elements.create('card', {hidePostalCode: true, style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Create a token or display an error when the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}


/*$(document).on("click", ".item", function(){
    $(".item").removeClass("active");
    $(this).addClass('active');
});*/

$(document).on("click", ".delete", function(){
    var card_id = $(this).data("card-id");
    if(confirm('Are you sure, you want to delete it?')){
        $.ajax({
            url : "<?php echo e(route('user.stripe_delete_card')); ?>",
            data : { card_id:card_id },
            dataType: 'json',
        }).done(function (data) { 
            $(".item-"+card_id).hide();
        });
    }
});

$(document).on("click", ".make-default", function(){
    var card_id = $(this).data("card-id");
    $.ajax({
        url : "<?php echo e(route('user.stripe_make_card_default')); ?>",
        data : { card_id:card_id },
        dataType: 'json',
    }).done(function (data) { 
        $(".item").removeClass("active");
        $(".item-"+card_id).addClass('active');
    });
    
});


  </script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/subscriptions.blade.php ENDPATH**/ ?>