<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
 <style type="text/css">
#image1-error{
    /*padding-top: 76px;*/
    margin-top:-35px;
}
 </style>

<?php $__env->stopSection(); ?>
 
<div class="businessdetails">
    <div class="businessdetailsinner">
        <div class="businessdetailsbox">
            <h2>Business Profile</h2>
            <div class="profileform">
            <?php echo Form::open(array('route' => 'add_business_details','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'add-business-details', 'enctype' => 'multipart/form-data')); ?>    

                <?php if($message = Session::get('success')): ?>
                    <div class="text-success">
                        <?php echo e($message); ?>

                    </div>
                <?php elseif($message = Session::get('error')): ?>
                    <div class="text-danger">
                        <?php echo e($message); ?>

                    </div>
                <?php endif; ?>

                <?php if($errors->any()): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="text-danger"><?php echo e($error); ?></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

                <!-- Gold - prod_Jlt063X2Fbdq8Q
                Silver - prod_JlszijxBbmy45T
                Bronze - prod_JlsygotTSpsjXZ -->
                <input type="hidden" name="product_id" value="<?php echo e(Request::get('product_id')); ?>">

                <div class="formfield">
                    <div class="fields">
                        <div class="field">
                            <label>Business Name*</label>
                            <input type="text" name="business_name" id="business_name" placeholder="Type your business name" required>
                            <div class="radio">
                                <input type="radio" name="use_legal_name_as_business_name" value="1"> Use legal name as business name
                            </div>
                        </div>

                        <div class="field">
                            <label>Category*</label>
                            <div class="select">
                                <select name="category_id" id="category_id" required>
                                     <option value=""></option>
                                    <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <option value="<?php echo e($value->category_id); ?>"><?php echo e($value->category_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <img src="<?php echo e(asset('images/ic_images_arrow_down.png')); ?>">
                            </div>
                        </div>

                        <div class="field">
                            <label>Region*</label>
                            <div class="select">
                                <select name="region_id" id="region_id" required>
                                    <option value=""></option>
                                    <?php $__currentLoopData = $regions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <option value="<?php echo e($value->region_id); ?>"><?php echo e($value->region_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <img src="<?php echo e(asset('images/ic_images_arrow_down.png')); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="field">
                            <label>Business Summary*</label>
                            <div class="textarea">
                                <div class="textarea">
                                    <textarea name="business_summary" id="business_summary" placeholder="Type here" required></textarea>
                                    <span>Minimum 50 words.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="formfield">
                    <div class="fields">
                        <div class="field">
                            <label>Address*</label>
                            <input type="text" name="address" id="address" placeholder="Type here" required>
                            <input type="hidden" name="location" id="location">
                            <input type="hidden" name="latitude" id="latitude">
                            <input type="hidden" name="longitude" id="longitude">
                            <input type="hidden" name="city" id="city">
                            <input type="hidden" name="state" id="state">
                            <input type="hidden" name="country" id="country">
                            
                        </div>
                    </div>
                    <div class="fields">
                        <div class="field">
                            <label>Phone Number*</label>
                            <input type="text" name="phone_no" id="phone_no" onkeypress="return KeycheckOnlyNumeric(event);" placeholder="Type your phone number" required>
                        </div>
                    </div>
                </div>

                <div class="formfield">
                    <div class="fields">
                        <div class="field">
                            <label>Email*  (for us to contact you)</label>
                            <input type="text" name="email_for_contact_you" id="email_for_contact_you" placeholder="Type your email address" required>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="field">
                            <label>Email*  (to be listed on the business directory)</label>
                            <input type="text" name="email_for_business_directory" id="email_for_business_directory" placeholder="Type your email address" required>
                        </div>
                    </div>
                </div>  
                <div class="formfield">
                    <div class="fields">
                        <div class="field">
                            <label>Opening Hours</label>
                            <div class="selectdays office-opening-hours">
                                <div class="row">
                                    <div class="col-md-4 pt-5 text-left">
                                        <label>Monday</label>
                                        <input type="hidden" name="day[]" id="day_0" value="Monday">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="open_time[]" id="open_time_0" class="from-timepicker" readonly="">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="close_time[]" id="close_time_0" class="to-timepicker" readonly="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pt-5 text-left">
                                        <label>Tuesday</label>
                                        <input type="hidden" name="day[]" id="day_1" value="Tuesday">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="open_time[]" id="open_time_1" class="from-timepicker" readonly="">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="close_time[]" id="close_time_1" class="to-timepicker" readonly="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pt-5 text-left">
                                        <label>Wednesday</label>
                                        <input type="hidden" name="day[]" id="day_1" value="Wednesday">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="open_time[]" id="open_time_1" class="from-timepicker" readonly="">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="close_time[]" id="close_time_1" class="to-timepicker" readonly="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pt-5 text-left">
                                        <label>Thursday</label>
                                        <input type="hidden" name="day[]" id="day_1" value="Thursday">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="open_time[]" id="open_time_1" class="from-timepicker" readonly="">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="close_time[]" id="close_time_1" class="to-timepicker" readonly="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pt-5 text-left">
                                        <label>Friday</label>
                                        <input type="hidden" name="day[]" id="day_1" value="Friday">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="open_time[]" id="open_time_1" class="from-timepicker" readonly="">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="close_time[]" id="close_time_1" class="to-timepicker" readonly="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pt-5 text-left">
                                        <label>Saturday</label>
                                        <input type="hidden" name="day[]" id="day_1" value="Saturday">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="open_time[]" id="open_time_1" class="from-timepicker" readonly="">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="close_time[]" id="close_time_1" class="to-timepicker" readonly="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 pt-5 text-left">
                                        <label>Sunday</label>
                                        <input type="hidden" name="day[]" id="day_1" value="Sunday">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="open_time[]" id="open_time_1" class="from-timepicker" readonly="">
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" name="close_time[]" id="close_time_1" class="to-timepicker" readonly="">
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="selectdays">
                                <select name="opening_hours" id="opening_hours">
                                  <option value="">Select Days</option>
                                  <option value="01">1</option>
                                  <option value="02">2</option>
                                  <option value="03">3</option>
                                  <option value="04">4</option>
                                  <option value="05">5</option>
                                  <option value="06">6</option>
                                  <option value="07">7</option>
                                  <option value="08">8</option>
                                  <option value="09">9</option>
                                  <option value="10">10</option>
                                  <option value="11">11</option>
                                  <option value="12">12</option>
                                  <option value="13">13</option>
                                  <option value="14">14</option>
                                  <option value="15">15</option>
                                  <option value="16">16</option>
                                  <option value="17">17</option>
                                  <option value="18">18</option>
                                  <option value="19">19</option>
                                  <option value="20">20</option>
                                  <option value="21">21</option>
                                  <option value="22">22</option>
                                  <option value="23">23</option>
                                  <option value="24">24</option>
                                  <option value="25">25</option>
                                  <option value="26">26</option>
                                  <option value="27">27</option>
                                  <option value="28">28</option>
                                  <option value="29">29</option>
                                  <option value="30">30</option>
                                  <option value="31">31</option>
                             </select>
                            </div> -->
                        </div>
                    </div>
                    <div class="fields">
                        <div class="field">
                            <label>Links</label>
                            <ul class="links">
                                <li>
                                    <input type="text" name="facebook_link" id="facebook_link">
                                    <img src="<?php echo e(asset('images/ic_business_listing_facebook.png')); ?>">
                                </li>
                                <li>
                                    <input type="text" name="twitter_link" id="twitter_link">
                                    <img src="<?php echo e(asset('images/ic_business_listing_twitter.png')); ?>">
                                </li>
                                <li>
                                    <input type="text" name="linkedin_link" id="linkedin_link">
                                    <img src="<?php echo e(asset('images/ic_business_listing_linkedin.png')); ?>">
                                </li>
                                <li>
                                    <input type="text" name="youtube_link" id="youtube_link">
                                    <img src="<?php echo e(asset('images/ic_business_listing_youtube.png')); ?>">
                                </li>
                                <li>
                                    <input type="text" name="website_link" id="website_link">
                                    <img src="<?php echo e(asset('images/ic_business_listing_website.png')); ?>">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>  

                <div class="formfield">
                    <div class="fields">
                        <div class="field">
                            <label>Business Services*</label>
                            <div class="textarea">
                                <textarea name="services" id="services" maxlength="100" onkeyup="countChar(this)" placeholder="Type here" required></textarea>
                                <span><b id="charNum">0</b>/100</span>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="field">
                            <label>Upload your crest, emblem or logo here</label>
                            <div class="uploadlogo">
                                <div class="upload">
                                    <input name="bus_logo" id="business_logo" type="file">
                                    <img src="<?php echo e(asset('images/ic_profile_logo_upload.png')); ?>">
                                    <span class="filename"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  

                <div class="formfield">
                    <label>Photos (minimum 1 image per profile. maximum 5 images)*</label>
                    <div class="photos">
                        <div class="item addphoto">
                            <div class="iteminner text-center">
                                <!-- <img src="<?php echo e(asset('images/businesses1.png')); ?>"> -->
                                <img  id="photo1" class="upload" src="<?php echo e(asset('images/ic_profile_images_upload.png')); ?>">
                                <input type="file" name="business_image" id="image1" accept="image/*" onchange="loadFile1(event)" required="" style="opacity:0;">
                                <img id="remove1" class="remove" src="<?php echo e(asset('images/ic_profile_images_remove.png')); ?>" style="display: none;">
                            </div>
                        </div>
                        <div class="item addphoto">
                            <div class="iteminner text-center">
                                <!-- <img src="<?php echo e(asset('images/businesses2.png')); ?>"> -->
                                <img  id="photo2" class="upload" src="<?php echo e(asset('images/ic_profile_images_upload.png')); ?>">
                                <input type="file" name="business_images[]" id="image2" accept="image/*" onchange="loadFile2(event)" style="display: none;">
                                <img id="remove2" class="remove" src="<?php echo e(asset('images/ic_profile_images_remove.png')); ?>" style="display: none;">
                            </div>
                        </div>
                        <div class="item addphoto">
                            <div class="iteminner text-center">
                                <!-- <img src="<?php echo e(asset('images/businesses3.png')); ?>"> -->
                                <img  id="photo3" class="upload" src="<?php echo e(asset('images/ic_profile_images_upload.png')); ?>">
                                <input type="file" name="business_images[]" id="image3" accept="image/*" onchange="loadFile3(event)" style="display: none;">
                                <img id="remove3" class="remove" src="<?php echo e(asset('images/ic_profile_images_remove.png')); ?>" style="display: none;">
                            </div>
                        </div>
                        <div class="item addphoto">
                            <div class="iteminner text-center">
                                <!-- <img src="<?php echo e(asset('images/businesses4.png')); ?>"> -->
                                <img  id="photo4" class="upload" src="<?php echo e(asset('images/ic_profile_images_upload.png')); ?>">
                                <input type="file" name="business_images[]" id="image4" accept="image/*" onchange="loadFile4(event)" style="display: none;">
                                <img id="remove4" class="remove" src="<?php echo e(asset('images/ic_profile_images_remove.png')); ?>" style="display: none;">
                            </div>
                        </div>
                        <div class="item addphoto">
                            <div class="iteminner text-center">
                                <img  id="photo5" class="upload" src="<?php echo e(asset('images/ic_profile_images_upload.png')); ?>">
                                <input type="file" name="business_images[]" id="image5" accept="image/*" onchange="loadFile5(event)" style="display: none;">
                                <img id="remove5" class="remove" src="<?php echo e(asset('images/ic_profile_images_remove.png')); ?>" style="display: none;">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="buttons">
                    <button type="submit" class="business-preview" name="preview">Preview</button>
                    <button type="submit">Next</button>
                </div>
            <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
</div>
<!-- <div id="popupmap" style="width: 550px; height: 400px;"></div> -->

<?php $__env->startSection('pagescript'); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH2uiqesOZEbWOdwb0YbnrO9pErqV28nk&libraries=places"></script>
<script type="text/javascript"> 

    $('.from-timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
       /* minTime: '12:00am',
        maxTime: '12:00pm',*/
        defaultTime: '12:00am',
       /* startTime: '10:00',*/
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
    $('.to-timepicker').timepicker({
        timeFormat: 'h:mm p',
        interval: 60,
        /*minTime: '12:00am',
        maxTime: '12:00pm',*/
        defaultTime: '12:00pm',
       /* startTime: '10:00',*/
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });
</script>

<script>
  function countChar(val) {
    var len = val.value.length;
    $('#charNum').text(len);
  };
</script>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                jQuery('#blah').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#business_logo").change(function(){
        readURL(this);
        //jQuery('.previewimg').show();
        $('.filename').show();
        var file = $('#business_logo')[0].files[0].name;
        $('.filename').text(file);
    });
</script>

<script>
    function KeycheckOnlyNumeric(e)
    {

        var _dom = 0;
        _dom = document.all ? 3 : (document.getElementById ? 1 : (document.layers ? 2 : 0));
        if (document.all)
            e = window.event; // for IE
        var ch = '';
        var KeyID = '';
        if (_dom == 2) {                     // for NN4
            if (e.which > 0)
                ch = '(' + String.fromCharCode(e.which) + ')';
            KeyID = e.which;
        }
        else
        {
            if (_dom == 3) {                   // for IE
                KeyID = (window.event) ? event.keyCode : e.which;
            }
            else {                       // for Mozilla
                if (e.charCode > 0)
                    ch = '(' + String.fromCharCode(e.charCode) + ')';
                KeyID = e.charCode;
            }
        }
        if ((KeyID >= 65 && KeyID <= 90) || (KeyID >= 97 && KeyID <= 122) || (KeyID >= 33 && KeyID <= 47) || (KeyID >= 58 && KeyID <= 64) || (KeyID >= 91 && KeyID <= 96) || (KeyID >= 123 && KeyID <= 126) || (KeyID == 32))//changed by jshah for stopping spaces
        {
            return false;
        }
        return true;
    }
</script>

<script type="text/javascript">
    var upload_plusicon = "<?php echo e(asset('images/ic_profile_images_upload.png')); ?>";

//image 1 upload start
    $(document).on('click','#photo1',function(){
        $( "#image1" ).trigger( "click" );
    });
    var loadFile1 = function(event) {
        var output = document.getElementById('photo1');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo1').removeClass('upload');
            $('#remove1').css('display','block');
        }
    };
    $(document).on('click', '#remove1', function(){
        document.getElementById("image1").value=null; 
        var output = document.getElementById('photo1');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo1').addClass('upload');
            $('#remove1').css('display','none');
        }
    });
//image 1 upload end

//image 2 upload start
    $(document).on('click','#photo2',function(){
        $( "#image2" ).trigger( "click" );
    });
    var loadFile2 = function(event) {
        var output = document.getElementById('photo2');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo2').removeClass('upload');
            $('#remove2').css('display','block');
        }
    };
    $(document).on('click', '#remove2', function(){
        document.getElementById("image2").value=null; 
        var output = document.getElementById('photo2');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo2').addClass('upload');
            $('#remove2').css('display','none');
        }
    });
//image 2 upload end

//image 3 upload start
    $(document).on('click','#photo3',function(){
        $( "#image3" ).trigger( "click" );
    });
    var loadFile3 = function(event) {
        var output = document.getElementById('photo3');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo3').removeClass('upload');
            $('#remove3').css('display','block');
        }
    };
    $(document).on('click', '#remove3', function(){
        document.getElementById("image3").value=null; 
        var output = document.getElementById('photo3');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo3').addClass('upload');
            $('#remove3').css('display','none');
        }
    });
//image 3 upload end

//image 4 upload start
    $(document).on('click','#photo4',function(){
        $( "#image4" ).trigger( "click" );
    });
    var loadFile4 = function(event) {
        var output = document.getElementById('photo4');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo4').removeClass('upload');
            $('#remove4').css('display','block');
        }
    };
    $(document).on('click', '#remove4', function(){
        document.getElementById("image4").value=null; 
        var output = document.getElementById('photo4');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo4').addClass('upload');
            $('#remove4').css('display','none');
        }
    });
//image 4 upload end

//image 5 upload start
    $(document).on('click','#photo5',function(){
        $( "#image5" ).trigger( "click" );
    });
    var loadFile5 = function(event) {
        var output = document.getElementById('photo5');
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo5').removeClass('upload');
            $('#remove5').css('display','block');
        }
    };
    $(document).on('click', '#remove5', function(){
        document.getElementById("image5").value=null; 
        var output = document.getElementById('photo5');
        output.src = upload_plusicon;
        output.onload = function() {
            URL.revokeObjectURL(output.src) 
            $('#photo5').addClass('upload');
            $('#remove5').css('display','none');
        }
    });

    /*$(document).on("click", ".business-preview", function(){

    });*/
//image 5 upload end
</script>

<!-- Jquery validate -->
<script type="text/javascript">
$(document).ready(function () {
    $('#add-business-details').validate({ 
        rules: {
            "business_name": {
                required: true
            },
            "category_id": {
                required: true
            },
            "business_summary": {
                required: true,
                wordCount: 5
            },
            "address": {
                required: true
            },
            "phone_no": {
                required: true,
                number: true
            },
            "email_for_contact_you": {
                required: true,
                email: true
            },
            "email_for_business_directory": {
                required: true,
                email: true
            },
            "services": {
                required: true,
                maxlength: 100
            },
            "bus_logo": {
                required: true,
            },
            "business_images[]": {
                required: true,
            },
        },
       /* errorPlacement: function(error, element){
            if (element.attr("name") == "Email" ){
                $(".form-error-msg").html( error ); 
            } else {
                $(".form-error-msg").html( 'Please fill in all the information.' );
            }
        },*/
       /* submitHandler: function (form) { 
            return false; 
        },*/
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
    });

    $.validator.addMethod('wordCount', function(value, element, params) {
          var typedWords = jQuery.trim(value).split(' ').length; 
          return value == '' || (typedWords >= params);
    }, 'Minimum 50 words.');

});

set_location_in_map(23.026454661469973,72.5569747308441);

 function set_location_in_map(latitude, longitude){
   
      var map;
      var marker;
      var myLatlng = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));
      var geocoder = new google.maps.Geocoder();
      var infowindow = new google.maps.InfoWindow();
      
      var mapOptions = {
        zoom: 5,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      
      //map = new google.maps.Map(document.getElementById("popupmap"), mapOptions);
      
      var input = document.getElementById('address');
      var searchBox = new google.maps.places.SearchBox(input);
      //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
      
      // Bias the SearchBox results towards current map's viewport.
      /*map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
      });
      
      var markers = [];
      
      marker = new google.maps.Marker({
        map: map,
        position: myLatlng,
        draggable: true
      });
      map.setCenter(marker.getPosition());
      geocoder.geocode({'latLng': myLatlng }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
            
            $('#address').val(results[0].formatted_address);
            $('#latitude').val(marker.getPosition().lat());
            $('#longitude').val(marker.getPosition().lng());
          }
        }
      });*/
      
      searchBox.addListener('places_changed', function(results, status) {
        var places = searchBox.getPlaces();
        
        if (places.length == 0) {
          return;
        }
        
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          
          //marker.setPosition(place.geometry.location);
          
          geocoder.geocode({'latLng': place.geometry.location}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[0]) {
                //alert(results[0]);
                $("#location").val('');
                $("#address").val('');
                $("#city").val('');
                $("#postcode").val('');
                $("#state").val('');
                $("#country").val('');
                lat = marker.getPosition().lat();
                lng = marker.getPosition().lng();
                $("#latitude").val(lat);
                $("#longitude").val(lng);
                
                arrAddress = results[0].address_components;
                for (ac = 0; ac < arrAddress.length; ac++) {
                  if (arrAddress[ac].types[0] == "street_number") { document.getElementById("location").value = arrAddress[ac].long_name }
                  if (arrAddress[ac].types[0] == "route") { document.getElementById("address").value = arrAddress[ac].long_name }
                  if (arrAddress[ac].types[0] == "locality") { document.getElementById("city").value = arrAddress[ac].long_name }
                  if (arrAddress[ac].types[0] == "postal_code") { document.getElementById("postcode").value = arrAddress[ac].long_name }
                  if (arrAddress[ac].types[0] == "administrative_area_level_1") { document.getElementById("state").value = arrAddress[ac].long_name; }
                  if (arrAddress[ac].types[0] == "country") { document.getElementById("country").value = arrAddress[ac].long_name; }
                }
              }
            }
          });
          
          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
       /* map.fitBounds(bounds);
        map.setZoom(17);
        map.setCenter(marker.getPosition());*/
        
      });
      
      /*google.maps.event.addListener(marker, 'dragend', function() {
        
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
              $("#location").val('');
              $("#address").val('');
              $("#city").val('');
              $("#postcode").val('');
              $("#state").val('');
              $("#country").val('');
              lat = marker.getPosition().lat();
              lng = marker.getPosition().lng();
              $("#latitude").val(lat);
              $("#longitude").val(lng);
              
              arrAddress = results[0].address_components;
              for (ac = 0; ac < arrAddress.length; ac++) {
                if (arrAddress[ac].types[0] == "street_number") { document.getElementById("location").value = arrAddress[ac].long_name }
                if (arrAddress[ac].types[0] == "route") { document.getElementById("address").value = arrAddress[ac].long_name }
                if (arrAddress[ac].types[0] == "locality") { document.getElementById("city").value = arrAddress[ac].long_name }
                if (arrAddress[ac].types[0] == "postal_code") { document.getElementById("postcode").value = arrAddress[ac].long_name }
                if (arrAddress[ac].types[0] == "administrative_area_level_1") { document.getElementById("state").value = arrAddress[ac].long_name; }
                if (arrAddress[ac].types[0] == "country") { document.getElementById("country").value = arrAddress[ac].long_name; }
              }
            }
          }
        });
      });*/
 }
</script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/business/business_details.blade.php ENDPATH**/ ?>