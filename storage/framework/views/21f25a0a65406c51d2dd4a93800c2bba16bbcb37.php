<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>

<div class="finbusiness">
    <div class="layer"></div>

    <a class="addlisting" href="#"><i class="fa fa-plus"></i> Add Your Listing</a>

    <div class="sitecontainer">
        <div class="findbusinessform text-center">
            <h1>Find a Business</h1>
            <p>Wet Tropic Times Directory Helps You Find Your Business </p>
            <div class="form">
                <?php echo Form::open(array('route' => 'business.search_results','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'search_results-form')); ?>

                    <div class="formfield">
                        <div class="field">
                            <input type="text" name="whatyoulookingfor" id="whatyoulookingfor" placeholder="What are you looking for?">
                        </div>
                        <div class="field">
                            <div class="select">
                                <select name="region_id" id="region_id" required="">
                                    <option value="">All Regions</option>
                                    <?php if(isset($data['regions']) && count($data['regions']) > 0): ?>
                                        <?php $__currentLoopData = $data['regions']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $region): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($region->region_id); ?>"><?php echo e($region->region_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                </select>
                                <img src="<?php echo e(asset('images/ic_images_arrow_down.png')); ?>">
                            </div>
                        </div>
                        <div class="field">
                            <div class="select">
                                <select name="category_id" id="category_id" required="">
                                    <option value="">Select category</option>
                                    <?php $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                                        <option value="<?php echo e($value->category_id); ?>"><?php echo e($value->category_name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                                <img src="<?php echo e(asset('images/ic_images_arrow_down.png')); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="button">
                        <button type="submit">Search</button>
                    </div>
                <?php echo Form::close(); ?>

            </div>
        </div>
    </div>
</div>

<div class="finbusinessmain">
    <div class="busenessheadding">
        <h2>Featured Businesses</h2>
    </div>
    <div class="busenesslisting">
        <div class="sitecontainer">
            <?php if(isset($data['businesses']) && count($data['businesses']) > 0): ?>
                <?php $__currentLoopData = $data['businesses']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $business): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(route('business.view_business',$business->business_id)); ?>">
                        <div class="item">
                            <div class="iteminner">
                                <img src="<?php echo e(asset('business_images/thumb')); ?>/<?php echo e($business->business_logo); ?>">
                                <h3><?php echo e($business->business_name); ?></h3>
                                <p><?php echo e($business->business_summary); ?></p>
                                <h4><?php echo e($business->address); ?></h4>
                                <span><img src="<?php echo e(asset('images/ic_distributor_phone.png')); ?>"> <?php echo e($business->phone_no); ?></span>
                            </div>
                        </div>
                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <p style="text-align: center;">No Featured Businesses Found</p>
            <?php endif; ?>
        </div>
    </div>

    <div class="busenessheadding">
        <h2>Search by Category</h2>
    </div>

    <div class="searchbybusiness">
        <div class="sitecontainer">
            <?php if(isset($data['categories']) && count($data['categories']) > 0): ?>
                <?php $__currentLoopData = $data['categories']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="#">
                        <div class="item col-md-2">
                            <div class="iteminner">
                                <div class="layer"></div>
                                <img src="<?php echo e(asset('category/thumb')); ?>/<?php echo e($category->category_image); ?>">
                                <h3><?php echo e($category->category_name); ?></h3>
                            </div>
                        </div>
                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <p style="text-align: center;">No Category Found</p>
            <?php endif; ?>
        </div>
    </div>

    <div class="busenessheadding">
        <h2>Search by Region</h2>
    </div>

    <div class="searchbybusiness">
        <div class="sitecontainer">
            <?php if(isset($data['regions']) && count($data['regions']) > 0): ?>
                <?php $__currentLoopData = $data['regions']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $region): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="#">
                        <div class="item col-md-2">
                            <div class="iteminner">
                                <div class="layer"></div>
                                <img src="<?php echo e(asset('regions/thumb')); ?>/<?php echo e($region->region_image); ?>">
                                <h3><?php echo e($region->region_name); ?></h3>
                            </div>
                        </div>
                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
                <p style="text-align: center;">No Category Found</p>
            <?php endif; ?>
            <!-- <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="<?php echo e(asset('images/localarea6.png')); ?>">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="<?php echo e(asset('images/localarea3.png')); ?>">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="<?php echo e(asset('images/localarea1.png')); ?>">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="<?php echo e(asset('images/localarea2.png')); ?>">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="<?php echo e(asset('images/localarea5.png')); ?>">
                    <h3>Tully, Lower…</h3>
                </div>
            </div>
            <div class="item col-md-2">
                <div class="iteminner">
                    <div class="layer"></div>
                    <img src="<?php echo e(asset('images/localarea4.png')); ?>">
                    <h3>Innisfail, Mour…</h3>
                </div>
            </div> -->
        </div>
    </div>
</div>
<?php $__env->startSection('pagescript'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/business/find_business.blade.php ENDPATH**/ ?>