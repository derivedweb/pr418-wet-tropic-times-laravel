<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>
 
<div class="userloginregister myaccount">

    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="#">My Account</a> <span>/</span> Change Password 
        </div>
    </div>

    <div class="userloginregisterinner">
        <div class="formbox">
            <h2>Change Password</h2>
            <?php echo Form::open(array('route' => 'update_change_password','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'update-change-password')); ?>


                <?php if($message = Session::get('success')): ?>
                    <div class="text-success">
                        <?php echo e($message); ?>

                    </div>
                <?php endif; ?>

                <div class="field">
                    <label>Old Password</label>
                    <input type="password" placeholder="XXXXXXXX" name="OldPassword" id="OldPassword" required>
                    <?php if($message = Session::get('error')): ?>
                        <div class="text-danger">
                            <?php echo e($message); ?>

                        </div>
                    <?php endif; ?> 
                </div>
                <div class="field">
                    <label>New Password</label>
                    <input type="password" placeholder="XXXXXXXX" name="NewPassword" id="NewPassword" required>
                </div>
                <div class="field">
                    <label>Confirm Password</label>
                    <input type="password" placeholder="XXXXXXXX" name="ConfirmNewPassword" id="ConfirmNewPassword" required>
                </div>
                <div class="button chaangepwd">
                    <button type="submit">Change Password</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $__env->startSection('pagescript'); ?>
<!-- Jquery validate -->
<script type="text/javascript">

$(document).ready(function () {
    
    $('#update-change-password').validate({ 
    rules: {
        "OldPassword": {
            required: true
        },
        "NewPassword": {
            required: true
        },
        "ConfirmNewPassword": {
            required: true,
            equalTo : "#NewPassword",
        },
    },
    messages:{
        ConfirmNewPassword:{
            equalTo:'Passwords don\'t match'
        }
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/user/change_password.blade.php ENDPATH**/ ?>