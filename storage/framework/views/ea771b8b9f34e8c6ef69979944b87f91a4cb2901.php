<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>
 <div class="userloginregister">
    <div class="userloginregisterinner">
        <div class="formbox">
            <h2>Login</h2>
            <?php echo Form::open(array('route' => 'login','method'=>'POST', 'class'=>'login-form', 'id'=>'login-form')); ?>


                <?php if($message = Session::get('success')): ?>
                    <div class="text-success">
                        <?php echo e($message); ?>

                    </div>
                <?php elseif($message = Session::get('error')): ?>
                    <div class="text-danger">
                        <?php echo e($message); ?>

                    </div>
                <?php endif; ?> 
                <div class="field">
                    <label>Email</label>
                    <input type="text" placeholder="Type your email address" name="email">
                </div>
                <div class="field">
                    <label>Password</label>
                    <input type="password" placeholder="Type your password" name="password">
                </div>
                <div class="button">
                    <button type="submit">Login</button>
                </div>
                <div class="forgotpwd">
                    <a href="<?php echo e(route('forget_password')); ?>">Forgotten your password?</a>
                </div>
                <div class="otherlogin text-center">
                    <div class="line"></div>
                    <label>Or use one of these options</label>
                    <ul>
                        <li class="apple"><a href="#"><img src="<?php echo e(asset('images/ic_login_apple.png')); ?>"></a></li>
                        <li class="google"><a href="#"><img src="<?php echo e(asset('images/ic_login_google.png')); ?>"></a></li>
                        <li class="facebook"><a href="#"><img src="<?php echo e(asset('images/ic_login_facebook.png')); ?>"></a></li>
                    </ul>
                </div>
                <div class="newuser">
                    <p>New to Wet Tropic Times? <a href="<?php echo e(route('create_your_account')); ?>">Sign Up Now</a></p>
                </div>
             <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<?php $__env->startSection('pagescript'); ?>
<!-- Jquery validate -->
<script type="text/javascript">

$(document).ready(function () {
    
    $('#login-form').validate({ 
    rules: {
        "email": {
            required: true,
            email: true
        },
        "password": {
            required: true
        },
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/login.blade.php ENDPATH**/ ?>