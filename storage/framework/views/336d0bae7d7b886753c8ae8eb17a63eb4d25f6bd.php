<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>
 
 <div class="subscriptionsplans">

    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="<?php echo e(route('user.subscriptions')); ?>">Subscriptions</a> <span>/</span> Cancel your plan 
        </div>
    </div>
    
    <div class="subscriptionsplansinner">
        <form method="post" action="<?php echo e(route('user.cancel_subscription_from_stripe')); ?>">
        <?php echo csrf_field(); ?>

        <input type="hidden" name="subscription_id" value="<?php echo e(Request::get('subscription_id')); ?>">
        <div class="subscriptionsbox">
            <h2>Cancel Your Plan</h2>
            <h4>Current Plan</h4>
            <div class="plandetails">
                 <div class="planinfo renewplan">
                    <label><?php echo e(Request::get('plan_title')); ?></label>
                    <span><?php echo e(Request::get('plan_desc')); ?></span>
                    <p>Your plan will be cancelled, but is still available until the end of your billing 
                    period on <?php echo e(date("d M Y", (Request::get('end_period')))); ?>.</p>

                    <p style="padding-top: 25px;">If you change your mind, you can renew your subscription.</p>
                 </div>
            </div>
            <div class="buttons cancelplan text-center">
                <a class="goback" href="<?php echo e(route('user.subscriptions')); ?>">Go Back</a>
                <button type="submit">Cancel Plan</button>
            </div>
        </div>
     </form>
    </div>
</div>
<?php $__env->startSection('pagescript'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/cancel_plan_confirmation.blade.php ENDPATH**/ ?>