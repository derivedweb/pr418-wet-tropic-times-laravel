<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>
 <div class="subscriptionsplans">

    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="<?php echo e(route('user.subscriptions')); ?>">Subscriptions</a> <span>/</span> Update your plan 
        </div>
    </div>
    
    <div class="subscriptionsplansinner">
         <form method="post" action="<?php echo e(route('user.renew_plan_from_stripe')); ?>">
            <?php echo csrf_field(); ?>
        <div class="subscriptionsbox">
            <h2>Renew Your Plan</h2>
            <h4>Current Plan</h4>
            <div class="plandetails">
                 <div class="planinfo renewplan">
                    <label><?php echo e(Request::get('plan_title')); ?></label>
                    <span><?php echo e(Request::get('plan_desc')); ?></span>
                    <p>This plan will no longer be cancelled. It will renew on <?php echo e(date("d M Y", (Request::get('end_period')))); ?>.</p>
                 </div>
            </div>
            <div class="buttons text-center">
                <a class="goback" href="<?php echo e(route('user.subscriptions')); ?>?flag=renew">Go Back</a>
                <button type="submit">Renew Plan</button>
            </div>
        </div>
        </form>
    </div>
</div>
<?php $__env->startSection('pagescript'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/renew_plan_confirmation.blade.php ENDPATH**/ ?>