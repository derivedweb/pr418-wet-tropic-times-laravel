<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>
 <div class="userloginregister myaccount">
    <div class="subscribebreadcumbs">
        <div class="sitecontainer">
            <a href="">Business - Account Details </a>
        </div>
    </div>

    <div class="userloginregisterinner">
        <div class="formbox">
            <h2>My Account</h2>
            <?php echo Form::open(array('route' => 'update_profile','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'user-update-profile')); ?>


                <?php if(isset($data['error']) && $data['error'] != ''): ?>
                    <div class="text-danger">
                        <?php echo e($data['error']); ?>

                    </div>
                <?php endif; ?>

                <?php if($errors->any()): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="text-danger"><?php echo e($error); ?></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

                <?php if($message = Session::get('success')): ?>
                    <div class="text-success">
                        <?php echo e($message); ?>

                    </div>
                <?php elseif($message = Session::get('error')): ?>
                    <div class="text-danger">
                        <?php echo e($message); ?>

                    </div>
                <?php endif; ?> 

                <div class="field">
                    <label>Display Name</label>
                    <input type="text" id="displayname" name="displayname" value="<?php echo e(Auth::user()->displayname); ?>">
                </div>
                <div class="field">
                    <label>Email</label>
                    <input type="email" id="email" name="email" value="<?php echo e(Auth::user()->email); ?>">
                </div>
                <div class="buttons">
                    <a href="<?php echo e(route('user.change_password')); ?>">Change Password</a>
                    <button type="submit">Update Account</button>
                </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<?php $__env->startSection('pagescript'); ?>
<!-- Jquery validate -->
<script type="text/javascript">

$(document).ready(function () {
    
    $('#user-update-profile').validate({ 
    rules: {
        "displayname": {
            required: true
        },
        "email": {
            required: true,
            email: true
        }
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/user/my_account.blade.php ENDPATH**/ ?>