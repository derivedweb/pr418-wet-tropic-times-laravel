<?php if(!$businesses->isEmpty()): ?>
<?php $__currentLoopData = $businesses; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $business): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <a href="javascript:void(0);"> 
        <div class="item" data-b-id="<?php echo e($business->business_id); ?>">
            <div class="image">
                <img src="<?php echo e(asset('business_images/thumb')); ?>/<?php echo e($business->business_logo); ?>">
            </div>
            <div class="content">
                <h3><?php echo e($business->business_name); ?></h3>
                <p><?php echo e($business->services); ?></p>
                <h4><?php echo e($business->address); ?></h4>
                <div class="info">
                    <img src="<?php echo e(asset('images/ic_distributor_phone.png')); ?>">
                    <label><?php echo e($business->phone_no); ?></label>
                    <?php if(!empty($business->business_time)): ?>
                   
                        <?php
                            $time = explode("-", $business->business_time);
                        ?>
                        <?php if(date("H:i:s") > $time[0] && date("H:i:s") < $time[1]): ?>
                            <span>Open &nbsp;<b>Closes <?php echo e(date("hA", strtotime($time[1]))); ?></b></span>
                        <?php else: ?>
                            <span style="color:red;">Closed &nbsp;<b><!-- Open <?php echo e(date("hA", strtotime($time[0]))); ?> --></b></span>
                        <?php endif; ?>
                    
                    <?php else: ?>
                        <span style="color:red;">Closed</span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
     </a> 
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<input type="hidden" class="last_page" value="<?php echo e($businesses->lastPage()); ?>">
<input type="hidden" class="current_page" value="<?php echo e($businesses->currentPage()); ?>">
<?php else: ?>
  <div class="text-center">
    <!-- <img class="no-messages" src="<?php echo e(asset('images/no-messages.png')); ?>" width="100"> -->
    <p>No Business.</p>
  </div>
<?php endif; ?>
<?php if($businesses->lastPage() > 1): ?>
   <!--  <div class="pagination">
        <ul>
            <li><a href="<?php echo e($businesses->url(1)); ?>"><i class="fa fa-angle-left"></i></a></li>
            <?php for($i = 1; $i <= $businesses->lastPage(); $i++): ?>
                <li class="page-item <?php echo e(($businesses->currentPage() == $i) ? ' active' : ''); ?>">
                    <a class="page-link" href="<?php echo e($businesses->url($i)); ?>"><?php echo e($i); ?></a>
                </li>
            <?php endfor; ?>
            <li><a href="<?php echo e(($businesses->currentPage() == $businesses->lastPage()) ? '#' : $businesses->url($businesses->currentPage()+1)); ?>" ><i class="fa fa-angle-right"></i></a></li>
        </ul>
    </div> -->
<?php endif; ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/business/load_business.blade.php ENDPATH**/ ?>