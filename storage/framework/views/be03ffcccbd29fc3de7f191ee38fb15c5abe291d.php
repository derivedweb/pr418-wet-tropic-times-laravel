<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>
  <div class="subscription">
    <div class="subscriptioninner">
        <div class="subscribebox">
            <img class="close" src="<?php echo e(asset('images/ic_profile_images_remove.png')); ?>">
            <h4>Subscribe to read full news articles, watch video 
            documentaries and access to exclusive content.</h4>

            <?php if($message = Session::get('success')): ?>
                <div class="text-success">
                    <?php echo e($message); ?>

                </div>
            <?php elseif($message = Session::get('error')): ?>
                <div class="text-danger">
                    <?php echo e($message); ?>

                </div>
            <?php endif; ?>

            <form action="<?php echo e(route('user.stripe_payment_request')); ?>" method="post" id="payment-form">
                  <?php echo csrf_field(); ?>

                <input type="hidden" name="displayname" value="<?php echo e(Request::get('displayname')); ?>">
                <input type="hidden" name="email" value="<?php echo e(Request::get('email')); ?>">
                <input type="hidden" name="password" value="<?php echo e(Request::get('password')); ?>">

            <div class="subscribeplan">
              <?php $__currentLoopData = $products->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($product->unit_label == ''): ?>
                <div class="item <?php echo e(($key == 4) ? 'active' : ''); ?>" data-price="<?php echo e(($product->price->unit_amount_decimal/100)); ?>" data-interval="<?php echo e($product->price->recurring->interval); ?>" data-interval-count="<?php echo e($product->price->recurring->interval_count); ?>">
                    <label><?php echo e($product->name); ?></label>
                    <span><?php echo e($product->description); ?></span>
                    <input type="radio" name="plan" value="<?php echo e($product->price->id); ?>" <?php echo e(($key == 4) ? 'checked' : ''); ?>>
                    <img class="check" src="<?php echo e(asset('images/check.png')); ?>">
                </div>
                <?php endif; ?>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <!-- <div class="item active">
                    <label>Quarterly</label>
                    <span>$59.99 / 3 months</span>
                    <input type="radio" name="plan" checked="" value="price_1J4kOeG8RgN0dSOYRAxatk0o">
                    <img class="check" src="<?php echo e(asset('images/check.png')); ?>">
                </div>
                <div class="item">
                    <label>Monthly</label>
                    <span>$14.99 / month</span>
                    <input type="radio" name="plan" value="price_1J4kPTG8RgN0dSOYHagXm9Qa">
                    <img class="check" src="<?php echo e(asset('images/check.png')); ?>">
                </div> -->
            </div>

            <div class="paymentdetails">
                <h3>Payment Details</h3>
                <div class="carddetails">
                    <div class="field cardname">
                        <input type="text" name="name_on_card" required="" placeholder="Name on card">
                    </div>
                    <div class="field cardinfo" style="padding: 16px;">
                        <div id="card-element">
                        <!-- A Stripe Element will be inserted here. -->
                        </div>
                        <div id="card-errors" role="alert"></div>
                       
                      <!--   <input type="text" name="" placeholder="Card number" onkeypress="return KeycheckOnlyNumeric(event);">
                        <input class="mask" type="text" name="" placeholder="MM/YY" onkeypress="return KeycheckOnlyNumeric(event);">
                        <input type="text" name="" placeholder="CVV" onkeypress="return KeycheckOnlyNumeric(event);"> -->
                    </div>
                </div>
            </div>
          
       
            <div class="button">
                <button type="submit">Complete Purchase</button>
            </div>
           </form>   
            <div class="subscribetext">
                <p>
                    Your subscription will automatically renew at $<span class="plan-price">59.99</span> AUD every <span class="plan-interval">3</span> <span class="plan-interval-text">months</span> until cancelled. 
                    You can cancel automatic renewal in your account settings any time before February 15, 2022.
                </p>
            </div>
        </div>

    </div>
</div>
<?php $__env->startSection('pagescript'); ?>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript">
var stripe = Stripe("<?php echo e(Config::get('constants.stripe.publishable-key')); ?>");
var elements = stripe.elements();
// Custom styling can be passed to options when creating an Element.
var style = {
  base: {
    // Add your base input styles here. For example:
    fontSize: '18px',
    /*color: '#32325d',*/
  },
};

// Create an instance of the card Element.
var card = elements.create('card', {hidePostalCode: true, style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Create a token or display an error when the form is submitted.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the customer that there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}


$(function() {
    $(".item.active").trigger("click");
});
$(document).on("click", ".item", function(){
    $(".item").removeClass("active");
    $(this).addClass('active');
    $(".plan-price").html($(this).data("price"));
    $(".plan-interval-text").html($(this).data("interval"));
    $(".plan-interval").html($(this).data("interval-count"));
});
  </script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/subscribe.blade.php ENDPATH**/ ?>