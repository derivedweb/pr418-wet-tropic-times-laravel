<?php $__env->startSection('content'); ?>

<?php $__env->startSection('pagestylesheet'); ?>
 
<?php $__env->stopSection(); ?>
<div class="userloginregister">
    <div class="userloginregisterinner">
        <div class="formbox createaccount">
            <h2>Create Account with <br> Wet Tropic Times</h2>
            <?php echo Form::open(array('route' => 'user.subscribe','method'=>'POST', 'class'=>'form-horizontal', 'id'=>'add-signup-data-form')); ?>


                <?php if($message = Session::get('success')): ?>
                    <div class="text-success">
                        <?php echo e($message); ?>

                    </div>
                <?php elseif(isset($data['error']) && $data['error'] != ''): ?>
                    <div class="text-danger">
                        <?php echo e($data['error']); ?>

                    </div>
                <?php endif; ?>

                <?php if($errors->any()): ?>
                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="text-danger"><?php echo e($error); ?></div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>

                <div class="field">
                    <label>Display Name</label>
                    <?php if(isset($data['displayname']) && $data['displayname'] != ''): ?>
                        <input type="text" name="displayname" id="displayname" placeholder="Type here" required value="<?php echo e($data['displayname']); ?>">
                    <?php else: ?>
                        <input type="text" name="displayname" id="displayname" placeholder="Type here" required>
                    <?php endif; ?>
                    <span>The name you see when you login with Wet Tropic Times </span>
                </div>
                <div class="field">
                    <label>Email Address</label>
                    <?php if(isset($data['email']) && $data['email'] != ''): ?>
                        <input type="email" name="email" id="email" placeholder="Type your email" required value="<?php echo e($data['email']); ?>">
                    <?php else: ?>
                        <input type="email" name="email" id="email" placeholder="Type your email" required>
                    <?php endif; ?>
                </div>
                <div class="field">
                    <label>Password</label>
                    <input type="password" name="password" id="password" placeholder="Type your password" required>
                </div>
                <div class="button">
                    <button type="submit">Continue with Email</button>
                </div>
                <div class="agreeterms">
                    <p>By signing in or creating an account, you agree to our <br>
                       <a href="<?php echo e(Config::get('constants.app.wp-url').'/terms-conditions'); ?>" target="_blank">Terms & Conditions</a> and <a href="<?php echo e(Config::get('constants.app.wp-url').'/privacy-policy'); ?>" target="_blank">Privacy Policy</a></p>
                </div>
            <?php echo Form::close(); ?>

        </div>
    </div>
</div>
<?php $__env->startSection('pagescript'); ?>
<!-- Jquery validate -->
<script type="text/javascript">

$(document).ready(function () {
    
    $('#add-signup-data-form').validate({ 
    rules: {
        "displayname": {
            required: true
        },
        "email": {
            required: true,
            email: true
        },
        "password": {
            required: true
        },
    },
   /* errorPlacement: function(error, element){
        if (element.attr("name") == "Email" ){
            $(".form-error-msg").html( error ); 
        } else {
            $(".form-error-msg").html( 'Please fill in all the information.' );
        }
    },*/
   /* submitHandler: function (form) { 
        return false; 
    },*/
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
    },
});
});
</script>
<?php $__env->stopSection(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/wtt_laravel/resources/views/create_your_account.blade.php ENDPATH**/ ?>